FROM python:3.11.3-slim-bullseye

LABEL maintainer="Demos Berlin e.V. <info@demonstrations.org>"

# update system and install system dependencies
# hadolint ignore=DL3015
RUN apt-get update -y -qq && \
    apt-get upgrade -y && \
    apt-get install -y -qq \
            git=1:2.30.2-1+deb11u2 \
            make=4.3-4.1 \
            curl=7.74.0-1.3+deb11u7 \
            docker=1.5-2 \
            docker-compose=1.25.0-1 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# install and set up go for gitleaks
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# currently disabled due to go fuck ups
#ENV PATH="/usr/local/go/bin:${PATH}"
#RUN curl -O https://dl.google.com/go/go1.12.7.linux-amd64.tar.gz && \
#    tar xvf go1.12.7.linux-amd64.tar.gz && \
#    mv go /usr/local

# install and set up poetry
ENV PATH="/root/.local/bin:${PATH}"
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl -sSL https://install.python-poetry.org | python - && \
    poetry config virtualenvs.in-project true
