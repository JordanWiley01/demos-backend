#! /usr/bin/env sh

# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -e

# Let the DB start
demos-backend check-dependencies

# only frontend_api service does migrations etc
if [ "$1" = "frontend-api" ]; then
  alembic upgrade head
  beanie migrate -uri "mongodb://$MONGO_USERNAME:$MONGO_PASSWORD@mongodb" -db demosdb -p /app/migrations/beanie
  demos-backend migrate-data

  if [ $# -eq 2 ]; then
    demos-backend initialize-databases "$2"
  elif [ $# -eq 3 ]; then
    demos-backend initialize-databases "$2" "$3"
  else
    demos-backend initialize-databases
  fi
fi

demos-backend "$1"
