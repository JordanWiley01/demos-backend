FROM python:3.11.3-slim-bullseye AS builder

# update system and install system dependencies
# hadolint ignore=DL3015
RUN apt-get update -y -qq && \
    apt-get upgrade -y && \
    apt-get install -y -qq \
            git=1:2.30.2-1+deb11u2  \
            curl=7.74.0-1.3+deb11u7 \
            make=4.3-4.1 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# install and set up poetry
ENV PATH="/root/.local/bin:${PATH}"
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl -sSL https://install.python-poetry.org | python - && \
    poetry config virtualenvs.in-project true

WORKDIR /build

COPY ./pyproject.toml ./pyproject.toml
COPY ./poetry.lock ./poetry.lock
COPY ./Makefile ./Makefile
COPY ./demos_backend ./demos_backend

RUN make install && \
    make build

FROM python:3.11.3-slim-bullseye

LABEL maintainer="Demos Berlin e.V. <info@demonstrations.org>"

# hadolint ignore=DL3015
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /src
COPY --from=builder /build/dist .
RUN pip3 --no-cache-dir install ./*.whl
COPY ./deployment/docker/app.entrypoint.sh /bin/entrypoint

WORKDIR /app
COPY ./migrations ./migrations
COPY ./alembic.ini ./alembic.ini

ENTRYPOINT ["entrypoint"]
