# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.schemas.event_type_request import (
    EventTypeRequestCreate,
    EventTypeRequestInDB,
)
from demos_backend.core.schemas.topic_request import TopicRequestUpdate
from demos_backend.core.types import RequestStatus


@pytest.mark.asyncio
class TestEventTypeRequest:
    async def test_new(self, authenticated_admin_api_client: TestClient):
        new_event_type_request = EventTypeRequestCreate(
            name="Infostand", requester_organisation_id=1
        )
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/event_type_request/new", json=new_event_type_request.dict()
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventTypeRequestInDB(**response_data)
        assert data.name == new_event_type_request.name

    async def test_get_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/event_type_request/1",
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventTypeRequestInDB(**response_data)
        assert data.name == "Sit-In"

    async def test_update_by_id(self, authenticated_admin_api_client: TestClient):
        update_data = TopicRequestUpdate(status=RequestStatus.DENIED)
        with authenticated_admin_api_client as client:
            response = client.put("/v1/event_type_request/1", json=update_data.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventTypeRequestInDB(**response_data)
        assert data.status == RequestStatus.DENIED
