# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.schemas.event_type import EventTypeInDB
from demos_backend.core.schemas.topic import TopicCreate, TopicInDB, TopicUpdate


@pytest.mark.asyncio
class TestTopic:
    async def test_list_all(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/topic/",
            )
        assert response.status_code == 200
        response_data: list = response.json()
        data = [TopicInDB(**topic) for topic in response_data]
        assert len(data) > 0

    async def test_get_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/topic/1",
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = TopicInDB(**response_data)
        assert data.id == 1

    async def test_new(self, authenticated_admin_api_client: TestClient):
        new_topic = TopicCreate(name_en="Baeume", name_de="Trees")
        with authenticated_admin_api_client as client:
            response = client.post("/v1/topic/new", json=new_topic.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = TopicInDB(**response_data)
        assert data.name_en == new_topic.name_en
        assert data.name_de == new_topic.name_de

    async def test_update(self, authenticated_admin_api_client: TestClient):
        new_topic = TopicUpdate(name_en="Sand", name_de="Sand")
        with authenticated_admin_api_client as client:
            response = client.put("/v1/topic/1", json=new_topic.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = TopicInDB(**response_data)
        assert data.name_en == new_topic.name_en
        assert data.name_de == new_topic.name_de

    async def test_delete(self, authenticated_admin_api_client: TestClient):
        new_event_type = TopicCreate(name_de="deletion", name_en="deletion")
        with authenticated_admin_api_client as client:
            response = client.post("/v1/topic/new", json=new_event_type.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventTypeInDB(**response_data)

        authenticated_admin_api_client.delete(f"/v1/topic/{data.id}")

        with authenticated_admin_api_client as client:
            response = client.get(f"/v1/topic/{data.id}")

        assert response.status_code == 404
