# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.schemas.information import (
    InformationCreate,
    InformationInDB,
    InformationQuery,
    InformationUpdate,
)


@pytest.mark.asyncio
class TestInformation:
    async def test_new(self, authenticated_admin_api_client: TestClient):
        new_information = InformationCreate(
            name_native="TestInfo",
            name_english="TestInfoEng",
            url_native="url_native",
            url_english="url_english",
            country="Deutschland",
            city="Berlin",
            zip_code="12345",
        )
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/information/new",
                json=new_information.dict(),
                params={"address_language": "de"},
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = InformationInDB(**response_data)
        assert data.name_native == new_information.name_native

    async def test_available_cities(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/information/available-cities")
        assert response.status_code == 200
        response_data: list = response.json()
        assert "Berlin" in response_data

    async def test_find(self, authenticated_admin_api_client: TestClient):
        criteria = InformationQuery(country="Deutschland", city="Berlin")
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/information/find",
                json=criteria.dict(),
                params={"address_language": "de"},
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = [InformationInDB(**info) for info in response_data]
        assert len(data) > 0

    async def test_update(self, authenticated_admin_api_client: TestClient):
        updated_info = InformationUpdate(
            name_native="Klima Demonstration",
            name_english="Climate Demonstration",
        )
        with authenticated_admin_api_client as client:
            response = client.put(
                "/v1/information/1",
                json=updated_info.dict(),
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = InformationInDB(**response_data)
        assert data.name_native == updated_info.name_native

    async def test_delete(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.delete("/v1/information/1")
        assert response.status_code == 200
