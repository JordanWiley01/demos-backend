# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.configuration import Configuration
from demos_backend.core.schemas.token import Token
from demos_backend.core.schemas.user import UserCreate, UserInDB, UserUpdate
from demos_backend.services.frontend_api.exceptions import email_exists, username_exists


@pytest.mark.asyncio
class TestUser:
    async def test_get_token(self, unauthenticated_api_client: TestClient):
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        content = (
            "username=solo_user@demonstrations.org&password=solo_usersolo_usersolo_user"
        )
        with unauthenticated_api_client as client:
            response = client.post(
                "/v1/user/get_token", headers=headers, content=content
            )

        assert response.status_code == 200
        response_data: dict = response.json()
        token = Token.parse_obj(response_data)
        assert token.access_token

    async def test_refresh_token(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/user/refresh_token")
        assert response.status_code == 200
        response_data: dict = response.json()

        token = Token.parse_obj(response_data)
        assert token.access_token

    async def test_registration(self, unauthenticated_api_client: TestClient):
        new_user = UserCreate(
            email="new@user.de",
            username="new_user",
            password="new_usernew_usernew_user",
        )
        with unauthenticated_api_client as client:
            response = client.post("/v1/user/registration", json=new_user.dict())
        assert response.status_code == 200
        response_data: dict = response.json()

        data = UserInDB.parse_obj(response_data)
        assert data.email == new_user.email
        assert data.username == new_user.username

    async def test_duplicate_username(self, unauthenticated_api_client: TestClient):
        duplicate_user = UserCreate(
            email="duplicate@user.de",
            username="duplicate_user",
            password="new_usernew_usernew_user",
        )

        duplicate_user2 = UserCreate(
            email="duplicate1@user.de",
            username="duplicate_user",
            password="new_usernew_usernew_user",
        )

        with unauthenticated_api_client as client:
            response = client.post("/v1/user/registration", json=duplicate_user.dict())
        assert response.status_code == 200

        with unauthenticated_api_client as client:
            response = client.post("/v1/user/registration", json=duplicate_user2.dict())
        assert response.status_code == 409
        data = response.json()
        assert data["detail"] == username_exists.detail

    async def test_duplicate_email(self, unauthenticated_api_client: TestClient):
        duplicate_user = UserCreate(
            email="duplicate_email@user.de",
            username="duplicate_email",
            password="new_usernew_usernew_user",
        )

        duplicate_user2 = UserCreate(
            email="duplicate_email@user.de",
            username="duplicate_email1",
            password="new_usernew_usernew_user",
        )

        with unauthenticated_api_client as client:
            response = client.post("/v1/user/registration", json=duplicate_user.dict())
        assert response.status_code == 200

        with unauthenticated_api_client as client:
            response = client.post("/v1/user/registration", json=duplicate_user2.dict())
        assert response.status_code == 409
        data = response.json()
        assert data["detail"] == email_exists.detail

    async def test_get_self(
        self, authenticated_admin_api_client: TestClient, loaded_config: Configuration
    ):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/user/me")
        assert response.status_code == 200
        response_data: dict = response.json()

        data = UserInDB.parse_obj(response_data)
        assert data.email == loaded_config.DEFAULT_USER_MAIL
        assert data.username == loaded_config.DEFAULT_USER_NAME

    async def test_delete_self(self, authenticated_delete_user_api_client: TestClient):
        with authenticated_delete_user_api_client as client:
            response = client.delete("/v1/user/me")
        assert response.status_code == 200
        response_data: dict = response.json()

        data = UserInDB.parse_obj(response_data)
        assert data.username == "delete_user"

        with authenticated_delete_user_api_client as client:
            response = client.get("/v1/user/me")
        assert response.status_code == 401

    async def test_update_self(self, authenticated_admin_api_client: TestClient):
        updated_user = UserUpdate(email="updated@testing.com")
        with authenticated_admin_api_client as client:
            response = client.put("/v1/user/me", json=updated_user.dict())
        assert response.status_code == 200
        response_data: dict = response.json()

        data = UserInDB.parse_obj(response_data)
        assert data.email == updated_user.email

    async def test_update_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/user/me")
        assert response.status_code == 200
        response_data: dict = response.json()

        data = UserInDB.parse_obj(response_data)

        updated_user = UserUpdate(email="updated2@testing.com")
        with authenticated_admin_api_client as client:
            response = client.put(f"/v1/user/{data.id}", json=updated_user.dict())
        assert response.status_code == 200
        response_data: dict = response.json()

        data = UserInDB.parse_obj(response_data)
        assert data.email == updated_user.email

    async def test_get_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/user/me")
        assert response.status_code == 200
        response_data: dict = response.json()

        data = UserInDB.parse_obj(response_data)

        with authenticated_admin_api_client as client:
            response = client.get(f"/v1/user/{data.id}")
        assert response.status_code == 200
        response_data: dict = response.json()

        data_id = UserInDB.parse_obj(response_data)
        assert data == data_id

    async def test_get_admin_ids(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/user/admins")
        assert response.status_code == 200
        response_data: list[int] = response.json()
        assert len(response_data) >= 1
