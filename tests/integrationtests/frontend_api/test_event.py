# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from datetime import datetime, timedelta

import pytest
from fastapi.testclient import TestClient
from sqlmodel import Session

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.location import Location
from demos_backend.core.schemas.event import (
    EventAddressSearch,
    EventCreate,
    EventInDB,
    EventPerimeterSearch,
    EventUpdate,
)
from demos_backend.core.schemas.location import LocationCreate
from demos_backend.core.schemas.organisation import OrganisationInDB
from demos_backend.core.types import ReoccurringEventPeriod


@pytest.fixture
def location(mariadb_session: Session) -> Location:
    """Get a location."""
    location = crud.location.get_by_id(session=mariadb_session, location_id=1)
    assert location
    return location


@pytest.mark.asyncio
class TestEvent:
    async def test_new(self, authenticated_admin_api_client: TestClient):
        new_event = EventCreate(
            name="TestEvent",
            description="awesome event",
            start_time=datetime.now() + timedelta(days=1),
            end_time=datetime.now() + timedelta(days=3),
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            type_id=1,
            topic_ids=[1],
            organising_organisation_id=2,
        )
        with authenticated_admin_api_client as client:
            response = client.post("/v1/event/new", content=new_event.json())
        assert response.status_code == 200
        response_data: list = response.json()

        data = [EventInDB.parse_obj(event) for event in response_data]
        assert data[0].description == new_event.description
        assert data[0].organising_organisation_id == 2

    async def test_new_reoccuring(self, authenticated_admin_api_client: TestClient):
        new_event = EventCreate(
            name="TestEvent",
            description="awesome event",
            start_time=datetime.now() + timedelta(days=1),
            end_time=datetime.now() + timedelta(days=3),
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            type_id=1,
            topic_ids=[1],
            organising_organisation_id=2,
            reoccurring=ReoccurringEventPeriod.MONTHLY,
            reoccurring_count=4,
        )
        with authenticated_admin_api_client as client:
            response = client.post("/v1/event/new", content=new_event.json())
        assert response.status_code == 200
        response_data: list = response.json()

        data = [EventInDB.parse_obj(event) for event in response_data]
        assert len(data) == 4

    async def test_perimeter_search(
        self, authenticated_admin_api_client: TestClient, location: Location
    ):
        search_criteria = EventPerimeterSearch(
            latitude=location.latitude, longitude=location.longitude, radius_km=2
        )
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/event/perimeter-search", json=search_criteria.dict()
            )
        assert response
        assert response.status_code == 200
        response_data: list[dict] = response.json()
        data = [EventInDB.parse_obj(event) for event in response_data]
        assert len(data) > 0

    async def test_perimeter_search_count(
        self, authenticated_admin_api_client: TestClient, location: Location
    ):
        search_criteria = EventPerimeterSearch(
            name="Demo",
            latitude=location.latitude,
            longitude=location.longitude,
            radius_km=50,
        )
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/event/perimeter-search/count", json=search_criteria.dict()
            )
        assert response
        assert response.status_code == 200
        response_data: int = response.json()
        assert response_data > 0

    async def test_address_search(
        self, authenticated_admin_api_client: TestClient, location: Location
    ):
        search_criteria = EventAddressSearch(
            city=location.city, country=location.country, zip_code=""
        )
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/event/address-search",
                json=search_criteria.dict(),
                params={"address_language": "en"},
            )
        assert response
        assert response.status_code == 200
        response_data: list[dict] = response.json()
        data = [EventInDB.parse_obj(event) for event in response_data]
        assert len(data) > 0

    async def test_address_search_count(
        self, authenticated_admin_api_client: TestClient
    ):
        search_criteria = EventAddressSearch(city="", country="", zip_code="")
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/event/address-search/count",
                json=search_criteria.dict(),
                params={"address_language": "en"},
            )
        assert response
        assert response.status_code == 200
        response_data: int = response.json()
        assert response_data > 0

    async def test_get_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/event/1")
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventInDB.parse_obj(response_data)
        assert data.name == "DemoDemo1"

    async def test_get_organizer(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/event/1/organizer")
        assert response.status_code == 200
        response_data: dict = response.json()
        data = OrganisationInDB.parse_obj(response_data)
        assert data.name == "AdminBabos"

    async def test_get_co_organizers(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/event/1/co-organizers")
        assert response.status_code == 200
        response_data: list = response.json()
        data = [OrganisationInDB.parse_obj(org) for org in response_data]
        assert len(data) == 1

    async def test_update_by_id(self, authenticated_admin_api_client: TestClient):
        updated_event = EventUpdate(name="DemoDemoDemoDemo")
        with authenticated_admin_api_client as client:
            response = client.put("/v1/event/2", json=updated_event.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventInDB.parse_obj(response_data)
        assert data.name == updated_event.name

    async def test_delete_by_id(self, authenticated_admin_api_client: TestClient):
        delete_event = EventCreate(
            name="Delete",
            description="awesome event",
            start_time=datetime.now() + timedelta(days=1),
            end_time=datetime.now() + timedelta(days=3),
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            type_id=1,
            topic_ids=[1],
            organising_organisation_id=2,
        )
        with authenticated_admin_api_client as client:
            response = client.post("/v1/event/new", content=delete_event.json())
        assert response.status_code == 200
        response_data: list = response.json()
        delete_event = [EventInDB.parse_obj(event) for event in response_data]

        with authenticated_admin_api_client as client:
            response = client.delete(f"/v1/event/{delete_event[0].id}")
        assert response.status_code == 200
        assert response.text == '"OK"'

        with authenticated_admin_api_client as client:
            response = client.get(f"/v1/event/{delete_event[0].id}")
        assert response.status_code == 404
