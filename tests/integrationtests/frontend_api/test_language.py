# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient


@pytest.mark.asyncio
class TestLanguage:
    async def test_available_languages(
        self, authenticated_admin_api_client: TestClient
    ):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/language/available")
        assert response.status_code == 200
        response_data: list = response.json()
        assert "en" in response_data
        assert "de" in response_data
