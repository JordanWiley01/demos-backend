# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.schemas.event_type import (
    EventTypeCreate,
    EventTypeInDB,
    EventTypeUpdate,
)


@pytest.mark.asyncio
class TestEventType:
    async def test_list_all(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/event_type/",
            )
        assert response.status_code == 200
        response_data: list = response.json()
        data = [EventTypeInDB(**event_type) for event_type in response_data]
        assert len(data) > 0

    async def test_new(self, authenticated_admin_api_client: TestClient):
        new_event_type = EventTypeCreate(name_de="Krawall", name_en="Riots")
        with authenticated_admin_api_client as client:
            response = client.post("/v1/event_type/new", json=new_event_type.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventTypeInDB(**response_data)
        assert data.name_en == new_event_type.name_en
        assert data.name_de == new_event_type.name_de

    async def test_get_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/event_type/1")
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventTypeInDB(**response_data)
        assert data.id == 1

    async def test_update_by_id(self, authenticated_admin_api_client: TestClient):
        updated_event_type = EventTypeUpdate(name_en="Foo", name_de="Bar")
        with authenticated_admin_api_client as client:
            response = client.put("/v1/event_type/1", json=updated_event_type.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventTypeInDB(**response_data)
        assert data.name_en == updated_event_type.name_en
        assert data.name_de == updated_event_type.name_de

    async def test_delete(self, authenticated_admin_api_client: TestClient):
        new_event_type = EventTypeCreate(name_de="deletion", name_en="deletion")
        with authenticated_admin_api_client as client:
            response = client.post("/v1/event_type/new", json=new_event_type.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventTypeInDB(**response_data)

        authenticated_admin_api_client.delete(f"/v1/event_type/{data.id}")

        with authenticated_admin_api_client as client:
            response = client.get(f"/v1/event_type/{data.id}")

        assert response.status_code == 404
