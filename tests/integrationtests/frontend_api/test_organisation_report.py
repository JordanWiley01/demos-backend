# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.schemas.organisation_report import (
    OrganisationReportCreate,
    OrganisationReportInDB,
    OrganisationReportUpdate,
)
from demos_backend.core.types import ReportStatus


@pytest.mark.asyncio
class TestOrganisationReport:
    async def test_new(self, authenticated_admin_api_client: TestClient):
        new_report = OrganisationReportCreate(
            description="we smell funky and taste weird",
            reported_organisation_id=1,
            reporter_organisation_id=2,
        )
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/organisation_report/new", json=new_report.dict()
            )
        assert response.status_code == 200
        response_data: dict = response.json()

        data = OrganisationReportInDB.parse_obj(response_data)
        assert data.status == ReportStatus.PENDING
        assert data.reporter_user_id == 1

    async def test_get_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/organisation_report/1")
        assert response.status_code == 200
        response_data: dict = response.json()

        data = OrganisationReportInDB.parse_obj(response_data)
        assert data.status == ReportStatus.PENDING
        assert data.reporter_user_id == 1

    async def test_update_by_id(self, authenticated_admin_api_client: TestClient):
        new_data = OrganisationReportUpdate(status=ReportStatus.DENIED)
        with authenticated_admin_api_client as client:
            response = client.put("/v1/organisation_report/1", json=new_data.dict())
        assert response
        assert response.status_code == 200
        response_data: dict = response.json()

        data = OrganisationReportInDB.parse_obj(response_data)
        assert data.status == ReportStatus.DENIED
        assert data.reporter_user_id == 1
