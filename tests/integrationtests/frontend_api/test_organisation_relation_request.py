# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.schemas.location import LocationCreate
from demos_backend.core.schemas.organisation import OrganisationCreate, OrganisationInDB
from demos_backend.core.schemas.organisation_relation_request import (
    OrganisationRelationRequestCreate,
    OrganisationRelationRequestInDB,
    OrganisationRelationRequestUpdate,
)
from demos_backend.core.types import RequestStatus

NEW_ORG_ID = None


@pytest.mark.asyncio
class TestOrganisationRelationRequest:
    async def test_new(self, authenticated_admin_api_client: TestClient):
        new_org = OrganisationCreate(
            name="TestOrganisationRelationRequest",
            internal_point_of_contact="admin@test.org",
            type_id=1,
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            image_name="0" * 32,
            topic_ids=[1],
            description="description",
        )
        with authenticated_admin_api_client as client:
            response = client.post("/v1/organisation/new", json=new_org.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        new_org = OrganisationInDB.parse_obj(response_data)

        # approve org
        with authenticated_admin_api_client as client:
            response = client.put(
                f"/v1/organisation/{new_org.id}/approval_status",
                params={"approval_status": str(RequestStatus.APPROVED.value)},
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        new_org = OrganisationInDB.parse_obj(response_data)

        new_request = OrganisationRelationRequestCreate(
            organisation_requester_id=2, organisation_requested_id=new_org.id
        )
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/organisation_relation_request/new", json=new_request.dict()
            )
        assert response.status_code == 200
        response_data: dict = response.json()

        data = OrganisationRelationRequestInDB.parse_obj(response_data)
        assert data.organisation_requester.id == 2
        assert data.organisation_requested.id == new_org.id

        global NEW_ORG_ID
        NEW_ORG_ID = new_org.id

    async def test_get_by_orgs(self, authenticated_admin_api_client: TestClient):
        params = {
            "organisation_relation_requester_id": 2,
            "organisation_relation_requested_id": NEW_ORG_ID,
        }
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/organisation_relation_request/by_ids", params=params
            )
        assert response.status_code == 200
        response_data: dict = response.json()

        data = OrganisationRelationRequestInDB.parse_obj(response_data)

        assert data.organisation_requester.id == 2
        assert data.organisation_requested.id == NEW_ORG_ID

    async def test_update_by_orgs(self, authenticated_admin_api_client: TestClient):
        # create a request
        requester_org = OrganisationCreate(
            name="RequesterOrganisation",
            internal_point_of_contact="admin@test.org",
            type_id=1,
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            image_name="0" * 32,
            topic_ids=[1],
            description="description",
        )
        with authenticated_admin_api_client as client:
            response = client.post("/v1/organisation/new", json=requester_org.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        requester_org = OrganisationInDB.parse_obj(response_data)
        requested_org = OrganisationCreate(
            name="RequestedOrganisation",
            internal_point_of_contact="admin@test.org",
            type_id=1,
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            image_name="0" * 32,
            topic_ids=[1],
            description="description",
        )
        with authenticated_admin_api_client as client:
            response = client.post("/v1/organisation/new", json=requested_org.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        requested_org = OrganisationInDB.parse_obj(response_data)

        # approve orgs
        with authenticated_admin_api_client as client:
            response = client.put(
                f"/v1/organisation/{requester_org.id}/approval_status",
                params={"approval_status": str(RequestStatus.APPROVED.value)},
            )
        assert response.status_code == 200
        with authenticated_admin_api_client as client:
            response = client.put(
                f"/v1/organisation/{requested_org.id}/approval_status",
                params={"approval_status": str(RequestStatus.APPROVED.value)},
            )
        assert response.status_code == 200

        request = OrganisationRelationRequestCreate(
            organisation_requester_id=requester_org.id,
            organisation_requested_id=requested_org.id,
        )
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/organisation_relation_request/new", json=request.dict()
            )
        assert response.status_code == 200

        # approve the request
        params = {
            "organisation_relation_requester_id": requester_org.id,
            "organisation_relation_requested_id": requested_org.id,
        }
        request_update = OrganisationRelationRequestUpdate(
            status=RequestStatus.APPROVED
        )
        with authenticated_admin_api_client as client:
            response = client.put(
                "/v1/organisation_relation_request/by_ids",
                params=params,
                json=request_update.dict(),
            )
        assert response.status_code == 200
        response_data: dict = response.json()

        request = OrganisationRelationRequestInDB.parse_obj(response_data)

        assert request.status == RequestStatus.APPROVED

        # check the relation
        with authenticated_admin_api_client as client:
            response = client.get(
                f"/v1/organisation/{requester_org.id}/affiliated_organisations"
            )
        assert response.status_code == 200
        requester_relations: list[int] = response.json()
        with authenticated_admin_api_client as client:
            response = client.get(
                f"/v1/organisation/{requested_org.id}/affiliated_organisations"
            )
        assert response.status_code == 200
        requested_relations: list[int] = response.json()

        found = False
        for org_id in requester_relations:
            if org_id == requested_org.id:
                found = True
                break
        assert found

        found = False
        for org_id in requested_relations:
            if org_id == requester_org.id:
                found = True
                break
        assert found

        # deny the request
        params = {
            "organisation_relation_requester_id": requester_org.id,
            "organisation_relation_requested_id": requested_org.id,
        }
        request_update = OrganisationRelationRequestUpdate(status=RequestStatus.DENIED)
        with authenticated_admin_api_client as client:
            response = client.put(
                "/v1/organisation_relation_request/by_ids",
                params=params,
                json=request_update.dict(),
            )
        assert response.status_code == 200
        response_data: dict = response.json()

        request = OrganisationRelationRequestInDB.parse_obj(response_data)

        assert request.status == RequestStatus.DENIED

        # check the relation
        with authenticated_admin_api_client as client:
            response = client.get(
                f"/v1/organisation/{requester_org.id}/affiliated_organisations"
            )
        assert response.status_code == 200
        requester_relations: list[int] = response.json()
        with authenticated_admin_api_client as client:
            response = client.get(
                f"/v1/organisation/{requested_org.id}/affiliated_organisations"
            )
        assert response.status_code == 200
        requested_relations: list[int] = response.json()

        found = False
        for org_id in requester_relations:
            if org_id == requested_org.id:
                found = True
                break
        assert not found

        found = False
        for org_id in requested_relations:
            if org_id == requester_org.id:
                found = True
                break
        assert not found
