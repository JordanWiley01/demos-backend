# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.schemas.location import LocationCreate
from demos_backend.core.schemas.organisation import (
    OrganisationAddressSearch,
    OrganisationCreate,
    OrganisationInDB,
    OrganisationUpdateUser,
)
from demos_backend.core.schemas.user import UserInDB, UserMail
from demos_backend.core.types import RequestStatus
from demos_backend.services.frontend_api.exceptions import organisation_name_exists


@pytest.mark.asyncio
class TestOrganisation:
    async def test_new(self, authenticated_admin_api_client: TestClient):
        new_org = OrganisationCreate(
            name="TestOrganisation",
            internal_point_of_contact="admin@test.org",
            type_id=1,
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            image_name="0" * 32,
            topic_ids=[1],
            description="description",
        )
        with authenticated_admin_api_client as client:
            response = client.post("/v1/organisation/new", json=new_org.dict())
        assert response.status_code == 200
        response_data: dict = response.json()

        data = OrganisationInDB.parse_obj(response_data)
        assert data.name == new_org.name
        assert data.type.name_en == "Womenrights"

    async def test_duplicate_name(self, authenticated_admin_api_client: TestClient):
        duplicated_org = OrganisationCreate(
            name="TestOrganisationDuplicate",
            internal_point_of_contact="admin@test.org",
            type_id=1,
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            image_name="0" * 32,
            topic_ids=[1],
            description="description",
        )

        duplicated_org2 = OrganisationCreate(
            name="TestOrganisationDuplicate",
            internal_point_of_contact="admin@test.org",
            type_id=1,
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            image_name="0" * 32,
            topic_ids=[1],
            description="description",
        )

        with authenticated_admin_api_client as client:
            response = client.post("/v1/organisation/new", json=duplicated_org.dict())
        assert response.status_code == 200

        with authenticated_admin_api_client as client:
            response = client.post("/v1/organisation/new", json=duplicated_org2.dict())
        assert response.status_code == 409
        data = response.json()
        assert data["detail"] == organisation_name_exists.detail

    async def test_search(self, authenticated_admin_api_client: TestClient):
        search = OrganisationAddressSearch(
            topic_ids=[1], city="Be", country="", zip_code=""
        )
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/organisation/address-search",
                json=search.dict(),
                params={"address_language": "en"},
            )
        assert response.status_code == 200
        response_data: list[int] = response.json()
        assert len(response_data) > 0

    async def test_get_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/organisation/1")
        assert response.status_code == 200
        response_data: dict = response.json()
        data = OrganisationInDB.parse_obj(response_data)
        assert data.name == "Org User Org"

    async def test_update_by_id(self, authenticated_admin_api_client: TestClient):
        updated_org = OrganisationUpdateUser(name="OberAdminBabos")
        with authenticated_admin_api_client as client:
            response = client.put("/v1/organisation/2", json=updated_org.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = OrganisationInDB.parse_obj(response_data)
        assert data.name == updated_org.name

    async def test_list_admins(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/organisation/2/admins")
        assert response.status_code == 200
        response_data: list = response.json()
        data = [UserInDB.parse_obj(user) for user in response_data]
        assert data[0].id == 1

    async def test_internal_contact(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/organisation/2/internal_contact")
        assert response.status_code == 200
        assert response.text == '"admin@demonstrations.org"'

    async def test_add_admin(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/user/2")
        assert response.status_code == 200
        response_data: dict = response.json()

        new_admin = UserInDB.parse_obj(response_data)

        with authenticated_admin_api_client as client:
            response = client.put(
                "/v1/organisation/2/add_admin",
                json=UserMail(email=new_admin.email).dict(),
            )
        assert response.status_code == 200
        response_data: list = response.json()
        data = [UserInDB.parse_obj(user) for user in response_data]
        assert new_admin in data

    async def test_remove_admin(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/user/2")
        assert response.status_code == 200
        response_data: dict = response.json()

        removed_admin = UserInDB.parse_obj(response_data)

        with authenticated_admin_api_client as client:
            response = client.put(f"/v1/organisation/2/remove_admin/{removed_admin.id}")
        assert response.status_code == 200
        response_data: list = response.json()
        data = [UserInDB.parse_obj(user) for user in response_data]
        assert removed_admin not in data

    async def test_approve(self, authenticated_admin_api_client: TestClient):
        new_org = OrganisationCreate(
            name="TestOrganisationFooBar",
            internal_point_of_contact="admin@test.org",
            type_id=1,
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            image_name="0" * 32,
            topic_ids=[1],
            description="description",
        )
        with authenticated_admin_api_client as client:
            response = client.post("/v1/organisation/new", json=new_org.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = OrganisationInDB.parse_obj(response_data)

        # approve org
        with authenticated_admin_api_client as client:
            response = client.put(
                f"/v1/organisation/{data.id}/approval_status",
                params={"approval_status": str(RequestStatus.APPROVED.value)},
            )
        assert response.status_code == 200

    async def test_future_events(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/organisation/1/future_events")
        assert response.status_code == 200
        response_data: list[int] = response.json()
        assert len(response_data) >= 1
        assert len(response_data) <= 25

    async def test_past_events(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/organisation/1/past_events")
        assert response.status_code == 200
        response_data: list[int] = response.json()
        assert len(response_data) >= 1
        assert len(response_data) <= 25

    async def test_editable_events(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/organisation/1/editable_events")
        assert response.status_code == 200
        response_data: list[int] = response.json()
        assert len(response_data) >= 1
        assert len(response_data) <= 25

    async def test_affiliated_organisations(
        self, authenticated_admin_api_client: TestClient
    ):
        with authenticated_admin_api_client as client:
            response = client.get("/v1/organisation/2/affiliated_organisations")
        assert response.status_code == 200
        response_data: list[int] = response.json()
        assert len(response_data) > 0

    async def test_list_all(self, authenticated_admin_api_client: TestClient):
        params = {
            "limit": 25,
            "offset": 0,
            "approved": str(RequestStatus.APPROVED.value),
        }
        with authenticated_admin_api_client as client:
            response = client.get("/v1/organisation/", params=params)
        assert response.status_code == 200
        response_data: list[int] = response.json()
        assert len(response_data) > 0
