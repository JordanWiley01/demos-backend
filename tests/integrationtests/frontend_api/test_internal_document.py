# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.types import AvailableLanguages, InternalDocumentType


@pytest.mark.asyncio
class TestInternalDocument:
    async def test_upload(self, authenticated_admin_api_client: TestClient):
        # the key 'file_bytes' comes from the arg name in the route
        files = {
            "file_bytes": open("tests/integrationtests/data/test_document.pdf", "rb")
        }
        params = {
            "language": AvailableLanguages.GERMAN.value,
            "document_type": InternalDocumentType.FINANCIAL_REPORT.value,
            "display_name": "test document",
        }
        with authenticated_admin_api_client as client:
            response = client.post("/v1/document/upload", files=files, params=params)
        assert response.status_code == 200
        assert '"OK"' == response.text

    async def test_get_by_name(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/document/test_document.pdf",
            )
        downloaded_document_size = len(response.read())
        assert response.status_code == 200
        assert downloaded_document_size > 0
        document = open("tests/integrationtests/data/test_document.pdf", "rb").read()
        assert downloaded_document_size == len(document)

    async def test_delete_by_name(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.delete(
                "/v1/document/test_document.pdf",
            )
        assert response.status_code == 200
        assert '"OK"' == response.text

    async def test_get_available_documents(
        self, unauthenticated_api_client: TestClient
    ):
        with unauthenticated_api_client as client:
            response = client.get("/v1/document/available")
        response_data: dict[str, str] = response.json()
        assert len(response_data) >= 2
