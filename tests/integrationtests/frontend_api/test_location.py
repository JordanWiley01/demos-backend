# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient


@pytest.mark.asyncio
class TestLocation:
    async def test_countries_create(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/location/available-countries/create", params={"language": "en"}
            )
        assert response.status_code == 200
        response_data: list = response.json()
        assert "Germany" in response_data

    async def test_countries_search(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/location/available-countries/search", params={"language": "de"}
            )
        assert response.status_code == 200
        response_data: list = response.json()
        assert "Deutschland" in response_data
