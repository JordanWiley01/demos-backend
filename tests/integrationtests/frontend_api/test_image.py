# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

IMAGE_ID = None


@pytest.mark.asyncio
class TestImage:
    async def test_upload(self, authenticated_admin_api_client: TestClient):
        # the key 'file_bytes' comes from the arg name in the route
        files = {"file_bytes": open("tests/integrationtests/data/test_image.png", "rb")}
        with authenticated_admin_api_client as client:
            response = client.post("/v1/image/upload", files=files)
        assert response.status_code == 200
        response_data: dict = response.json()
        assert "image_name" in response_data
        assert len(response_data["image_name"]) == 32
        global IMAGE_ID
        IMAGE_ID = response_data["image_name"]

    async def test_get_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                f"/v1/image/{IMAGE_ID}",
            )
        downloaded_img_size = len(response.read())
        assert response.status_code == 200
        assert downloaded_img_size > 0
        img = open("tests/integrationtests/data/test_image.png", "rb").read()
        assert downloaded_img_size == len(img)
