# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.schemas.event_type import EventTypeInDB
from demos_backend.core.schemas.organisation_type import (
    OrganisationTypeCreate,
    OrganisationTypeInDB,
    OrganisationTypeUpdate,
)


@pytest.mark.asyncio
class TestOrganisationType:
    async def test_list_all(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/organisation_type/",
            )
        assert response.status_code == 200
        response_data: list = response.json()
        data = [OrganisationTypeInDB(**org_type) for org_type in response_data]
        assert len(data) > 0

    async def test_get_by_id(self, authenticated_admin_api_client: TestClient):
        with authenticated_admin_api_client as client:
            response = client.get(
                "/v1/organisation_type/1",
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = OrganisationTypeInDB(**response_data)
        assert data.id == 1

    async def test_new(self, authenticated_admin_api_client: TestClient):
        new_org_type = OrganisationTypeCreate(name_en="Pack", name_de="Rudel")
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/organisation_type/new", json=new_org_type.dict()
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = OrganisationTypeInDB(**response_data)
        assert data.name_en == new_org_type.name_en
        assert data.name_de == new_org_type.name_de

    async def test_update(self, authenticated_admin_api_client: TestClient):
        updated_org_type = OrganisationTypeUpdate(name_en="Blub", name_de="Blah")
        with authenticated_admin_api_client as client:
            response = client.put(
                "/v1/organisation_type/1", json=updated_org_type.dict()
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = OrganisationTypeInDB(**response_data)
        assert data.name_en == updated_org_type.name_en
        assert data.name_de == updated_org_type.name_de

    async def test_delete(self, authenticated_admin_api_client: TestClient):
        new_event_type = OrganisationTypeCreate(name_de="deletion", name_en="deletion")
        with authenticated_admin_api_client as client:
            response = client.post(
                "/v1/organisation_type/new", json=new_event_type.dict()
            )
        assert response.status_code == 200
        response_data: dict = response.json()
        data = EventTypeInDB(**response_data)

        authenticated_admin_api_client.delete(f"/v1/organisation_type/{data.id}")

        with authenticated_admin_api_client as client:
            response = client.get(f"/v1/organisation_type/{data.id}")

        assert response.status_code == 404
