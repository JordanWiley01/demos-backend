# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pytest
from fastapi.testclient import TestClient

from demos_backend.core.schemas.token import Token


@pytest.mark.asyncio
class TestCrawlerUser:
    async def test_get_token(self, unauthenticated_api_client: TestClient):
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        content = (
            "username=solo_user@demonstrations.org&password=solo_usersolo_usersolo_user"
        )
        with unauthenticated_api_client as client:
            response = client.post(
                "/v1/user/get_token", headers=headers, content=content
            )

        assert response.status_code == 200
        response_data: dict = response.json()
        token = Token.parse_obj(response_data)
        assert token.access_token
