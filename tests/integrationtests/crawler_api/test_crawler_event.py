# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import time
from datetime import datetime, timedelta

import pytest
from fastapi.testclient import TestClient
from sqlmodel import Session

from demos_backend.core.configuration import Configuration
from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.location import Location
from demos_backend.core.schemas.event import (
    EventCreateCrawler,
    EventInDB,
)
from demos_backend.core.schemas.location import LocationCreate, LocationCreateCrawler
from demos_backend.core.schemas.organisation import OrganisationCreate, OrganisationInDB
from demos_backend.core.types import RequestStatus


@pytest.fixture
def location(mariadb_session: Session) -> Location:
    """Get a location."""
    location = crud.location.get_by_id(session=mariadb_session, location_id=1)
    assert location
    return location


@pytest.mark.asyncio
class TestCrawlerEvent:
    async def test_new_crawler(
        self,
        authenticated_crawler_api_client: TestClient,
        authenticated_admin_api_client: TestClient,
    ):
        new_org = OrganisationCreate(
            name="CrawlerOrg",
            internal_point_of_contact="crawler@test.org",
            type_id=1,
            location=LocationCreate(
                country="Deutschland",
                city="Berlin",
                street="Pariser Platz",
                zip_code="10117",
                house_number="1",
            ),
            image_name="0" * 32,
            topic_ids=[1],
            description="description",
        )
        with authenticated_admin_api_client as client:
            response = client.post("/v1/organisation/new", json=new_org.dict())
        assert response.status_code == 200
        response_data: dict = response.json()
        data = OrganisationInDB.parse_obj(response_data)

        with authenticated_admin_api_client as client:
            response = client.put(
                f"/v1/organisation/{data.id}/approval_status",
                params={"approval_status": str(RequestStatus.APPROVED.value)},
            )
        assert response.status_code == 200

        new_events = [
            EventCreateCrawler(
                name="TestEvent",
                description="awesome event",
                start_time=datetime.now() + timedelta(days=1),
                end_time=datetime.now() + timedelta(days=3),
                location=LocationCreateCrawler(
                    country="Deutschland",
                    city="Berlin",
                    street="Pariser Platz",
                    zip_code="10117",
                ),
                type_id=1,
                topic_ids=[1],
                organising_organisation_id=data.id,
                raw_location="Pariser Platz 1, 10117 Berlin",
            ),
            EventCreateCrawler(
                name="TestEvent2",
                description="double awesome event",
                start_time=datetime.now() + timedelta(days=1),
                end_time=datetime.now() + timedelta(days=3),
                location=LocationCreateCrawler(
                    country="Deutschland",
                    city="Berlin",
                    street="Pariser Platz",
                    zip_code="10117",
                ),
                type_id=1,
                topic_ids=[1],
                organising_organisation_id=data.id,
                raw_location="Pariser Platz 1, 10117 Berlin",
            ),
        ]
        data = "["
        for event in new_events:
            data += event.json() + ", "
        data = data[:-2]
        data += "]"
        with authenticated_crawler_api_client as client:
            response = client.post("/v1/event/crawler/new", content=data)
        assert response
        assert response.status_code == 200
        response_data: list[dict] = response.json()
        data = [EventInDB.parse_obj(event) for event in response_data]
        assert len(data) == 2

    @pytest.mark.xfail(reason="unknown")
    async def test_crawler_altered_event(
        self,
        authenticated_crawler_api_client: TestClient,
        authenticated_admin_api_client: TestClient,
        loaded_config: Configuration,
    ):
        crawled_events = []
        now = datetime.now()
        for i in range(10):
            if i < 3:
                crawled_events.append(
                    EventCreateCrawler(
                        name=str(i),
                        description=f"event {i}",
                        start_time=now + timedelta(days=1),
                        end_time=now + timedelta(days=3),
                        location=LocationCreateCrawler(
                            country="Deutschland",
                            city="Berlin",
                            street="Pariser Platz",
                            zip_code="10117",
                        ),
                        type_id=1,
                        topic_ids=[1],
                        organising_organisation_id=2,
                        raw_location="Pariser Platz 1, 10117 Berlin",
                    )
                )
            elif i < 5:
                crawled_events.append(
                    EventCreateCrawler(
                        name=str(i),
                        description=f"event {i}",
                        start_time=now + timedelta(days=7),
                        end_time=now + timedelta(days=8),
                        location=LocationCreateCrawler(
                            country="Deutschland",
                            city="Berlin",
                            street="Pariser Platz",
                            zip_code="10117",
                        ),
                        type_id=1,
                        topic_ids=[1],
                        organising_organisation_id=2,
                        raw_location="Pariser Platz 1, 10117 Berlin",
                    )
                )
            else:
                crawled_events.append(
                    EventCreateCrawler(
                        name=str(i),
                        description=f"event {i}",
                        start_time=now + timedelta(days=13),
                        end_time=now + timedelta(days=15),
                        location=LocationCreateCrawler(
                            country="Deutschland",
                            city="Berlin",
                            street="Pariser Platz",
                            zip_code="10117",
                        ),
                        type_id=1,
                        topic_ids=[1],
                        organising_organisation_id=2,
                        raw_location="Pariser Platz 1, 10117 Berlin",
                    )
                )
        data = "["
        for event in crawled_events:
            data += event.json() + ", "
        data = data[:-2]
        data += "]"
        with authenticated_crawler_api_client as client:
            response = client.post("/v1/event/crawler/new", content=data)
        assert response.status_code == 200
        response_data = response.json()
        parsed_data = [EventInDB(**data) for data in response_data]

        for event in parsed_data:
            with authenticated_admin_api_client as client:
                response = client.get(f"/v1/event/{event.id}")
            assert response.status_code == 200

        removed_event_schema = crawled_events.pop()
        removed_event_model = None
        for event in parsed_data:
            if event.name == removed_event_schema.name:
                removed_event_model = event
                parsed_data.pop()
                break
        assert removed_event_model is not None

        data = "["
        for event in crawled_events:
            data += event.json() + ", "
        data = data[:-2]
        data += "]"

        logging.error("sleeping %s s", 2 * 60 * loaded_config.CRAWLER_INTERVAL_MINUTES)
        time.sleep(2 * 60 * loaded_config.CRAWLER_INTERVAL_MINUTES)

        with authenticated_crawler_api_client as client:
            response = client.post("/v1/event/crawler/new", content=data)
        assert response.status_code == 200

        with authenticated_crawler_api_client as client:
            response = client.post("/v1/event/cleanup/2", content=data)
        assert response.status_code == 200

        time.sleep(5)

        with authenticated_admin_api_client as client:
            response = client.get(f"/v1/event/{removed_event_model.id}")
        assert response.status_code == 404

        for event in parsed_data:
            with authenticated_admin_api_client as client:
                response = client.get(f"/v1/event/{event.id}")
            assert response.status_code == 200
