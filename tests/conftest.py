# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import asyncio
import logging
import os
from pathlib import Path
from uuid import uuid4

import pytest
from alembic.command import upgrade
from alembic.config import Config
from fastapi.testclient import TestClient
from pytest_docker_tools import container, fetch
from pytest_docker_tools.wrappers import Container
from sqlmodel import Session

from demos_backend.core.configuration import Configuration, get_config
from demos_backend.core.databases.mariadb.session import get_session
from demos_backend.core.schemas.token import Token
from demos_backend.services.crawler_api import crawler_api
from demos_backend.services.frontend_api import frontend_api

config = get_config()
config_path = Path(__file__).parent / "config"


def alembic_migration():
    logging.info("migrating db with alembic")
    root_path = Path(__file__).parent.parent
    ini_path = root_path / "alembic.ini"
    cfg = Config(file_=str(ini_path))
    upgrade(cfg, "head")


mongodb_image = fetch(repository="mongo:latest")
mongodb = container(
    name=f"mongodb-demos-test-{uuid4()}",
    image="{mongodb_image.id}",
    environment={
        "MONGO_INITDB_ROOT_USERNAME": config.MONGODB_USERNAME,
        "MONGO_INITDB_ROOT_PASSWORD": config.MONGODB_PASSWORD,
    },
    ports={27017: 27017},
    scope="session",
)
mariadb_image = fetch(repository="mariadb:latest")
mariadb = container(
    name=f"mariadb-demos-test-{uuid4()}",
    image="{mariadb_image.id}",
    environment={
        "MARIADB_ROOT_PASSWORD": config.MARIADB_PASSWORD,
        "MARIADB_USER": config.MARIADB_USERNAME,
        "MARIADB_PASSWORD": config.MARIADB_PASSWORD,
        "MARIADB_DATABASE": config.MARIADB_DATABASE,
    },
    ports={3306: 3306},
    scope="session",
)


@pytest.fixture(scope="session")
def event_loop():
    loop = None
    try:
        loop = asyncio.get_event_loop()
        yield loop
    finally:
        if loop:
            loop.close()


@pytest.fixture(scope="session")
def loaded_config() -> Configuration:
    """Provide a config for the application."""
    return config


@pytest.fixture(scope="session")
def mariadb_session(mariadb_service: Container) -> Session:
    """Get a reference to the database session."""
    try:
        session = get_session()
        yield session
    finally:
        session.close()


@pytest.fixture(scope="session")
async def mongodb_service(mongodb: Container):
    """Ensure mongodb service."""
    ip, port = mongodb.get_addr("27017/tcp")
    config.MONGODB_HOSTNAME = ip
    config.MONGODB_PORT = port
    os.environ["DEMOS_MONGODB_HOSTNAME"] = config.MONGODB_HOSTNAME
    os.environ["DEMOS_MONGODB_PORT"] = str(config.MONGODB_PORT)

    from demos_backend.core.databases.mongodb import initialize_mongo

    await initialize_mongo(test_mode=True)
    return mongodb


@pytest.fixture(scope="session")
async def mariadb_service(mariadb: Container):
    """Ensure mariadb service."""
    ip, port = mariadb.get_addr("3306/tcp")
    config.MARIADB_HOSTNAME = ip
    config.MARIADB_PORT = port
    os.environ["DEMOS_MARIADB_HOSTNAME"] = config.MARIADB_HOSTNAME
    os.environ["DEMOS_MARIADB_PORT"] = str(config.MARIADB_PORT)

    from demos_backend.core.databases.mariadb.initialize_data import init_mariadb_data

    alembic_migration()
    await init_mariadb_data(test_mode=True)
    return mariadb


@pytest.fixture(scope="session")
async def unauthenticated_api_client(
    mongodb_service: Container, mariadb_service: Container
) -> TestClient:
    """Provide a frontend frontend_api client."""
    return TestClient(
        app=frontend_api,
        base_url="http://frontend-api",
    )


@pytest.fixture(scope="session")
async def delete_user_api_token(
    unauthenticated_api_client: TestClient,
    loaded_config: Configuration,
) -> str:
    """Provide a valid token for frontend frontend_api requests."""
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    content = (
        "username=delete_user@demonstrations.org"
        "&password=delete_userdelete_userdelete_user"
    )
    with unauthenticated_api_client as client:
        response = client.post("/v1/user/get_token", headers=headers, content=content)

    if response.status_code != 200:
        raise RuntimeError("Unauthenticated delete_user")

    token = Token.parse_obj(response.json())

    return token.access_token


@pytest.fixture(scope="session")
async def crawler_api_token(
    unauthenticated_api_client: TestClient,
    loaded_config: Configuration,
) -> str:
    """Provide a valid token for frontend frontend_api requests."""
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    content = (
        f"username={loaded_config.CRAWLER_USER_MAIL}"
        f"&password={loaded_config.CRAWLER_USER_PASSWORD}"
    )
    with unauthenticated_api_client as client:
        response = client.post("/v1/user/get_token", headers=headers, content=content)

    if response.status_code != 200:
        raise RuntimeError("Unauthenticated Crawler")

    token = Token.parse_obj(response.json())

    return token.access_token


@pytest.fixture(scope="session")
async def admin_api_token(
    unauthenticated_api_client: TestClient,
    loaded_config: Configuration,
) -> str:
    """Provide a valid token for frontend frontend_api requests."""
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    content = (
        f"username={loaded_config.DEFAULT_USER_MAIL}"
        f"&password={loaded_config.DEFAULT_USER_PASSWORD}"
    )
    with unauthenticated_api_client as client:
        response = client.post("/v1/user/get_token", headers=headers, content=content)

    if response.status_code != 200:
        raise RuntimeError("Unauthenticated Admin")

    token = Token.parse_obj(response.json())

    return token.access_token


@pytest.fixture(scope="session")
async def authenticated_admin_api_client(
    mongodb_service: Container, mariadb_service: Container, admin_api_token: str
) -> TestClient:
    """Provide a preconfigured and authenticated frontend_api client."""
    client = TestClient(
        app=frontend_api,
        base_url="http://frontend-api",
    )
    client.headers = {"Authorization": f"Bearer {admin_api_token}"}
    return client


@pytest.fixture(scope="session")
async def authenticated_crawler_api_client(
    mongodb_service: Container, mariadb_service: Container, crawler_api_token: str
) -> TestClient:
    """Provide a preconfigured and authenticated frontend_api client."""
    client = TestClient(
        app=crawler_api,
        base_url="http://crawler-api",
    )
    client.headers = {"Authorization": f"Bearer {crawler_api_token}"}
    return client


@pytest.fixture(scope="session")
async def authenticated_delete_user_api_client(
    mongodb_service: Container, mariadb_service: Container, delete_user_api_token: str
) -> TestClient:
    """Provide a preconfigured and authenticated frontend_api client."""
    client = TestClient(
        app=frontend_api,
        base_url="http://frontend-api",
    )
    client.headers = {"Authorization": f"Bearer {delete_user_api_token}"}
    return client
