# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from demos_backend.core.schemas.location import LocationCreate, LocationCreateCrawler
from demos_backend.core.util import (
    get_geolocation_from_crawler_location_create,
    get_geolocation_from_location_create,
    get_random_uid,
)


class TestRandomUID:
    async def test_get_random_uid(self):
        uid = get_random_uid()
        assert len(uid) == 32
        uid = get_random_uid(32)
        assert len(uid) == 64
        uid1 = get_random_uid()
        uid2 = get_random_uid()
        assert uid1 != uid2


class TestGeoCoder:
    async def test_get_lat_long(self):
        location_in = LocationCreate(
            country="Germany",
            city="Berlin",
            street="Rathausstraße",
            zip_code="10178",
            house_number="15",
        )
        location = get_geolocation_from_location_create(location_in)
        assert location.country == location_in.country
        assert location.city == location_in.city
        assert location.zip_code == location_in.zip_code
        assert location.street == location_in.street
        assert location.house_number == location_in.house_number
        assert location.latitude is not None
        assert location.longitude is not None

    async def test_get_addr(self):
        location_in = LocationCreate(
            latitude=52.487567949868144,
            longitude=13.469164568232088,
        )
        location = get_geolocation_from_location_create(location_in)
        assert location.country is not None
        assert location.city is not None
        assert location.zip_code is not None
        assert location.street is not None
        assert location.house_number is not None
        assert location.latitude == location_in.latitude
        assert location.longitude == location_in.longitude

    async def test_get_addr_crawler(self):
        location_in = LocationCreateCrawler(
            country="Deutschland", city="Berlin", street="Gneisenaustraße"
        )
        location = get_geolocation_from_crawler_location_create(location_in)
        assert location.country is not None
        assert location.city is not None
        assert location.zip_code is not None
        assert location.street is not None
        assert location.house_number is not None
        assert location.latitude != 0
        assert location.longitude != 0
