# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

.DEFAULT_GOAL := build
.PHONY: clean install install_pre_commit \
		build build_docker \
		lint \
		docker_network \
		run stop prepare_rerun

MONGO_USERNAME?=not_set
MONGO_PASSWORD?=not_set
DATABASE?=demosdb
DIRECTION?=
VOLUMES=$(docker volume ls -q --filter dangling=true)

clean :
	rm -rf .pytest_cache \
	rm -rf build \
	rm -rf dist \
	rm requirements*.txt \
	poetry run coverage erase

install:
	PIP_NO_BINARY="cffi" poetry install

install_pre_commit:
	poetry run pre-commit install

build:
	poetry build

lint:
	poetry run pre-commit run --all-files

stop:
	docker-compose -p demosbackend -f deployment/local.docker-compose.yml down --volumes --remove-orphans
	docker volume prune -f

run: stop docker_network
	docker-compose -p demosbackend -f deployment/local.docker-compose.yml up --build -d

test: stop
	poetry run py.test -vv --cov-report html --cov=demos_backend
	${BROWSER} htmlcov/index.html

docker_network:
	docker network create demos-application || true

prepare_rerun:
	make stop
	make run
	rm -rf ./api/alembic/versions/*.py
	poetry run demos-backend check-dependencies
	cd api && poetry run alembic revision -m "initial_db" --autogenerate && poetry run alembic upgrade head
	poetry run demos-backend initialize-databases

migration_beanie_create:
	if [ "$(FROM_VERSION)" = "not_set" ]; then \
        echo ""; \
        echo ""; \
        echo "FROM_VERSION not set"; \
        echo ""; \
        echo ""; \
        exit 1; \
    fi
	if [ "$(TO_VERSION)" = "not_set" ]; then \
        echo ""; \
        echo ""; \
        echo "TO_VERSION not set"; \
        echo ""; \
        echo ""; \
        exit 1; \
    fi
	poetry run beanie new-migration -n $(FROM_VERSION)-$(TO_VERSION) -p migrations/beanie/

migration_beanie_run:
	if [ "$(MONGO_USERNAME)" = "not_set" ]; then \
        echo ""; \
        echo ""; \
        echo "MONGO_USERNAME not set"; \
        echo ""; \
        echo ""; \
        exit 1; \
    fi
	if [ "$(MONGO_PASSWORD)" = "not_set" ]; then \
        echo ""; \
        echo ""; \
        echo "MONGO_PASSWORD not set"; \
        echo ""; \
        echo ""; \
        exit 1; \
    fi
	poetry run beanie migrate -uri 'mongodb://$(MONGO_USERNAME):$(MONGO_PASSWORD)@127.0.0.1' -db $(DATABASE) -p migrations/beanie/ $(DIRECTION)

migration_alembic_create:
	if [ "$(FROM_VERSION)" = "not_set" ]; then \
        echo ""; \
        echo ""; \
        echo "FROM_VERSION not set"; \
        echo ""; \
        echo ""; \
        exit 1; \
    fi
	if [ "$(TO_VERSION)" = "not_set" ]; then \
        echo ""; \
        echo ""; \
        echo "TO_VERSION not set"; \
        echo ""; \
        echo ""; \
        exit 1; \
    fi
	poetry run alembic revision --autogenerate -m '$(FROM_VERSION)-$(TO_VERSION)'

migration_alembic_run:
	poetry run alembic upgrade head
