<!--
SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Demos Backend

This project provides the backend for the demos web application and mobile app.
It provides a [REST API](https://www.ibm.com/cloud/learn/rest-apis) to interact with.

---

## Table of Content

* [Requirements](#requirements)
* [Development](#development)
  * [Setup](#setup)
  * [Build](#build)
  * [Build](#run)
* [Author Information](#author-information)
* [Contribute](#contribute)
* [License](#license)

---

## Requirements

To run services natively, you need to have Python and several libraries installed.
Please see the [pyproject.toml](pyproject.toml) for more information.
The main software used is:

* [poetry]() >= 1.1.14 for dependency management
* [pyenv]() >= 2.3.3 for Python version management

For running the services dockerized (recommended), you need to have installed:

* [docker](https://www.docker.com/) >= 20.10.10
* [docker-compose](https://docs.docker.com/compose/) >= 2.1.1

---

## Development

### Setup

We use `pre-commit` for linting and formatting.
To install the hooks locally do:

```sh
make install_pre_commit
# or
poetry run pre-commit install
```

### Run

To build and run the application docker image, you can run the following command:

```sh
  make run
# or
  docker network create demos-application || true
  docker-compose -f deployment/local.docker-compose.yml up --build -d
```

This will start the database (and inspector) as well as the API.
You can find the autodoc here: `http://localhost:3080/v1/docs`

---

## Author Information

_demos-backend_ was created by
[Demos Berlin e.V.](https://demonstrations.org/) and various voluntary [contributors](https://gitlab.com/demos-berlin-ev/demos-backend/activity).

---

## Contribute

If you want to contribute to this project, please see our [contribution guidelines](CONTRIBUTING.md)

---

## License

Copyright © 2023 Demos Berlin e.V. (https://demonstrations.org)

This work is licensed under the following license(s):
* Everything else is licensed under [AGPL-3.0-or-later](LICENSES/AGPL-3.0-or-later.txt)

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
