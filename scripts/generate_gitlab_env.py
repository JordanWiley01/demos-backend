# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This script generates all relevant gitlab ci vars."""
import json
import logging
import os
import secrets
from pathlib import Path

from demos_backend.core.configuration import Configuration, Environment

OUTPUT_PATH = Path(__file__).parent / "gitlab_env/"

STAGES = {"local", "staging", "production"}


SECRETS_TO_GENERATE = [
    "MARIADB_PASSWORD",
    "MONGODB_PASSWORD",
    "DEFAULT_USER_PASSWORD",
    "CRAWLER_USER_PASSWORD",
]

with open(Path(__file__).parent / "gitlab_env_base.json", "r", encoding="utf8") as file:
    BASE_VARS = json.loads(file.read())

if not os.path.isdir(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

for stage in STAGES:
    print(f"# Handling Stage {stage}")
    GENERATED_SECRETS = {}
    for secret_var in SECRETS_TO_GENERATE:
        file_path = OUTPUT_PATH / f"{secret_var}.{stage}"
        if file_path.exists():
            with open(file_path, "r", encoding="utf8") as file:
                GENERATED_SECRETS[secret_var] = file.read()
        else:
            with open(file_path, "w", encoding="utf8") as file:
                GENERATED_SECRETS[secret_var] = secrets.token_urlsafe(32)
                file.write(GENERATED_SECRETS[secret_var])

    config = Configuration()

    if stage == "staging":
        config.ENVIRONMENT = Environment.STAGING
        config.FRONTEND_BASE_DOMAIN = "https://staging.demonstrations.org"
    if stage == "production":
        config.ENVIRONMENT = Environment.PRODUCTION
        config.FRONTEND_BASE_DOMAIN = "https://demonstrations.org"

    if stage != "local":
        config.MONGODB_USERNAME = BASE_VARS["MONGODB_USERNAME"]
        config.MONGODB_PASSWORD = GENERATED_SECRETS["MONGODB_PASSWORD"]
        config.MARIADB_USERNAME = BASE_VARS["MARIADB_USERNAME"]
        config.MARIADB_PASSWORD = GENERATED_SECRETS["MARIADB_PASSWORD"]
        config.DEFAULT_USER_PASSWORD = GENERATED_SECRETS["DEFAULT_USER_PASSWORD"]
        config.CRAWLER_USER_PASSWORD = GENERATED_SECRETS["CRAWLER_USER_PASSWORD"]
        config.LOG_LEVEL = logging.INFO

    config.write_config_to_file(file=OUTPUT_PATH / f"DEMOS_ENV.{stage}")
