# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import csv
from pathlib import Path

topics_path = Path(__file__).parent / "legacy_data/Topics.csv"
topics_template = "initial_topics = [\n"

with open(topics_path, "r") as csv_file:
    reader = csv.reader(csv_file)
    first_row = True
    for row in reader:
        if first_row:
            first_row = False
            continue
        cleaned_row = []
        for item in row:
            item = item.lstrip().rstrip()
            cleaned_row.append(item)
        topics_template += f'    ("{cleaned_row[0]}","{cleaned_row[1]}"),\n'

topics_template += "]"

event_types_path = Path(__file__).parent / "legacy_data/EventTypes.csv"
event_types_template = "initial_event_types = [\n"

with open(event_types_path, "r") as csv_file:
    reader = csv.reader(csv_file)
    first_row = True
    for row in reader:
        if first_row:
            first_row = False
            continue
        cleaned_row = []
        for item in row:
            item = item.lstrip().rstrip()
            cleaned_row.append(item)
        event_types_template += f'    ("{cleaned_row[0]}","{cleaned_row[1]}"),\n'

event_types_template += "]"

organisation_types_path = Path(__file__).parent / "legacy_data/OrganizationTypes.csv"
organisation_types_template = "initial_organisation_types = [\n"

with open(organisation_types_path, "r") as csv_file:
    reader = csv.reader(csv_file)
    first_row = True
    for row in reader:
        if first_row:
            first_row = False
            continue
        cleaned_row = []
        for item in row:
            item = item.lstrip().rstrip()
            cleaned_row.append(item)
        organisation_types_template += f'    ("{cleaned_row[0]}","{cleaned_row[1]}"),\n'

organisation_types_template += "]"

info_path = Path(__file__).parent / "legacy_data/InfoBase.csv"
info_template = "initial_info = [\n"

with open(info_path, "r") as csv_file:
    reader = csv.reader(csv_file)

    first_row = True
    for row in reader:
        if first_row:
            first_row = False
            continue
        cleaned_row = []
        for item in row:
            item = item.lstrip().rstrip()
            cleaned_row.append(item)
        info_template += (
            f'    ("{cleaned_row[0]}","{cleaned_row[1]}",'
            f'"{cleaned_row[2]}","{cleaned_row[3]}","{cleaned_row[8]}",'
            f'"{cleaned_row[9]}"),\n'
        )

info_template += "]"

out_path = (
    Path(__file__).parent.parent / "demos_backend/databases/mariadb/initial_data.py"
)

with open(out_path, "w") as file:
    file.write('"""This module contains initial data taken from the prototype."""\n\n')
    file.write(info_template)
    file.write("\n")
    file.write(topics_template)
    file.write("\n")
    file.write(event_types_template)
    file.write("\n")
    file.write(organisation_types_template)
    file.write("\n")
