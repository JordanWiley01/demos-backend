# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This is the cli entry point for demos-backend."""
import asyncio
import logging
import pathlib
from functools import wraps
from typing import Callable

import click
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from demos_backend.core.configuration import Configuration, get_config
from demos_backend.core.databases.mariadb.initialize_data import init_mariadb_data
from demos_backend.core.databases.mariadb.session import get_session
from demos_backend.core.logging import prepare_logger
from demos_backend.core.server import FastAPIServer
from demos_backend.services.crawler import run_scrappy

config = get_config()


def async_cmd(func: Callable):
    """Decorator to provide async click functions."""

    @wraps(func)
    def wrapper(*args: tuple, **kwargs: dict) -> None:
        """Wrapper function of the decorator."""
        return asyncio.run(func(*args, **kwargs))

    return wrapper


@click.group()
@click.option(
    "--config-file",
    "-c",
    type=click.Path(exists=True, dir_okay=False, path_type=pathlib.Path),
    help="Specify a .env file to load as config.",
)
@async_cmd
async def cli(config_file: pathlib.Path) -> None:
    """Start the Demos server backend.

    For configuring the application you have multiple options:

    - You can set environment variables, which will overwrite the default configuration.

    - You can use a .env file, which will overwrite the default configuration and
      environment variables.

    Args:
        config_file (Path): The path to a .env file to load.
    """
    # NOTE that click takes above documentation for generating help text
    # Thus the documentation refers to the application per se and not the
    # function (as it should)

    if config_file is not None:
        global config  # pylint: disable =W0603,C0103
        config = Configuration(_env_file=config_file)
        config.write_config_to_file()


@cli.command()
def configuration() -> None:
    """Print the configuration in .env style."""
    print(config.get_env())


@cli.command()
def check_dependencies() -> None:
    """Check the connection to the database."""
    prepare_logger(config=config, service_name="dependency_check")
    logging.info("Wait for mariadb startup")

    max_tries = 60 * 5  # 5 minutes
    wait_seconds = 1

    @retry(
        stop=stop_after_attempt(max_tries),
        wait=wait_fixed(wait_seconds),
        before=before_log(logging.getLogger(), logging.INFO),
        after=after_log(logging.getLogger(), logging.WARN),
    )
    def check_mariadb() -> None:
        try:
            session = get_session()
            # Try to create session to check if DB is awake
            session.execute("SELECT 1")
        except Exception as e:
            logging.error(e)
            raise e

    check_mariadb()

    logging.info("Mariadb started.")


@click.option(
    "--test-mode",
    "-t",
    is_flag=True,
    type=click.BOOL,
    help="Whether test data should be generated or not.",
)
@click.option(
    "--initial-data",
    "-i",
    is_flag=True,
    type=click.BOOL,
    help="Whether initial data should be added or not.",
)
@cli.command()
@async_cmd
async def initialize_databases(test_mode: bool, initial_data: bool) -> None:
    """Fill the database with initial data.

    Args:
        test_mode: flag if test data should be created.
        initial_data: flag if initial data should be added.
    """
    prepare_logger(config=config, service_name="database_preparation")
    if test_mode:
        logging.info("Initializing Databases with test data.")
    else:
        logging.info("Initializing Databases.")

    await init_mariadb_data(test_mode=test_mode, initial_data=initial_data)
    logging.debug("Initializing Mariadb complete")

    # pylint: disable=C0415
    from demos_backend.core.databases.mongodb import initialize_mongo

    await initialize_mongo(test_mode=test_mode, initial_data=initial_data)
    logging.debug("Initializing MongoDB complete")


@cli.command()
@async_cmd
async def migrate_data() -> None:
    """Migrate data."""
    prepare_logger(config=config, service_name="data_migration")
    pass


@cli.command()
@async_cmd
async def frontend_api() -> None:
    """Start the frontend API."""
    prepare_logger(config=config, service_name="frontend_api")

    logging.info(
        "You can find the API documentation under http://localhost:%s%s/docs",
        config.FRONTEND_API_PORT,
        config.API_V1_ROOT_PATH,
    )
    server = FastAPIServer(
        app_uri="demos_backend.services.frontend_api:frontend_api",
        hostname=config.FRONTEND_API_HOSTNAME,
        port=config.FRONTEND_API_PORT,
        config=config,
    )
    server.run()


@cli.command()
@async_cmd
async def notifications() -> None:
    """Start the notifications service."""
    prepare_logger(config=config, service_name="notifications")
    logging.info(
        "You can find the API documentation under http://localhost:%s%s/docs",
        config.NOTIFICATION_SERVICE_PORT,
        config.API_V1_ROOT_PATH,
    )
    server = FastAPIServer(
        app_uri="demos_backend.services.notifications:notification_service",
        hostname=config.NOTIFICATION_SERVICE_HOSTNAME,
        port=config.NOTIFICATION_SERVICE_PORT,
        config=config,
    )
    server.run()


@cli.command()
@async_cmd
async def crawler_api() -> None:
    """Start the crawler API."""
    prepare_logger(config=config, service_name="crawler_api")
    logging.info(
        "You can find the API documentation under http://localhost:%s%s/docs",
        config.CRAWLER_API_PORT,
        config.API_V1_ROOT_PATH,
    )
    server = FastAPIServer(
        app_uri="demos_backend.services.crawler_api:crawler_api",
        hostname=config.CRAWLER_API_HOSTNAME,
        port=config.CRAWLER_API_PORT,
        config=config,
    )
    server.run()


@cli.command()
@async_cmd
async def crawler() -> None:
    """Start the crawler."""
    prepare_logger(config=config, service_name="crawler")
    await run_scrappy(config=config)
