# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter
from starlette.background import BackgroundTasks

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.event import Event
from demos_backend.core.schemas.event import (
    EventCreateCrawler,
    EventInDB,
    EventUpdate,
)
from demos_backend.core.schemas.message import Message
from demos_backend.core.types import AvailableLanguages, RequestStatus
from demos_backend.core.util import (
    get_geolocation_from_crawler_location_create,
    translate_location_create,
)
from demos_backend.services.crawler_api.api.v1.dependencies import (
    CurrentActiveCrawler,
    LoadedConfig,
    MariaDBSession,
)
from demos_backend.services.crawler_api.background_tasks import cleanup_crawled_events
from demos_backend.services.crawler_api.exceptions import internal_issues, not_found

router = APIRouter(prefix="/event", tags=["Event"])


@router.post("/crawler/new", tags=["Crawler"], response_model=list[EventInDB])
async def new_crawler_event(
    current_active_crawler: CurrentActiveCrawler,
    schemas_in: list[EventCreateCrawler],
    session: MariaDBSession,
) -> list[Event]:
    """Create new crawler events."""
    if not schemas_in:
        return []
    event_models = []
    organisation = crud.organisation.get_by_id(
        session=session, organisation_id=schemas_in[0].organising_organisation_id
    )
    if not organisation:
        raise not_found

    for schema_in in schemas_in:
        preexisting_event = crud.event.get_preexisting_event(
            session=session,
            schema_in=schema_in,
        )

        if preexisting_event:
            crud.event.update(
                session=session,
                model_in=preexisting_event,
                schema_in=EventUpdate(),
                visible=True,
            )
            continue

        topics = crud.topic.get_by_ids(session=session, topic_ids=schema_in.topic_ids)
        co_organizers = crud.organisation.get_by_ids(
            session=session, organisation_ids=schema_in.co_organising_organisations_ids
        )
        if schema_in.location.address_language != AvailableLanguages.ENGLISH:
            schema_in.location = translate_location_create(
                schema_in=schema_in.location,
                src_language=schema_in.location.address_language,
            )
        try:
            schema_in.location = get_geolocation_from_crawler_location_create(
                location_create_crawler=schema_in.location
            )
        except RuntimeError as e:
            raise internal_issues from e

        new_location = crud.location.create(
            session=session, schema_in=schema_in.location
        )

        new_model = crud.event.create(
            session=session,
            schema_in=schema_in,
            creator=current_active_crawler,
            organising_organisation=organisation,
            topics=topics,
            co_organizers=co_organizers,
            location=new_location,
        )

        event_models.append(new_model)

    return event_models


@router.post("/cleanup/{organisation_id}", tags=["Crawler"], response_model=Message)
async def cleanup(
    organisation_id: int,
    current_active_crawler: CurrentActiveCrawler,
    config: LoadedConfig,
    session: MariaDBSession,
    background_tasks: BackgroundTasks,
) -> dict:
    """Create new crawler events."""
    approved = RequestStatus.APPROVED
    organisation = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id, approved=approved
    )
    if not organisation:
        raise not_found

    background_tasks.add_task(cleanup_crawled_events, session, organisation, config)
    return {"detail": "Starting cleanup."}
