# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all user status related API endpoints."""
from __future__ import annotations

from datetime import timedelta
from typing import Annotated

from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm

from demos_backend.core import security
from demos_backend.core.databases.mariadb import crud
from demos_backend.core.schemas.token import Token
from demos_backend.services.crawler_api.api.v1.dependencies import (
    LoadedConfig,
    MariaDBSession,
)
from demos_backend.services.crawler_api.exceptions import (
    invalid_oauth_credentials,
)

router = APIRouter(prefix="/user", tags=["User"])

FormData = Annotated[OAuth2PasswordRequestForm, Depends(OAuth2PasswordRequestForm)]


@router.post("/get_token", response_model=Token)
async def get_access_token_by_login(
    session: MariaDBSession,
    config: LoadedConfig,
    form_data: FormData,
) -> dict:
    """Retrieve an access token for the API by logging in.

    Args:
        form_data: The login form data.
        config: The applications' configuration.
        session: The database Session.
    """
    user = crud.user.authenticate(
        session=session, email=form_data.username, password=form_data.password
    )
    if not user:
        raise invalid_oauth_credentials

    access_token_expires: timedelta = timedelta(
        minutes=config.API_ACCESS_TOKEN_EXPIRE_MINUTES
    )
    access_token: str = security.create_access_token(
        subject=user.id,
        expires_delta=access_token_expires,
    )
    return {"access_token": access_token, "token_type": "Bearer"}
