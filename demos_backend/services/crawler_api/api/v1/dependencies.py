# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains shared dependencies for all V1 API endpoints."""
from __future__ import annotations

from typing import Annotated, Generator

from fastapi import Depends
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from jose.exceptions import ExpiredSignatureError
from pydantic import ValidationError
from sqlmodel import Session

from demos_backend.core import security
from demos_backend.core.configuration import Configuration, get_config
from demos_backend.core.databases.mariadb import crud
from demos_backend.core.databases.mariadb.session import get_session
from demos_backend.core.models.user import User
from demos_backend.core.schemas.token import TokenPayload
from demos_backend.services.crawler_api.exceptions import (
    inactive_user,
    insufficient_permissions,
    invalid_oauth_credentials,
    session_expired,
    unverified_user,
)

config = get_config()

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl=f"{config.API_V1_ROOT_PATH}/user/get_token"
)


def get_loaded_config() -> Configuration:
    """Get a reference to the loaded configuration."""
    return config


def get_mariadb_session() -> Generator:
    """Get a reference to the database session."""
    try:
        session = get_session()
        yield session
    finally:
        session.close()


def get_user_model(session: MariaDBSession, token: SubmittedToken) -> User:
    """Get user from JWT."""
    try:
        payload: dict = jwt.decode(
            token, config.API_SECRET_KEY, algorithms=[security.ALGORITHM]
        )
        token_data: TokenPayload = TokenPayload(**payload)
    except ExpiredSignatureError as e:
        raise session_expired from e
    except (jwt.JWTError, ValidationError) as e:
        raise invalid_oauth_credentials from e
    user = crud.user.get_by_id(session=session, user_id=token_data.sub)
    if user is None:
        raise invalid_oauth_credentials
    return user


def get_current_active_user(
    current_user: CurrentUser,
) -> User:
    """Get an active user from JWT."""
    if not current_user.active:
        raise inactive_user
    if not current_user.verified:
        raise unverified_user
    return current_user


def get_current_active_crawler(
    current_active_user: CurrentActiveUser,
) -> User:
    """Get an active crawler user from JWT."""
    if not current_active_user.username == config.CRAWLER_USER_NAME:
        raise insufficient_permissions
    return current_active_user


CurrentUser = Annotated[User, Depends(get_user_model)]
CurrentActiveUser = Annotated[User, Depends(get_current_active_user)]
CurrentActiveCrawler = Annotated[User, Depends(get_current_active_crawler)]
MariaDBSession = Annotated[Session, Depends(get_mariadb_session)]
LoadedConfig = Annotated[Configuration, Depends(get_loaded_config)]
SubmittedToken = Annotated[str, Depends(oauth2_scheme)]
