# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all routers for assembling an API."""

from fastapi import APIRouter

from .endpoints import event, user

v1_router = APIRouter()
v1_router.include_router(event.router)
v1_router.include_router(user.router)
