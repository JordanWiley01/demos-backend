# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module holds service specific exceptions."""
from fastapi import HTTPException, status

invalid_oauth_credentials = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Incorrect email or password.",
)

internal_issues = HTTPException(
    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    detail="Technical error - try again later.",
)

not_found = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND,
    detail="Whatever you are looking for, it is not here.",
)

session_expired = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Session expired, please log in again.",
    headers={"WWW-Authenticate": "Bearer"},
)

inactive_user = HTTPException(status_code=401, detail="Inactive user")

unverified_user = HTTPException(status_code=401, detail="Unverified user")

insufficient_permissions = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Insufficient permissions",
)
