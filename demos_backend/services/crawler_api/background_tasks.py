# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module holds service specific background tasks."""
import logging
from datetime import datetime, timedelta

from sqlmodel import Session

from demos_backend.core.configuration import Configuration
from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.organisation import Organisation
from demos_backend.core.schemas.event import EventSearch, EventUpdate


def cleanup_crawled_events(
    session: Session, organisation: Organisation, config: Configuration
):
    """Set all non updated events to invisible."""
    logging.debug("Cleaning up crawled events")
    now = datetime.now()
    last_updated_lower_bound = now - timedelta(minutes=config.CRAWLER_INTERVAL_MINUTES)
    events_to_check = crud.event.search(
        session=session,
        schema_in=EventSearch(
            organiser_ids=[organisation.id], start_time_start=datetime.now()
        ),
        offset=0,
        limit=1000000,
    )
    for event in events_to_check:
        if event.updated < last_updated_lower_bound:
            logging.debug("making event %s invisible", event.id)
            crud.event.update(
                session=session, model_in=event, schema_in=EventUpdate(), visible=False
            )
        else:
            logging.debug(
                "keeping event %s visible because %s < %s",
                event.id,
                last_updated_lower_bound,
                event.updated,
            )
