# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all routers for assembling an API."""

from fastapi import APIRouter

from .endpoints import (
    exceptions,
)

dev_router = APIRouter(prefix="/dev")
dev_router.include_router(exceptions.router)
