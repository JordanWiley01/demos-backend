# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.services.frontend_api.exceptions import (
    document_name_exists,
    email_exists,
    inactive_user,
    insufficient_permissions,
    internal_issues,
    invalid_oauth_credentials,
    not_anymore,
    not_found,
    organisation_name_exists,
    session_expired,
    topic_name_exists,
    type_name_exists,
    unverified_user,
    username_exists,
)

router = APIRouter(prefix="/exceptions", tags=["Exceptions"])


@router.post("/all", response_model=dict[str, str])
async def exception_translations() -> dict[str, str]:
    """Get all exception strings."""
    return {
        invalid_oauth_credentials.detail: invalid_oauth_credentials.detail,
        internal_issues.detail: internal_issues.detail,
        not_found.detail: not_found.detail,
        username_exists.detail: username_exists.detail,
        email_exists.detail: email_exists.detail,
        topic_name_exists.detail: topic_name_exists.detail,
        type_name_exists.detail: type_name_exists.detail,
        organisation_name_exists.detail: organisation_name_exists.detail,
        session_expired.detail: session_expired.detail,
        inactive_user.detail: inactive_user.detail,
        unverified_user.detail: unverified_user.detail,
        insufficient_permissions.detail: insufficient_permissions.detail,
        not_anymore.detail: not_anymore.detail,
        document_name_exists.detail: document_name_exists.detail,
    }
