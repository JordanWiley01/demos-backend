# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.event_report import EventReport
from demos_backend.core.schemas.event_report import (
    EventReportCreate,
    EventReportInDB,
    EventReportUpdate,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    CurrentActiveUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import internal_issues, not_found

router = APIRouter(prefix="/event_report", tags=["Event", "Report"])


@router.post("/new", response_model=EventReportInDB)
async def new_event_report(
    current_active_user: CurrentActiveUser,
    schema_in: EventReportCreate,
    session: MariaDBSession,
) -> EventReport:
    """Report an event."""
    new_model = crud.event_report.create(
        session=session, schema_in=schema_in, reporter_user_id=current_active_user.id
    )
    return new_model


@router.put("/{event_report_id}", response_model=EventReportInDB, tags=["Admin"])
async def update_event_report(
    current_active_super_user: CurrentActiveSuperUser,
    event_report_id: int,
    schema_in: EventReportUpdate,
    session: MariaDBSession,
) -> EventReport:
    """Update an event report by ID."""
    model_to_update = crud.event_report.get_by_id(
        session=session, report_id=event_report_id
    )
    if not model_to_update:
        raise not_found

    updated_model = crud.event_report.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.get("/{event_report_id}", response_model=EventReportInDB, tags=["Admin"])
async def get_event_report_by_id(
    current_active_super_user: CurrentActiveSuperUser,
    event_report_id: int,
    session: MariaDBSession,
) -> EventReport:
    """Get an event report by ID."""
    event_report = crud.event_report.get_by_id(
        session=session, report_id=event_report_id
    )
    if not event_report:
        raise not_found
    return event_report
