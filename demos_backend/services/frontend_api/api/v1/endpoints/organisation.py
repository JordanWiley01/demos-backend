# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all organisation status related API endpoints."""
from __future__ import annotations

from datetime import datetime
from operator import attrgetter

from fastapi import APIRouter
from pydantic import conint

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.organisation import Organisation
from demos_backend.core.models.user import User
from demos_backend.core.schemas.event import EventSearch
from demos_backend.core.schemas.organisation import (
    OrganisationAddressSearch,
    OrganisationCreate,
    OrganisationInDB,
    OrganisationUpdateAdmin,
    OrganisationUpdateUser,
)
from demos_backend.core.schemas.user import UserInDB, UserMail
from demos_backend.core.types import AvailableLanguages, RequestStatus
from demos_backend.core.util import (
    get_geolocation_from_location_create,
    get_geolocation_from_location_update,
    translate_event_address_search,
    translate_location_create,
    translate_location_update,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    CurrentActiveUser,
    MariaDBSession,
    OptionalUser,
)
from demos_backend.services.frontend_api.exceptions import (
    insufficient_permissions,
    internal_issues,
    not_anymore,
    not_found,
    organisation_name_exists,
)

router = APIRouter(prefix="/organisation", tags=["Organisation"])


@router.post("/new", response_model=OrganisationInDB)
async def new_organisation(
    schema_in: OrganisationCreate,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> Organisation:
    """Create a new organisation."""
    # guards
    organisation = crud.organisation.get_by_name(
        session=session, organisation_name=schema_in.name
    )
    if organisation:
        raise organisation_name_exists

    # sub queries
    if schema_in.location.address_language != AvailableLanguages.ENGLISH:
        schema_in.location = translate_location_create(
            schema_in=schema_in.location,
            src_language=schema_in.location.address_language,
        )
    try:
        schema_in.location = get_geolocation_from_location_create(
            location_create=schema_in.location
        )
    except RuntimeError as e:
        raise internal_issues from e

    new_location = crud.location.create(session=session, schema_in=schema_in.location)

    topics = crud.topic.get_by_ids(session=session, topic_ids=schema_in.topic_ids)

    new_model = crud.organisation.create(
        session=session,
        schema_in=schema_in,
        creator=current_active_user,
        topics=topics,
        location=new_location,
    )

    new_model = crud.organisation.add_admin(
        session=session, model_in=new_model, new_admin=current_active_user
    )

    if not new_model:
        raise internal_issues
    return new_model


@router.get("/", response_model=list[int])
async def all_organisations(
    session: MariaDBSession,
    get_current_active_user: CurrentActiveSuperUser,
    limit: conint(ge=1, le=25) = 25,
    offset: int = 0,
    approved: RequestStatus = RequestStatus.PENDING,
) -> list[int]:
    """Get all organisations."""
    organisations = crud.organisation.search(
        session=session, approved=approved, limit=limit, offset=offset
    )

    return [organisation.id for organisation in organisations]


@router.post("/address-search", response_model=list[int])
async def search_organisation(
    schema_in: OrganisationAddressSearch,
    address_language: AvailableLanguages,
    session: MariaDBSession,
) -> list[int]:
    """Search organisations with a certain address."""
    schema_in: OrganisationAddressSearch = translate_event_address_search(
        schema_in=schema_in, src_language=address_language
    )
    if len(schema_in.city + schema_in.zip_code + schema_in.country) == 0:
        location_list = None
    else:
        location_list = crud.location.get_by_address(
            session=session,
            country=schema_in.country,
            zip_code=schema_in.zip_code,
            city=schema_in.city,
        )
    organisations = crud.organisation.search(
        session=session,
        organisation_name=schema_in.name,
        location_list=location_list,
        type_id_list=schema_in.type_ids,
        topic_id_list=schema_in.topic_ids,
        limit=schema_in.limit,
        offset=schema_in.offset,
    )
    return [organisation.id for organisation in organisations]


@router.get("/{organisation_id}", response_model=OrganisationInDB)
async def get_organisation_by_id(
    user: OptionalUser,
    organisation_id: int,
    session: MariaDBSession,
) -> Organisation:
    """Get an organisation by ID."""
    approved = RequestStatus.APPROVED
    if user is not None and user.admin:
        approved = None
    organisation_data = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id, approved=approved
    )
    if not organisation_data:
        raise not_found

    return organisation_data


@router.get("/{organisation_id}/admins", response_model=list[UserInDB])
async def get_organisation_admins_by_id(
    organisation_id: int,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> list[User]:
    """Get all administrators of an organisation."""
    organisation_data = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id
    )
    if not organisation_data:
        raise not_found
    if current_active_user not in organisation_data.admins:
        raise insufficient_permissions

    return organisation_data.admins


@router.get("/{organisation_id}/internal_contact", response_model=str)
async def get_organisation_contact_by_id(
    organisation_id: int,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> str:
    """Get an organisations internal contact."""
    organisation_data = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id, approved=None
    )
    if not organisation_data:
        raise not_found
    if (
        current_active_user not in organisation_data.admins
        and not current_active_user.admin
    ):
        raise insufficient_permissions

    return organisation_data.internal_point_of_contact


@router.put("/{organisation_id}", response_model=OrganisationInDB)
async def update_organisation(
    organisation_id: int,
    schema_in: OrganisationUpdateUser,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> Organisation:
    """Update an organisations by ID."""
    # guards
    model_to_update = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id
    )
    if not model_to_update:
        raise not_found
    if current_active_user not in model_to_update.admins:
        raise insufficient_permissions

    organisation = crud.organisation.get_by_name(
        session=session, organisation_name=schema_in.name
    )
    if organisation and organisation.id != organisation_id:
        raise organisation_name_exists

    # sub queries
    if schema_in.location:
        if schema_in.location.address_language != AvailableLanguages.ENGLISH:
            schema_in.location = translate_location_update(
                schema_in=schema_in.location,
                src_language=schema_in.location.address_language,
            )

        if not schema_in.location.country:
            schema_in.location.country = organisation.location.country
        if not schema_in.location.city:
            schema_in.location.city = organisation.location.city
        if not schema_in.location.zip_code:
            schema_in.location.zip_code = organisation.location.zip_code
        if not schema_in.location.street:
            schema_in.location.street = organisation.location.street
        if not schema_in.location.house_number:
            schema_in.location.house_number = organisation.location.house_number

        try:
            schema_in.location = get_geolocation_from_location_update(
                location_update=schema_in.location
            )
        except RuntimeError as e:
            raise internal_issues from e

        crud.location.update(
            session=session,
            model_in=model_to_update.location,
            schema_in=schema_in.location,
        )

    topics = None
    if schema_in.topic_ids is not None:
        topics = crud.topic.get_by_ids(session=session, topic_ids=schema_in.topic_ids)

    update_schema = OrganisationUpdateAdmin.parse_obj(schema_in)

    updated_model = crud.organisation.update(
        session=session,
        schema_in=update_schema,
        model_in=model_to_update,
        topics=topics,
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.put("/{organisation_id}/approval_status", response_model=OrganisationInDB)
async def update_organisation_admin(
    organisation_id: int,
    approval_status: RequestStatus,
    session: MariaDBSession,
    current_active_user: CurrentActiveSuperUser,
) -> Organisation:
    """Update an organisations approval status."""
    model_to_update = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id, approved=None
    )

    if not model_to_update:
        raise not_found

    updated_model = crud.organisation.update(
        session=session,
        schema_in=OrganisationUpdateAdmin(approved=approval_status),
        model_in=model_to_update,
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.put(
    "/{organisation_id}/add_admin",
    response_model=list[UserInDB],
)
async def add_organisation_admin(
    organisation_id: int,
    admin_user_email: UserMail,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> list[User]:
    """Add an admin to an organisation."""
    model_to_update = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id
    )
    new_admin_user = crud.user.get_by_email(
        session=session, email=admin_user_email.email
    )

    if not model_to_update or not new_admin_user:
        raise not_found
    if current_active_user not in model_to_update.admins:
        raise insufficient_permissions

    updated_model = crud.organisation.add_admin(
        session=session, model_in=model_to_update, new_admin=new_admin_user
    )

    if not updated_model:
        raise internal_issues

    return updated_model.admins


@router.put(
    "/{organisation_id}/remove_admin/{admin_user_id}",
    response_model=list[UserInDB],
)
async def remove_organisation_admin(
    organisation_id: int,
    admin_user_id: int,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> list[User]:
    """Remove an admin from an organisation."""
    model_to_update = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id
    )
    remove_admin_user = crud.user.get_by_id(session=session, user_id=admin_user_id)

    if not model_to_update or not remove_admin_user:
        raise not_found
    if current_active_user not in model_to_update.admins:
        raise insufficient_permissions

    if len(model_to_update.admins) == 1:
        raise not_anymore

    updated_model = crud.organisation.remove_admin(
        session=session, model_in=model_to_update, admin_to_remove=remove_admin_user
    )

    if not updated_model:
        raise internal_issues

    return updated_model.admins


@router.get(
    "/{organisation_id}/future_events",
    response_model=list[int],
)
async def future_events(
    organisation_id: int,
    session: MariaDBSession,
    limit: conint(ge=1, le=25) = 25,
    offset: int = 0,
) -> list[int]:
    """Get the 25 next events an organisation organised."""
    now = datetime.now()
    own_search = EventSearch(
        organiser_ids=[organisation_id],
        start_time_start=now,
        limit=limit,
        offset=offset,
    )
    own_events = crud.event.search(
        session=session, schema_in=own_search, limit=limit, offset=offset, visible=True
    )
    co_search = EventSearch(co_organiser_ids=[organisation_id], start_time_start=now)
    co_events = crud.event.search(
        session=session, schema_in=co_search, limit=limit, offset=offset, visible=True
    )

    events = own_events + co_events
    events = sorted(events, key=attrgetter("start_time"))

    return [event.id for event in events[:limit]]


@router.get(
    "/{organisation_id}/past_events",
    response_model=list[int],
)
async def past_events(
    organisation_id: int,
    session: MariaDBSession,
    limit: conint(ge=1, le=25) = 25,
    offset: int = 0,
) -> list[int]:
    """Get the most recent 25 events an organisation organised."""
    now = datetime.now()
    own_search = EventSearch(organiser_ids=[organisation_id], start_time_end=now)
    own_events = crud.event.search(
        session=session, schema_in=own_search, limit=limit, offset=offset, visible=True
    )
    co_search = EventSearch(co_organiser_ids=[organisation_id], start_time_end=now)
    co_events = crud.event.search(
        session=session,
        schema_in=co_search,
        limit=limit,
        offset=offset,
        time_sorting="desc",
        visible=True,
    )

    events = own_events + co_events
    events = sorted(events, key=attrgetter("start_time"))

    return [event.id for event in reversed(events[-limit:])]


@router.get(
    "/{organisation_id}/editable_events",
    response_model=list[int],
)
async def editable_events(
    organisation_id: int,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
    limit: conint(ge=1, le=1000) = 25,
    offset: int = 0,
) -> list[int]:
    """Get all editable events."""
    organisation = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id
    )

    if not organisation:
        raise not_found
    if current_active_user not in organisation.admins:
        raise insufficient_permissions

    now = datetime.now()
    own_search = EventSearch(
        organiser_ids=[organisation_id],
        start_time_start=now,
        offset=offset,
        limit=limit,
    )
    events = crud.event.search(
        session=session, schema_in=own_search, offset=offset, limit=limit, visible=True
    )

    return [event.id for event in events]


@router.get(
    "/{organisation_id}/affiliated_organisations",
    response_model=list[int],
)
async def affiliated_organisations(
    organisation_id: int,
    session: MariaDBSession,
) -> list[int]:
    """Get all affiliated organisations of an organisation."""
    organisation = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_id
    )

    return [organisation.id for organisation in organisation.relations]
