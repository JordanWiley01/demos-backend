# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all information status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.information import Information
from demos_backend.core.schemas.information import (
    InformationCreate,
    InformationInDB,
    InformationQuery,
    InformationUpdate,
)
from demos_backend.core.types import AvailableLanguages, InformationCity
from demos_backend.core.util import (
    translate_information_create,
    translate_information_query,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import internal_issues, not_found

router = APIRouter(prefix="/information", tags=["Information"])


@router.post("/new", response_model=InformationInDB)
async def new_information(
    current_active_superuser: CurrentActiveSuperUser,
    schema_in: InformationCreate,
    address_language: AvailableLanguages,
    session: MariaDBSession,
) -> Information:
    """Create a new information entry."""
    schema_in = translate_information_create(
        schema_in=schema_in, src_language=address_language
    )
    new_model = crud.information.create(session=session, schema_in=schema_in)
    return new_model


@router.get("/available-cities", response_model=list[str])
async def get_available_cities() -> list[str]:
    """Get all available cities for which information are available."""
    return [city.value for city in InformationCity]


@router.post("/find", response_model=list[InformationInDB])
async def find_information(
    query: InformationQuery,
    address_language: AvailableLanguages,
    session: MariaDBSession,
) -> list[Information]:
    """Find information for certain cities or countries."""
    query = translate_information_query(schema_in=query, src_language=address_language)
    information_data = crud.information.get_by_query(session=session, query=query)
    return information_data


@router.put("/{information_id}", response_model=InformationInDB)
async def update_information(
    current_active_superuser: CurrentActiveSuperUser,
    information_id: int,
    schema_in: InformationUpdate,
    session: MariaDBSession,
) -> Information:
    """Update an information by ID."""
    model_to_update = crud.information.get_by_id(
        session=session, link_id=information_id
    )
    if not model_to_update:
        raise not_found

    updated_model = crud.information.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.delete("/{information_id}", response_model=InformationInDB)
async def delete_information(
    current_active_superuser: CurrentActiveSuperUser,
    information_id: int,
    session: MariaDBSession,
) -> Information:
    """Delete an information by ID."""
    link_data = crud.information.get_by_id(session=session, link_id=information_id)

    if not link_data:
        raise not_found

    deleted_link_data = crud.information.delete_by_model(
        session=session, model_to_delete=link_data
    )

    return deleted_link_data
