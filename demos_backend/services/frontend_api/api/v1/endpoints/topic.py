# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.topic import Topic
from demos_backend.core.schemas.topic import TopicCreate, TopicInDB, TopicUpdate
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import (
    internal_issues,
    not_found,
    topic_name_exists,
)

router = APIRouter(prefix="/topic", tags=["Topic"])


@router.get("/", response_model=list[TopicInDB])
async def all_topics(
    session: MariaDBSession,
) -> list[Topic]:
    """Get all topics."""
    topic_models = crud.topic.get_all(session=session)
    return topic_models


@router.post("/new", response_model=TopicInDB, tags=["Admin"])
async def new_topic(
    current_active_superuser: CurrentActiveSuperUser,
    schema_in: TopicCreate,
    session: MariaDBSession,
) -> Topic:
    """Create a new topic."""
    existing_model_name = crud.topic.get_by_name(
        session=session, topic_name=schema_in.name_en
    )
    if existing_model_name:
        raise topic_name_exists

    new_model = crud.topic.create(session=session, schema_in=schema_in)
    return new_model


@router.put("/{event_topic_id}", response_model=TopicInDB, tags=["Admin"])
async def update_topic(
    current_active_superuser: CurrentActiveSuperUser,
    event_topic_id: int,
    schema_in: TopicUpdate,
    session: MariaDBSession,
) -> Topic:
    """Update a topic by ID."""
    model_to_update = crud.topic.get_by_id(session=session, topic_id=event_topic_id)
    if not model_to_update:
        raise not_found

    existing_model_name = crud.topic.get_by_name(
        session=session, topic_name=schema_in.name_en
    )
    if existing_model_name:
        raise topic_name_exists

    updated_model = crud.topic.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.get("/{topic_id}", response_model=TopicInDB)
async def get_topic_by_id(
    topic_id: int,
    session: MariaDBSession,
) -> Topic:
    """Get a topic by ID."""
    topic = crud.topic.get_by_id(session=session, topic_id=topic_id)

    if not topic:
        raise not_found

    return topic


@router.delete("/{topic_id}", response_model=TopicInDB)
async def delete_topic_by_id(
    topic_id: int,
    session: MariaDBSession,
) -> Topic:
    """Get a topic by ID."""
    topic = crud.topic.get_by_id(session=session, topic_id=topic_id)

    if not topic:
        raise not_found

    return crud.topic.delete_by_id(session=session, topic=topic)
