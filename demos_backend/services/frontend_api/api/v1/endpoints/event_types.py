# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.event_type import EventType
from demos_backend.core.schemas.event_type import (
    EventTypeCreate,
    EventTypeInDB,
    EventTypeUpdate,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import (
    internal_issues,
    not_found,
    type_name_exists,
)

router = APIRouter(prefix="/event_type", tags=["Event", "Type"])


@router.get("/", response_model=list[EventTypeInDB])
async def all_event_types(
    session: MariaDBSession,
) -> list[EventType]:
    """Get all event types."""
    type_models = crud.event_type.get_all(session=session)
    return type_models


@router.post("/new", response_model=EventTypeInDB, tags=["Admin"])
async def new_event_type(
    current_active_superuser: CurrentActiveSuperUser,
    schema_in: EventTypeCreate,
    session: MariaDBSession,
) -> EventType:
    """Create a new event type."""
    existing_model_name = crud.event_type.get_by_name(
        session=session, event_type_name=schema_in.name_en
    )
    if existing_model_name:
        raise type_name_exists

    new_model = crud.event_type.create(session=session, schema_in=schema_in)
    return new_model


@router.put("/{event_type_id}", response_model=EventTypeInDB, tags=["Admin"])
async def update_event_type(
    current_active_superuser: CurrentActiveSuperUser,
    event_type_id: int,
    schema_in: EventTypeUpdate,
    session: MariaDBSession,
) -> EventType:
    """Update an event type by ID."""
    model_to_update = crud.event_type.get_by_id(session=session, type_id=event_type_id)

    if not model_to_update:
        raise not_found

    existing_model_name = crud.event_type.get_by_name(
        session=session, event_type_name=schema_in.name_en
    )
    if existing_model_name:
        raise type_name_exists

    updated_model = crud.event_type.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.get("/{event_type_id}", response_model=EventTypeInDB)
async def get_event_type_by_id(
    event_type_id: int,
    session: MariaDBSession,
) -> EventType:
    """Get an event type by ID."""
    event_data = crud.event_type.get_by_id(session=session, type_id=event_type_id)

    if not event_data:
        raise not_found

    return event_data


@router.delete("/{type_id}", response_model=EventTypeInDB)
async def delete_event_type_by_id(
    type_id: int,
    session: MariaDBSession,
) -> EventType:
    """Delete an event type by ID."""
    _type = crud.event_type.get_by_id(session=session, type_id=type_id)

    if not _type:
        raise not_found

    return crud.event_type.delete_by_id(session=session, event_type=_type)
