# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all language related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.types import AvailableLanguages

router = APIRouter(prefix="/language", tags=["Language"])


@router.get("/available", response_model=list[str])
async def get_search_languages() -> list[str]:
    """List all supported languages."""
    return [language.value for language in AvailableLanguages]
