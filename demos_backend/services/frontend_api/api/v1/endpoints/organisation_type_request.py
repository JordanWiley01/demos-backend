# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all organisation status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.organisation_type_request import OrganisationTypeRequest
from demos_backend.core.schemas.organisation_type_request import (
    OrganisationTypeRequestCreate,
    OrganisationTypeRequestInDB,
    OrganisationTypeRequestUpdate,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    CurrentActiveUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import internal_issues, not_found

router = APIRouter(
    prefix="/organisation_type_request", tags=["Organisation", "Type", "Request"]
)


@router.post("/new", response_model=OrganisationTypeRequestInDB)
async def new_organisation_type_request(
    schema_in: OrganisationTypeRequestCreate,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> OrganisationTypeRequest:
    """Create a new organisation type request."""
    new_model = crud.organisation_type_request.create(
        session=session, schema_in=schema_in, requester_user_id=current_active_user.id
    )
    return new_model


@router.put(
    "/{organisation_type_request_id}",
    response_model=OrganisationTypeRequestInDB,
    tags=["Admin"],
)
async def update_organisation_type_request(
    current_active_superuser: CurrentActiveSuperUser,
    organisation_type_request_id: int,
    schema_in: OrganisationTypeRequestUpdate,
    session: MariaDBSession,
) -> OrganisationTypeRequest:
    """Update an organisation type request by ID."""
    model_to_update = crud.organisation_type_request.get_by_id(
        session=session, request_id=organisation_type_request_id
    )

    if not model_to_update:
        raise not_found

    updated_model = crud.organisation_type_request.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.get(
    "/{organisation_type_request_id}",
    response_model=OrganisationTypeRequestInDB,
)
async def get_organisation_type_request(
    organisation_type_request_id: int,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> OrganisationTypeRequest:
    """Get an organisation type request by ID."""
    organisation_type_request_data = crud.organisation_type_request.get_by_id(
        session=session, request_id=organisation_type_request_id
    )
    return organisation_type_request_data
