# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.topic_request import TopicRequest
from demos_backend.core.schemas.topic_request import (
    TopicRequestCreate,
    TopicRequestInDB,
    TopicRequestUpdate,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    CurrentActiveUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import internal_issues, not_found

router = APIRouter(prefix="/topic_request", tags=["Topic", "Request"])


@router.post("/new", response_model=TopicRequestInDB)
async def new_topic_request(
    schema_in: TopicRequestCreate,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> TopicRequest:
    """Create a new topic request."""
    new_model = crud.topic_request.create(
        session=session, schema_in=schema_in, requester_user_id=current_active_user.id
    )
    return new_model


@router.put("/{topic_request_id}", response_model=TopicRequestInDB, tags=["Admin"])
async def update_topic_request(
    current_active_superuser: CurrentActiveSuperUser,
    topic_request_id: int,
    schema_in: TopicRequestUpdate,
    session: MariaDBSession,
) -> TopicRequest:
    """Update a topic request."""
    model_to_update = crud.topic_request.get_by_id(
        session=session, request_id=topic_request_id
    )

    if not model_to_update:
        raise not_found

    updated_model = crud.topic_request.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.get("/{topic_request_id}", response_model=TopicRequestInDB)
async def get_topic_request(
    topic_request_id: int,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> TopicRequest:
    """Get a new topic request by ID."""
    topic_request_data = crud.topic_request.get_by_id(
        session=session, request_id=topic_request_id
    )
    return topic_request_data
