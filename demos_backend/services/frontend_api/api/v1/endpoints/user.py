# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all user status related API endpoints."""
from __future__ import annotations

from datetime import timedelta
from typing import Annotated

from aiohttp.client_exceptions import ClientConnectorError
from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from jose import ExpiredSignatureError, jwt
from pydantic import ValidationError

from demos_backend.core import security
from demos_backend.core.clients.http.exceptions import ServiceError
from demos_backend.core.configuration import Environment
from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.organisation import Organisation
from demos_backend.core.models.user import User
from demos_backend.core.schemas.notification_request import (
    UserNotificationRequest,
    UserResetPasswordRequest,
)
from demos_backend.core.schemas.organisation import OrganisationInDB
from demos_backend.core.schemas.token import Token, TokenPayload
from demos_backend.core.schemas.user import (
    UserCreate,
    UserInDB,
    UserMail,
    UserSuperUserUpdate,
    UserUpdate,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    CurrentActiveUser,
    LoadedConfig,
    LoadedServiceClient,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import (
    email_exists,
    internal_issues,
    invalid_oauth_credentials,
    not_found,
    session_expired,
    username_exists,
)

router = APIRouter(prefix="/user", tags=["User"])

FormData = Annotated[OAuth2PasswordRequestForm, Depends(OAuth2PasswordRequestForm)]


@router.post("/get_token", response_model=Token)
async def get_access_token_by_login(
    session: MariaDBSession,
    config: LoadedConfig,
    form_data: FormData,
) -> dict:
    """Retrieve an access token for the API by logging in.

    Args:
        form_data: The login form data.
        config: The applications' configuration.
        session: The database Session.
    """
    user = crud.user.authenticate(
        session=session, email=form_data.username, password=form_data.password
    )
    if not user:
        raise invalid_oauth_credentials

    access_token_expires: timedelta = timedelta(
        minutes=config.API_ACCESS_TOKEN_EXPIRE_MINUTES
    )
    access_token: str = security.create_access_token(
        subject=user.id,
        expires_delta=access_token_expires,
    )
    return {"access_token": access_token, "token_type": "Bearer"}


@router.get("/refresh_token", response_model=Token)
async def get_access_token_by_token(
    current_active_user: CurrentActiveUser,
    config: LoadedConfig,
) -> dict:
    """Retrieve an access token for the API with a currently active token."""
    access_token_expires: timedelta = timedelta(
        minutes=config.API_ACCESS_TOKEN_EXPIRE_MINUTES
    )
    access_token: str = security.create_access_token(
        subject=current_active_user.id,
        expires_delta=access_token_expires,
    )
    return {"access_token": access_token, "token_type": "Bearer"}


@router.post("/registration", response_model=UserInDB)
async def new_user(
    schema_in: UserCreate,
    session: MariaDBSession,
    config: LoadedConfig,
    service_client: LoadedServiceClient,
) -> User:
    """Create a new user."""
    user = crud.user.get_by_username(session=session, username=schema_in.username)
    if user:
        raise username_exists
    user = crud.user.get_by_email(session=session, email=schema_in.email)
    if user:
        raise email_exists
    new_model = crud.user.create(session=session, schema_in=schema_in)

    # Creates and sets verification token for user
    access_token_expires: timedelta = timedelta(
        minutes=config.USER_VERIFICATION_TOKEN_EXPIRE_MINUTES
    )

    verification_token: str = security.create_access_token(
        subject=new_model.id,
        expires_delta=access_token_expires,
    )

    try:
        await service_client.notification_service.send_user_verification_mail(
            request=UserNotificationRequest(
                user_email=[new_model.email],
                username=new_model.username,
                token=verification_token,
                base_domain=config.FRONTEND_BASE_DOMAIN,
            )
        )
    except (ServiceError, ClientConnectorError) as e:
        if config.ENVIRONMENT != Environment.TESTING:
            crud.user.delete_by_model(session=session, model_to_delete=new_model)
            raise internal_issues from e

    return new_model


# This should be used from the frontend to verify user
@router.get("/verify", response_model=UserInDB)
async def verify_user(
    token: str,
    session: MariaDBSession,
    config: LoadedConfig,
) -> User:
    """Verify a user with the token sent via mail."""
    try:
        payload: dict = jwt.decode(
            token, config.API_SECRET_KEY, algorithms=[security.ALGORITHM]
        )
        token_data: TokenPayload = TokenPayload(**payload)
    except ExpiredSignatureError as e:
        raise session_expired from e
    except (jwt.JWTError, ValidationError) as e:
        raise invalid_oauth_credentials from e
    user = crud.user.get_by_id(session=session, user_id=token_data.sub)
    if user is None:
        raise invalid_oauth_credentials

    updated_user = crud.user.update(
        session=session,
        schema_in=UserSuperUserUpdate(verified=True),
        model_in=user,
    )

    return updated_user


@router.post("/resend-verification")
async def resend_verification(
    schema_in: UserMail,
    session: MariaDBSession,
    config: LoadedConfig,
    service_client: LoadedServiceClient,
) -> None:
    """Resend the verification mail to a user."""
    user = crud.user.get_by_email(session=session, email=schema_in.email)

    if not user:
        return

    # Creates and sets verification token for user
    access_token_expires: timedelta = timedelta(
        minutes=config.USER_VERIFICATION_TOKEN_EXPIRE_MINUTES
    )

    verification_token: str = security.create_access_token(
        subject=user.id,
        expires_delta=access_token_expires,
    )

    try:
        await service_client.notification_service.send_user_verification_mail(
            request=UserNotificationRequest(
                user_email=[user.email],
                username=user.username,
                token=verification_token,
                base_domain=config.FRONTEND_BASE_DOMAIN,
            )
        )
    except (ServiceError, ClientConnectorError) as e:
        if config.ENVIRONMENT != Environment.TESTING:
            raise internal_issues from e


@router.post("/reset-password")
async def reset_password(
    user_email: UserMail,
    session: MariaDBSession,
    config: LoadedConfig,
    service_client: LoadedServiceClient,
) -> None:
    """Reset a user's password."""
    user = crud.user.get_by_email(session=session, email=user_email.email)

    if user is None:
        return

    access_token_expires: timedelta = timedelta(
        minutes=config.PASSWORD_TOKEN_EXPIRE_MINUTES
    )

    password_token: str = security.create_access_token(
        subject=user.id,
        expires_delta=access_token_expires,
    )

    try:
        await service_client.notification_service.send_user_password_reset_mail(
            request=UserResetPasswordRequest(
                user_email=[user.email],
                token=password_token,
                base_domain=config.FRONTEND_BASE_DOMAIN,
            )
        )
    except ServiceError:
        return


@router.get("/me", response_model=UserInDB)
async def get_own_user(
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> User:
    """Get own user information."""
    user_data = crud.user.get_by_id(session=session, user_id=current_active_user.id)

    if not user_data:
        raise not_found

    return user_data


@router.delete("/me", response_model=UserInDB)
async def delete_own_user(
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> User:
    """Delete own account."""
    user_data = crud.user.get_by_id(session=session, user_id=current_active_user.id)

    if not user_data:
        raise not_found

    deleted_user = crud.user.delete_by_model(session=session, model_to_delete=user_data)

    return deleted_user


@router.put("/me", response_model=UserInDB)
async def update_own_user(
    schema_in: UserUpdate,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> User:
    """Update own user data."""
    user = crud.user.get_by_username(session=session, username=schema_in.username)
    if user and user.id != current_active_user.id:
        raise username_exists
    user = crud.user.get_by_email(session=session, email=schema_in.email)
    if user and user.id != current_active_user.id:
        raise email_exists

    model_to_update = crud.user.get_by_id(
        session=session, user_id=current_active_user.id
    )
    if not model_to_update:
        raise not_found

    updated_model = crud.user.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.get("/me/organisations", response_model=list[OrganisationInDB])
async def get_own_organisations(
    current_active_user: CurrentActiveUser,
) -> list[Organisation]:
    """Get own administrated organisations."""
    orgs = current_active_user.administrated_organisations
    approved_orgs = [org for org in orgs if org.approved]
    approved_orgs = sorted(approved_orgs, key=lambda org: org.name)
    return approved_orgs


@router.get("/admins", response_model=list[int])
async def get_admin_ids(
    session: MariaDBSession,
    current_active_user: CurrentActiveSuperUser,
) -> list[int]:
    """Get all administrations of the application."""
    admins = crud.user.get_admins(session=session)

    return [admin.id for admin in admins]


@router.post("/make_admin", response_model=UserInDB)
async def make_admin(
    user_mail: UserMail,
    session: MariaDBSession,
    current_active_user: CurrentActiveSuperUser,
) -> User:
    """Make a user an application admin."""
    model_to_update = crud.user.get_by_email(session=session, email=user_mail.email)
    if not model_to_update:
        raise not_found

    updated_model = crud.user.update(
        session=session,
        schema_in=UserSuperUserUpdate(admin=True),
        model_in=model_to_update,
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.put("/{user_id}", response_model=UserInDB)
async def update_user_by_id(
    user_id: int,
    schema_in: UserSuperUserUpdate,
    session: MariaDBSession,
    current_active_user: CurrentActiveSuperUser,
) -> User:
    """Update a user's data by ID."""
    user = crud.user.get_by_username(session=session, username=schema_in.username)
    if user:
        raise username_exists
    user = crud.user.get_by_email(session=session, email=schema_in.email)
    if user:
        raise email_exists

    model_to_update = crud.user.get_by_id(session=session, user_id=user_id)
    if not model_to_update:
        raise not_found

    updated_model = crud.user.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.get("/{user_id}", response_model=UserInDB)
async def get_user_by_id(
    user_id: int,
    session: MariaDBSession,
    current_active_user: CurrentActiveSuperUser,
) -> User:
    """Get a user's data by ID."""
    user_data = crud.user.get_by_id(session=session, user_id=user_id)

    if not user_data:
        raise not_found

    return user_data
