# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all organisation status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.organisation_type import OrganisationType
from demos_backend.core.schemas.organisation_type import (
    OrganisationTypeCreate,
    OrganisationTypeInDB,
    OrganisationTypeUpdate,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import (
    internal_issues,
    not_found,
    type_name_exists,
)

router = APIRouter(prefix="/organisation_type", tags=["Organisation", "Type"])


@router.get("/", response_model=list[OrganisationTypeInDB])
async def all_organisation_types(
    session: MariaDBSession,
) -> list[OrganisationType]:
    """Get all organisation types."""
    type_models = crud.organisation_type.get_all(session=session)
    return type_models


@router.post("/new", response_model=OrganisationTypeInDB, tags=["Admin"])
async def new_organisation_type(
    current_active_superuser: CurrentActiveSuperUser,
    schema_in: OrganisationTypeCreate,
    session: MariaDBSession,
) -> OrganisationType:
    """Create a new organisation type."""
    existing_model_name = crud.organisation_type.get_by_name(
        session=session, organisation_type_name=schema_in.name_en
    )
    if existing_model_name:
        raise type_name_exists

    new_model = crud.organisation_type.create(session=session, schema_in=schema_in)
    return new_model


@router.put(
    "/{organisation_type_id}",
    response_model=OrganisationTypeInDB,
    tags=["Admin"],
)
async def update_organisation_type(
    current_active_superuser: CurrentActiveSuperUser,
    organisation_type_id: int,
    schema_in: OrganisationTypeUpdate,
    session: MariaDBSession,
) -> OrganisationType:
    """Get an organisation type by ID."""
    model_to_update = crud.organisation_type.get_by_id(
        session=session, type_id=organisation_type_id
    )

    if not model_to_update:
        raise not_found

    existing_model_name = crud.organisation_type.get_by_name(
        session=session, organisation_type_name=schema_in.name_en
    )
    if existing_model_name:
        raise type_name_exists

    updated_model = crud.organisation_type.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.get("/{organisation_type_id}", response_model=OrganisationTypeInDB)
async def get_organisation_type_by_id(
    organisation_type_id: int,
    session: MariaDBSession,
) -> OrganisationType:
    """Update an organisation type by ID."""
    org_type = crud.organisation_type.get_by_id(
        session=session, type_id=organisation_type_id
    )

    if not org_type:
        raise not_found

    return org_type


@router.delete("/{type_id}", response_model=OrganisationTypeInDB)
async def delete_organisation_type_by_id(
    type_id: int,
    session: MariaDBSession,
) -> OrganisationType:
    """Delete an organisation type by ID."""
    _type = crud.organisation_type.get_by_id(session=session, type_id=type_id)

    if not _type:
        raise not_found

    return crud.organisation_type.delete_by_id(session=session, organisation_type=_type)
