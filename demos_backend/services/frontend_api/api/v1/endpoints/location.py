# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.types import AvailableLanguages, InformationCountry
from demos_backend.core.util import (
    translate_text,
)

router = APIRouter(prefix="/location", tags=["Location"])


@router.get("/available-countries/create", response_model=list[str])
async def get_create_countries(
    language: AvailableLanguages = AvailableLanguages.ENGLISH,
) -> list[str]:
    """List all countries for which are information can be created."""
    return [
        translate_text(
            text=country.value,
            src_language=AvailableLanguages.ENGLISH,
            target_language=language,
        )
        for country in InformationCountry
    ]


@router.get("/available-countries/search", response_model=list[str])
async def get_search_countries(
    language: AvailableLanguages = AvailableLanguages.ENGLISH,
) -> list[str]:
    """List all countries for which are information available."""
    return [
        translate_text(
            text=country.value,
            src_language=AvailableLanguages.ENGLISH,
            target_language=language,
        )
        for country in InformationCountry
    ]
