# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all endpoints for reports."""
from __future__ import annotations

from datetime import timedelta

from fastapi import APIRouter
from pandas import DataFrame

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.schemas.analysis import (
    AnalysisDensitymapData,
    AnalysisEventDensitymapQuery,
    AnalysisEventTimeseriesQuery,
    AnalysisTimeseriesData,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    MariaDBSession,
)

router = APIRouter(prefix="/analysis", tags=["Analysis"])


@router.post(
    "/event/timeseries",
    response_model=AnalysisTimeseriesData,
)
async def get_event_timeseries(
    query: AnalysisEventTimeseriesQuery,
    session: MariaDBSession,
) -> AnalysisTimeseriesData:
    """Get event data for time series."""
    frequencies = {
        "day": timedelta(days=1),
        "week": timedelta(weeks=1),
        "month": "M",
        "year": "Y",
    }
    events = crud.analysis.get_event_analysis_data(session=session, query=query)

    if len(events) == 0:
        return AnalysisTimeseriesData(times=[], values=[])

    if query.event_time == "start":
        times = [event.start_time for event in events]
    else:
        times = [event.created for event in events]
    values = [1 for event in events]

    # aggregate
    data = {"times": times, "values": values}
    df = DataFrame(data=data)
    view = df.resample(frequencies[query.interval], on="times").values.sum()
    return AnalysisTimeseriesData(
        times=view.index.values.tolist(), values=view.values.tolist()
    )


@router.post(
    "/event/densitymap",
    response_model=AnalysisDensitymapData,
)
async def get_event_densitymap(
    query: AnalysisEventDensitymapQuery,
    session: MariaDBSession,
) -> AnalysisDensitymapData:
    """Get event data for densitymaps."""
    events = crud.analysis.get_event_analysis_data(session=session, query=query)

    if len(events) == 0:
        return AnalysisDensitymapData(latitudes=[], longitudes=[], values=[])

    latitudes = []
    longitudes = []
    values = []

    for event in events:
        latitudes.append(event.location.latitude)
        longitudes.append(event.location.longitude)
        values.append(1)

    return AnalysisDensitymapData(
        latitudes=latitudes, longitudes=longitudes, values=values
    )
