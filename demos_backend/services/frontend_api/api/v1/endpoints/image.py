# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from io import BytesIO
from typing import Annotated

from fastapi import APIRouter, File, UploadFile
from fastapi.responses import StreamingResponse

from demos_backend.core.models.image import Image
from demos_backend.core.schemas.image import ImageCreate
from demos_backend.services.frontend_api.api.v1.dependencies import CurrentActiveUser
from demos_backend.services.frontend_api.exceptions import internal_issues, not_found

UploadedFile = Annotated[UploadFile, File(...)]
router = APIRouter(prefix="/image", tags=["Image"])


@router.get("/{image_name}")
async def get_image_by_id(image_name: str) -> StreamingResponse:
    """Get an image by name.

    Args:
        image_name: The name of the image.

    Returns:
        StreamingResponse: The image from the database.

    """
    image = await Image.find_by_name(image_name)
    if not image:
        raise not_found
    byte_data = BytesIO(image.image)
    return StreamingResponse(byte_data, media_type="image/jpeg")


@router.post("/upload", response_model=dict[str, str])
async def upload_image(
    current_active_user: CurrentActiveUser,
    file_bytes: UploadedFile,
) -> dict[str, str]:
    """Upload a new image.

    Args:
        file_bytes: The image data.
        current_active_user: A logged-in user.

    Returns:
        dict[str, str]: A dictionary containing the 'image_name'.
    """
    allowed_files = {"image/jpeg", "image/png", "image/svg"}
    if file_bytes.content_type not in allowed_files:
        raise internal_issues

    file_bytes = await file_bytes.read()
    creation_schema = ImageCreate(
        image=file_bytes, creator_user_id=current_active_user.id
    )
    image = await Image.new(creation_schema)
    if not image:
        raise internal_issues
    return {"image_name": image.name}
