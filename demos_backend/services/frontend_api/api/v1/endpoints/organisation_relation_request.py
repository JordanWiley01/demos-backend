# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all organisation status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.organisation_relation_request import (
    OrganisationRelationRequest,
)
from demos_backend.core.schemas.organisation_relation_request import (
    OrganisationRelationRequestCreate,
    OrganisationRelationRequestInDB,
    OrganisationRelationRequestUpdate,
)
from demos_backend.core.types import RequestStatus
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import (
    insufficient_permissions,
    internal_issues,
    not_found,
)

router = APIRouter(prefix="/organisation_relation_request", tags=["Organisation"])


@router.post("/new", response_model=OrganisationRelationRequestInDB)
async def new_organisation_relation_request(
    schema_in: OrganisationRelationRequestCreate,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> OrganisationRelationRequest:
    """Request a relation with another organisation."""
    requester_org = crud.organisation.get_by_id(
        session=session, organisation_id=schema_in.organisation_requester_id
    )
    requested_org = crud.organisation.get_by_id(
        session=session, organisation_id=schema_in.organisation_requested_id
    )
    if not requester_org or not requested_org:
        raise not_found

    if current_active_user not in requester_org.admins:
        raise insufficient_permissions
    new_model = crud.organisation_relation_request.create(
        requester_organisation=requester_org,
        requested_organisation=requested_org,
        session=session,
    )
    if not new_model:
        raise internal_issues
    return new_model


@router.get(
    "/by_ids",
    response_model=OrganisationRelationRequestInDB,
)
async def get_organisation_relation_request_by_ids(
    organisation_relation_requester_id: int,
    organisation_relation_requested_id: int,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> OrganisationRelationRequest:
    """Get organisation requests by IDs."""
    requester = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_relation_requester_id
    )
    requested = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_relation_requested_id
    )

    if not requester or not requested:
        raise not_found

    organisation_relation_request = crud.organisation_relation_request.get_by_orgs(
        session=session, requester=requester, requested=requested
    )

    if not organisation_relation_request:
        # this raises insufficient permission to block scraping of
        # the existence of requests.
        raise insufficient_permissions

    if (
        current_active_user
        not in organisation_relation_request.organisation_requester.admins
    ) or (
        current_active_user
        not in organisation_relation_request.organisation_requested.admins
    ):
        raise insufficient_permissions

    return organisation_relation_request


@router.put(
    "/by_ids",
    response_model=OrganisationRelationRequestInDB,
)
async def update_organisation_relation_request_by_ids(
    organisation_relation_requester_id: int,
    organisation_relation_requested_id: int,
    schema_in: OrganisationRelationRequestUpdate,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> OrganisationRelationRequest:
    """Update organisation requests by IDs."""
    requester = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_relation_requester_id
    )
    requested = crud.organisation.get_by_id(
        session=session, organisation_id=organisation_relation_requested_id
    )

    if not requester or not requested:
        raise not_found

    model_to_update = crud.organisation_relation_request.get_by_orgs(
        session=session, requester=requester, requested=requested
    )

    if not model_to_update:
        # this raises insufficient permission to block scraping of
        # the existence of requests.
        raise insufficient_permissions

    if current_active_user not in model_to_update.organisation_requested.admins:
        raise insufficient_permissions

    updated_model = crud.organisation_relation_request.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    if schema_in.status:
        if schema_in.status == RequestStatus.APPROVED.value:
            crud.organisation.add_relation(
                session=session, model_in=requester, org_to_add=requested
            )
            crud.organisation.add_relation(
                session=session, model_in=requested, org_to_add=requester
            )
        if schema_in.status == RequestStatus.DENIED.value:
            crud.organisation.remove_relation(
                session=session, model_in=requester, org_to_remove=requested
            )
            crud.organisation.remove_relation(
                session=session, model_in=requested, org_to_remove=requester
            )

    return updated_model
