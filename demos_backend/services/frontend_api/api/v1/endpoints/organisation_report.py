# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all organisation status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.organisation_report import OrganisationReport
from demos_backend.core.schemas.organisation_report import (
    OrganisationReportCreate,
    OrganisationReportInDB,
    OrganisationReportUpdate,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    CurrentActiveUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import (
    insufficient_permissions,
    internal_issues,
    not_found,
)

router = APIRouter(prefix="/organisation_report", tags=["Organisation", "Report"])


@router.post("/new", response_model=OrganisationReportInDB)
async def new_organisation_report(
    schema_in: OrganisationReportCreate,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> OrganisationReport:
    """Report an organisation."""
    reported_org = crud.organisation.get_by_id(
        session=session, organisation_id=schema_in.reported_organisation_id
    )
    reporter_org = crud.organisation.get_by_id(
        session=session, organisation_id=schema_in.reporter_organisation_id
    )
    if not reported_org or not reporter_org:
        raise not_found

    if current_active_user not in reporter_org.admins:
        raise insufficient_permissions

    new_model = crud.organisation_report.create(
        session=session, schema_in=schema_in, reporter_user_id=current_active_user.id
    )
    return new_model


@router.get(
    "/{organisation_report_id}",
    response_model=OrganisationReportInDB,
    tags=["Admin"],
)
async def get_organisation_report_by_id(
    organisation_report_id: int,
    session: MariaDBSession,
    current_active_super_user: CurrentActiveSuperUser,
) -> OrganisationReport:
    """Get an organisation report by ID."""
    organisation_report = crud.organisation_report.get_by_id(
        session=session, report_id=organisation_report_id
    )
    if not organisation_report:
        raise not_found
    return organisation_report


@router.put(
    "/{organisation_report_id}",
    response_model=OrganisationReportInDB,
    tags=["Admin"],
)
async def update_organisation_report(
    organisation_report_id: int,
    schema_in: OrganisationReportUpdate,
    session: MariaDBSession,
    current_active_super_user: CurrentActiveSuperUser,
) -> OrganisationReport:
    """Update an organisation report by ID."""
    model_to_update = crud.organisation_report.get_by_id(
        session=session, report_id=organisation_report_id
    )
    if not model_to_update:
        raise not_found

    updated_model = crud.organisation_report.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model
