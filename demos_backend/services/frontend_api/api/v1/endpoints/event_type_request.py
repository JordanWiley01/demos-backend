# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.event_type_request import EventTypeRequest
from demos_backend.core.schemas.event_type_request import (
    EventTypeRequestCreate,
    EventTypeRequestInDB,
    EventTypeRequestUpdate,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    CurrentActiveUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import internal_issues, not_found

router = APIRouter(prefix="/event_type_request", tags=["Event", "Type", "Request"])


@router.post("/new", response_model=EventTypeRequestInDB)
async def new_event_type_request(
    schema_in: EventTypeRequestCreate,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> EventTypeRequest:
    """Request a new event type."""
    new_model = crud.event_type_request.create(
        session=session, schema_in=schema_in, requester_user_id=current_active_user.id
    )
    return new_model


@router.put(
    "/{event_type_request_id}",
    response_model=EventTypeRequestInDB,
    tags=["Admin"],
)
async def update_event_type_request(
    current_active_superuser: CurrentActiveSuperUser,
    event_type_request_id: int,
    schema_in: EventTypeRequestUpdate,
    session: MariaDBSession,
) -> EventTypeRequest:
    """Update an event type request by ID."""
    model_to_update = crud.event_type_request.get_by_id(
        session=session, request_id=event_type_request_id
    )

    if not model_to_update:
        raise not_found

    updated_model = crud.event_type_request.update(
        session=session, schema_in=schema_in, model_in=model_to_update
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.get(
    "/{event_type_request_id}",
    response_model=EventTypeRequestInDB,
    tags=["Admin"],
)
async def get_event_type_request(
    current_active_superuser: CurrentActiveSuperUser,
    event_type_request_id: int,
    session: MariaDBSession,
) -> EventTypeRequest:
    """Get an event type request by ID."""
    event_type_request_data = crud.event_type_request.get_by_id(
        session=session, request_id=event_type_request_id
    )

    if not event_type_request_data:
        raise not_found

    return event_type_request_data
