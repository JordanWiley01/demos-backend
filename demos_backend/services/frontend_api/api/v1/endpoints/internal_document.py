# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from io import BytesIO
from typing import Annotated

from fastapi import APIRouter, File, UploadFile
from fastapi.responses import StreamingResponse
from pydantic import constr

from demos_backend.core.models.internal_document import InternalDocument
from demos_backend.core.schemas.internal_document import InternalDocumentCreate
from demos_backend.core.types import AvailableLanguages, InternalDocumentType
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
)
from demos_backend.services.frontend_api.exceptions import (
    document_name_exists,
    internal_issues,
    not_found,
)

UploadedFile = Annotated[UploadFile, File(...)]
router = APIRouter(prefix="/document", tags=["Document"])


@router.get("/available", response_model=dict[str, str])
async def get_available_documents() -> dict[str, str]:
    """Get all available documents.

    Returns:
        dict[str, str]: All names of available documents.
    """
    names = await InternalDocument.get_all_names()
    return names


@router.get("/{file_name}")
async def get_document_by_file_name(file_name: str) -> StreamingResponse:
    """Get a document by file name.

    Args:
        file_name: The file name of the document.

    Returns:
        StreamingResponse: The document from the database.

    """
    document = await InternalDocument.find_by_file_name(file_name)
    if not document:
        raise not_found
    byte_data = BytesIO(document.data)
    return StreamingResponse(byte_data, media_type="application/pdf")


@router.delete("/{file_name}")
async def delete_document_by_name(
    file_name: str, current_active_superuser: CurrentActiveSuperUser
) -> str:
    """Delete a document by file name.

    Args:
        file_name: The file name of the document.
        current_active_superuser: A logged-in superuser.

    Returns:
        str: If the request was successful
    """
    document = await InternalDocument.find_by_file_name(file_name)
    if not document:
        raise not_found
    await document.delete()
    return "OK"


@router.post("/upload", response_model=str)
async def upload_document(
    language: AvailableLanguages,
    document_type: InternalDocumentType,
    display_name: constr(min_length=1, max_length=100),
    current_active_superuser: CurrentActiveSuperUser,
    file_bytes: UploadedFile,
) -> str:
    """Upload a new document.

    Args:
        language: The language of the file.
        document_type: The type of the document (e.g. financial report)
        file_bytes: The document data.
        current_active_superuser: A logged-in superuser.
        display_name: The name to show in the UI.

    Returns:
        str: A status response
    """
    allowed_files = {"application/pdf"}
    if file_bytes.content_type not in allowed_files:
        raise internal_issues

    file_name = file_bytes.filename

    file_bytes = await file_bytes.read()
    creation_schema = InternalDocumentCreate(
        language=language,
        creator_user_id=current_active_superuser.id,
        data=file_bytes,
        type=document_type,
        file_name=file_name,
        display_name=display_name,
    )
    try:
        document = await InternalDocument.new(creation_schema)
    except ValueError as e:
        raise document_name_exists from e
    if not document:
        raise internal_issues
    return "OK"
