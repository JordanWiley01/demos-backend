# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all event status related API endpoints."""
from __future__ import annotations

from datetime import datetime

from dateutil.relativedelta import relativedelta
from fastapi import APIRouter

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.models.event import Event
from demos_backend.core.models.organisation import Organisation
from demos_backend.core.schemas.event import (
    EventAddressSearch,
    EventCreate,
    EventInDB,
    EventPerimeterSearch,
    EventUpdate,
)
from demos_backend.core.schemas.organisation import OrganisationInDB
from demos_backend.core.types import AvailableLanguages, ReoccurringEventPeriod
from demos_backend.core.util import (
    get_geolocation_from_location_create,
    translate_event_address_search,
    translate_location_create,
    translate_location_update,
)
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.exceptions import (
    insufficient_permissions,
    internal_issues,
    not_anymore,
    not_found,
)

router = APIRouter(prefix="/event", tags=["Event"])


@router.post("/new", response_model=list[EventInDB])
async def new_event(
    current_active_user: CurrentActiveUser,
    schema_in: EventCreate,
    session: MariaDBSession,
) -> list[Event]:
    """Create a new event."""
    # guards
    organisation = crud.organisation.get_by_id(
        session=session, organisation_id=schema_in.organising_organisation_id
    )

    if not organisation:
        raise not_found

    if current_active_user not in organisation.admins:
        raise insufficient_permissions

    # handle sub queries
    if schema_in.location.address_language != AvailableLanguages.ENGLISH:
        schema_in.location = translate_location_create(
            schema_in=schema_in.location,
            src_language=schema_in.location.address_language,
        )
    try:
        schema_in.location = get_geolocation_from_location_create(
            location_create=schema_in.location
        )
    except RuntimeError as e:
        raise internal_issues from e

    new_location = crud.location.create(session=session, schema_in=schema_in.location)

    topics = crud.topic.get_by_ids(session=session, topic_ids=schema_in.topic_ids)
    co_organizers = crud.organisation.get_by_ids(
        session=session, organisation_ids=schema_in.co_organising_organisations_ids
    )

    # create events
    events: list[Event] = []
    for count in range(schema_in.reoccurring_count):
        if schema_in.reoccurring == ReoccurringEventPeriod.WEEKLY:
            delta_t = relativedelta(weeks=count)
            shifted_schema = schema_in.copy(deep=True)
            shifted_schema.start_time += delta_t
            shifted_schema.end_time += delta_t
        elif schema_in.reoccurring == ReoccurringEventPeriod.BIWEEKLY:
            delta_t = relativedelta(weeks=2 * count)
            shifted_schema = schema_in.copy(deep=True)
            shifted_schema.start_time += delta_t
            shifted_schema.end_time += delta_t
        elif schema_in.reoccurring == ReoccurringEventPeriod.MONTHLY:
            delta_t = relativedelta(month=count)
            shifted_schema = schema_in.copy(deep=True)
            shifted_schema.start_time += delta_t
            shifted_schema.end_time += delta_t
        else:
            shifted_schema = schema_in.copy(deep=True)

        new_model = crud.event.create(
            session=session,
            schema_in=shifted_schema,
            creator=current_active_user,
            organising_organisation=organisation,
            topics=topics,
            location=new_location,
            co_organizers=co_organizers,
        )
        events.append(new_model)

    return events


@router.post("/perimeter-search", response_model=list[EventInDB])
async def search_by_perimeter(
    schema_in: EventPerimeterSearch,
    session: MariaDBSession,
) -> list[Event]:
    """Search events within a radius around a point."""
    location_list = crud.location.get_by_radius(
        session=session,
        mid_lat=schema_in.latitude,
        mid_long=schema_in.longitude,
        radius_km=schema_in.radius_km,
    )

    events = crud.event.search(
        session=session,
        schema_in=schema_in,
        location_list=location_list,
        limit=schema_in.limit,
        offset=schema_in.offset,
        visible=True,
    )
    return events


@router.post("/perimeter-search/count", response_model=int)
async def search_by_perimeter_count(
    schema_in: EventPerimeterSearch,
    session: MariaDBSession,
) -> int:
    """Get the number of events within a radius around a point."""
    location_list = crud.location.get_by_radius(
        session=session,
        mid_lat=schema_in.latitude,
        mid_long=schema_in.longitude,
        radius_km=schema_in.radius_km,
    )

    events = crud.event.search(
        session=session,
        schema_in=schema_in,
        location_list=location_list,
        limit=schema_in.limit,
        offset=schema_in.offset,
        visible=True,
    )
    return len(events)


@router.post("/address-search", response_model=list[EventInDB])
async def search_by_address(
    schema_in: EventAddressSearch,
    address_language: AvailableLanguages,
    session: MariaDBSession,
) -> list[Event]:
    """Search events with a certain address."""
    schema_in = translate_event_address_search(
        schema_in=schema_in, src_language=address_language
    )
    if len(schema_in.city + schema_in.zip_code + schema_in.country) == 0:
        location_list = None
    else:
        location_list = crud.location.get_by_address(
            session=session,
            country=schema_in.country,
            zip_code=schema_in.zip_code,
            city=schema_in.city,
        )

    events = crud.event.search(
        session=session,
        schema_in=schema_in,
        location_list=location_list,
        limit=schema_in.limit,
        offset=schema_in.offset,
        visible=True,
    )
    return events


@router.post("/address-search/count", response_model=int)
async def search_by_address_count(
    schema_in: EventAddressSearch,
    address_language: AvailableLanguages,
    session: MariaDBSession,
) -> int:
    """Get the number of events with a certain address."""
    schema_in = translate_event_address_search(
        schema_in=schema_in, src_language=address_language
    )
    if len(schema_in.city + schema_in.zip_code + schema_in.country) == 0:
        location_list = None
    else:
        location_list = crud.location.get_by_address(
            session=session,
            country=schema_in.country,
            zip_code=schema_in.zip_code,
            city=schema_in.city,
        )

    events = crud.event.search(
        session=session,
        schema_in=schema_in,
        location_list=location_list,
        limit=schema_in.limit,
        offset=schema_in.offset,
        visible=True,
    )
    return len(events)


@router.get("/{event_id}", response_model=EventInDB)
async def get_event_by_id(event_id: int, session: MariaDBSession) -> Event:
    """Get an event by ID."""
    event_data = crud.event.get_by_id(session=session, event_id=event_id)

    if not event_data or not event_data.visible:
        raise not_found

    return event_data


@router.put("/{event_id}", response_model=EventInDB)
async def update_event(
    current_active_user: CurrentActiveUser,
    event_id: int,
    schema_in: EventUpdate,
    session: MariaDBSession,
) -> Event:
    """Update an event by ID."""
    # guards
    model_to_update = crud.event.get_by_id(session=session, event_id=event_id)
    if not model_to_update:
        raise not_found
    if (
        model_to_update.creator_id != current_active_user.id
        and not current_active_user.admin
    ):
        raise insufficient_permissions

    if schema_in.organising_organisation_id is not None:
        organisation = crud.organisation.get_by_id(
            session=session, organisation_id=schema_in.organising_organisation_id
        )

        if not organisation:
            raise not_found

        if (
            current_active_user not in organisation.admins
            and not current_active_user.admin
        ):
            raise insufficient_permissions

    if model_to_update.start_time < datetime.now() and not current_active_user.admin:
        raise not_anymore

    # sub queries
    if schema_in.location:
        if schema_in.location.address_language != AvailableLanguages.ENGLISH:
            schema_in.location = translate_location_update(
                schema_in=schema_in.location,
                src_language=schema_in.location.address_language,
            )

        crud.location.update(
            session=session,
            model_in=model_to_update.location,
            schema_in=schema_in.location,
        )
    topics = crud.topic.get_by_ids(session=session, topic_ids=schema_in.topic_ids)

    co_organizers = crud.organisation.get_by_ids(
        session=session, organisation_ids=schema_in.co_organising_organisations_ids
    )

    updated_model = crud.event.update(
        session=session,
        schema_in=schema_in,
        model_in=model_to_update,
        topics=topics,
        co_organizers=co_organizers,
    )

    if not updated_model:
        raise internal_issues

    return updated_model


@router.delete("/{event_id}", response_model=str)
async def delete_event(
    event_id: int,
    session: MariaDBSession,
    current_active_user: CurrentActiveUser,
) -> str:
    """Delete an event by ID."""
    model_to_delete = crud.event.get_by_id(session=session, event_id=event_id)

    if not model_to_delete:
        raise not_found

    if current_active_user.id != model_to_delete.creator_id:
        raise insufficient_permissions

    if model_to_delete.start_time < datetime.now():
        raise not_anymore

    crud.event.delete_by_id(session=session, model_in=model_to_delete)

    return "OK"


@router.get("/{event_id}/organizer", response_model=OrganisationInDB)
async def get_event_organizer(event_id: int, session: MariaDBSession) -> Organisation:
    """Get an events organising organisation."""
    event_data = crud.event.get_by_id(session=session, event_id=event_id)

    if not event_data:
        raise not_found

    return event_data.organising_organisation


@router.get("/{event_id}/co-organizers", response_model=list[OrganisationInDB])
async def get_event_co_organizers(
    event_id: int, session: MariaDBSession
) -> list[Organisation]:
    """Get an events co-organisers."""
    event_data = crud.event.get_by_id(session=session, event_id=event_id)

    if not event_data:
        raise not_found

    return event_data.co_organisers
