# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all endpoints for reports."""
from __future__ import annotations

from fastapi import APIRouter
from starlette.background import BackgroundTasks

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.schemas.admin import AdminStatistics, CleanupTasks
from demos_backend.core.schemas.message import Message
from demos_backend.services.frontend_api.api.v1.dependencies import (
    CurrentActiveSuperUser,
    MariaDBSession,
)
from demos_backend.services.frontend_api.background_tasks import (
    cleanup_dangling_locations,
    cleanup_future_events,
)

router = APIRouter(prefix="/admin", tags=["Admin"])


@router.post("/statistics", response_model=AdminStatistics)
async def get_admin_statistics(
    session: MariaDBSession, superuser: CurrentActiveSuperUser
) -> AdminStatistics:
    """Get statistics for the admin."""
    locations = crud.location.get_all(session=session)
    locations = [
        location
        for location in locations
        if len(location.events) == 0 and len(location.organisations) == 0
    ]
    user_count = crud.user.count(session=session)
    organisation_count = crud.organisation.count(session=session)
    event_count = crud.event.count(session=session)
    return AdminStatistics(
        dangling_locations=len(locations),
        user_count=user_count,
        organisation_count=organisation_count,
        event_count=event_count,
    )


@router.post("/cleanup", response_model=Message)
async def run_cleanup(
    session: MariaDBSession,
    cleanup_tasks: CleanupTasks,
    superuser: CurrentActiveSuperUser,
    background_tasks: BackgroundTasks,
) -> dict:
    """Perform cleanup tasks."""
    if cleanup_tasks.dangling_locations:
        background_tasks.add_task(cleanup_dangling_locations, session)
    if cleanup_tasks.future_events:
        background_tasks.add_task(cleanup_future_events, session)
    return {"detail": "Starting cleanup."}
