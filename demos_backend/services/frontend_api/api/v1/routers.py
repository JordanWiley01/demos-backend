# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains all routers for assembling an API."""

from fastapi import APIRouter

from .endpoints import (
    admin,
    analysis,
    event,
    event_report,
    event_type_request,
    event_types,
    image,
    information,
    internal_document,
    language,
    location,
    organisation,
    organisation_relation_request,
    organisation_report,
    organisation_type_request,
    organisation_types,
    topic,
    topic_request,
    user,
)

v1_router = APIRouter()
v1_router.include_router(event.router)
v1_router.include_router(event_types.router)
v1_router.include_router(event_report.router)
v1_router.include_router(event_type_request.router)
v1_router.include_router(location.router)
v1_router.include_router(organisation.router)
v1_router.include_router(organisation_types.router)
v1_router.include_router(organisation_report.router)
v1_router.include_router(organisation_type_request.router)
v1_router.include_router(organisation_relation_request.router)
v1_router.include_router(topic.router)
v1_router.include_router(topic_request.router)
v1_router.include_router(information.router)
v1_router.include_router(user.router)
v1_router.include_router(image.router)
v1_router.include_router(language.router)
v1_router.include_router(internal_document.router)
v1_router.include_router(analysis.router)
v1_router.include_router(admin.router)
