# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module holds service specific exceptions.

The detail messages are translated in the frontend. After adding / changing exceptions,
they need to be exported via `demos_backend/frontend_api/dev/endpoints/exceptions.py`
and then translated va i18n in the frontend.
"""
from fastapi import HTTPException, status

invalid_oauth_credentials = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Incorrect email or password.",
)

internal_issues = HTTPException(
    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    detail="Technical error - try again later.",
)

not_found = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND,
    detail="Whatever you are looking for, it is not here.",
)

username_exists = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="Username already exists.",
)
document_name_exists = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="Document name already exists.",
)

email_exists = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="Email already exists.",
)

topic_name_exists = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="Topic name already exists.",
)


type_name_exists = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="Type name already exists.",
)

organisation_name_exists = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="Organisation name already exists.",
)

session_expired = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Session expired, please log in again.",
    headers={"WWW-Authenticate": "Bearer"},
)

inactive_user = HTTPException(status_code=401, detail="Inactive user")

unverified_user = HTTPException(status_code=401, detail="Unverified user")

insufficient_permissions = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Insufficient permissions",
)

not_anymore = HTTPException(
    status_code=status.HTTP_403_FORBIDDEN,
    detail="This action is not possible anymore.",
)
