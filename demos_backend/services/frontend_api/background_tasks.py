# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module holds service specific background tasks."""
import logging
from datetime import datetime

from sqlmodel import Session

from demos_backend.core.databases.mariadb import crud
from demos_backend.core.schemas.event import EventSearch


def cleanup_dangling_locations(session: Session):
    """Delete all dangling locations."""
    logging.debug("Cleaning up dangling events")
    locations = crud.location.get_all(session=session)
    locations = [
        location
        for location in locations
        if len(location.events) == 0 and len(location.organisations) == 0
    ]
    for location in locations:
        logging.warning("Deleting location %s", location.id)
        crud.location.delete(session=session, location=location)


def cleanup_future_events(session: Session):
    """Delete all future events."""
    logging.debug("Cleaning up future events.")
    now = datetime.now()
    events = crud.event.search(session=session, schema_in=EventSearch(end_time_end=now))

    for event in events:
        logging.warning("Deleting event %s", event.id)
        crud.event.delete_by_id(session=session, model_in=event)
