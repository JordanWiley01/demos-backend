# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the API for the frontend."""
from __future__ import annotations

import logging
from contextlib import asynccontextmanager

from fastapi import FastAPI, HTTPException
from fastapi.exception_handlers import (
    http_exception_handler,
    request_validation_exception_handler,
)
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse, Response

from demos_backend.core.configuration import Environment, get_config
from demos_backend.core.databases.mongodb import initialize_beanie
from demos_backend.core.monitoring.prometheus.fastapi import add_endpoint
from demos_backend.core.util import use_route_names_as_operation_ids
from demos_backend.services.frontend_api.api.dev.routers import dev_router
from demos_backend.services.frontend_api.api.v1.routers import v1_router

config = get_config()

origins = ["*"]


logging.debug("Setting up ASGI application.")


@asynccontextmanager
async def lifespan(app: FastAPI) -> None:
    """Lifecycle management."""
    await initialize_beanie()
    yield


frontend_api = FastAPI(
    title="Demons Berlin e.V. Frontend API",
    description="Provides access to the backend.",
    docs_url=f"{config.API_V1_ROOT_PATH}/docs",
    openapi_url=f"{config.API_V1_ROOT_PATH}/openapi.json",
    redoc_url=None,
    lifespan=lifespan,
)

frontend_api.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

frontend_api.include_router(v1_router, prefix=config.API_V1_ROOT_PATH)
if config.ENVIRONMENT == Environment.DEVELOPMENT:
    frontend_api.include_router(dev_router, prefix=config.API_V1_ROOT_PATH)

use_route_names_as_operation_ids(frontend_api)

add_endpoint(app_name="frontend_api", app=frontend_api, config=config)


@frontend_api.exception_handler(HTTPException)
async def custom_http_exception_handler(
    request: Request, exc: HTTPException
) -> Response:
    """Log the raised exception and proceed with default exception handler."""
    logging.error(
        exc.detail,
        exc_info=exc,
        extra={
            "tags": {
                "exception_type": exc.__class__.__name__,
                "status": exc.status_code,
            },
        },
    )
    return await http_exception_handler(request, exc)


@frontend_api.exception_handler(RequestValidationError)
async def custom_request_validation_exception_handler(
    request: Request, exc: RequestValidationError
) -> JSONResponse:
    """Log the raised exception and proceed with default exception handler."""
    logging.warning(
        "Validation Exception",
        exc_info=exc,
        extra={"tags": {"exception_type": "RequestValidationError", "status": 422}},
    )
    return await request_validation_exception_handler(request, exc)
