# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Schemas to fill out mail templates."""
from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, EmailStr


class OrganisationEvent(BaseModel):
    """Required information to fill an event notification of an organisation."""

    user_email: List[EmailStr]
    username: List[str]
    organisation_name: str
    url: str
    event_name: str
    start_time: datetime
    end_time: datetime


class AccountVerification(BaseModel):
    """Required information to fill a verification mail."""

    user_email: List[EmailStr]
    base_domain: str
    username: str
    token: str
    language: Optional[str] = "de"


class ResetPassword(BaseModel):
    """Required information to fill a password reset mail."""

    user_email: List[EmailStr]
    base_domain: str
    token: str
    language: Optional[str] = "de"
