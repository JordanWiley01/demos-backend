# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Provides a metrics class for prometheus measurements."""

from prometheus_client import Counter

from demos_backend.core.monitoring import PrometheusBaseMetrics


class NotificationsMetrics(PrometheusBaseMetrics):
    """Wrapper class for prometheus metrics."""

    sent_mails: Counter
    app_name: str

    def __init__(self) -> None:
        """Create an instance of the class."""
        self.app_name = "notifications"
        self.sent_mails = Counter(
            name=f"{self.prefix}_sent_mails",
            documentation="Count of sent mails.",
            labelnames=["mail_type"],
        )
