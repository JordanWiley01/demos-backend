# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""The notification service, responsible for sending messages to the user."""
import logging

from fastapi import FastAPI, HTTPException
from fastapi.exception_handlers import (
    http_exception_handler,
    request_validation_exception_handler,
)
from fastapi.exceptions import RequestValidationError
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse, Response

from demos_backend.core.configuration import get_config
from demos_backend.core.monitoring.prometheus.fastapi import add_endpoint
from demos_backend.core.util import use_route_names_as_operation_ids
from demos_backend.services.notifications.api.v1.routers import v1_router

config = get_config()


notification_service = FastAPI(
    title="Demons Berlin e.V. Notification Service",
    description="Provides endpoints for sending notifications.",
    docs_url=f"{config.NOTIFICATION_SERVICE_API_V1_ROOT_PATH}/docs",
    openapi_url=f"{config.NOTIFICATION_SERVICE_API_V1_ROOT_PATH}/openapi.json",
    redoc_url=None,
)


origins = ["*"]
notification_service.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

notification_service.include_router(
    v1_router, prefix=config.NOTIFICATION_SERVICE_API_V1_ROOT_PATH
)

use_route_names_as_operation_ids(notification_service)

add_endpoint(app_name="notifications", app=notification_service, config=config)


@notification_service.exception_handler(HTTPException)
async def custom_http_exception_handler(
    request: Request, exc: HTTPException
) -> Response:
    """Log the raised exception and proceed with default exception handler."""
    logging.error(
        exc.detail,
        exc_info=exc,
        extra={
            "tags": {
                "exception_type": exc.__class__.__name__,
                "status": exc.status_code,
            },
        },
    )
    return await http_exception_handler(request, exc)


@notification_service.exception_handler(RequestValidationError)
async def custom_request_validation_exception_handler(
    request: Request, exc: RequestValidationError
) -> JSONResponse:
    """Log the raised exception and proceed with default exception handler."""
    logging.warning(
        "Validation Exception",
        exc_info=exc,
        extra={"tags": {"exception_type": "RequestValidationError", "status": 422}},
    )
    return await request_validation_exception_handler(request, exc)
