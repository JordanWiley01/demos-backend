# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Shared code for the notification service."""
from pathlib import Path
from typing import Annotated

from fastapi import Depends
from fastapi_mail import ConnectionConfig, FastMail

from demos_backend.core.configuration import Configuration, get_config
from demos_backend.services.notifications.metrics.prometheus import NotificationsMetrics

config = get_config()
metrics = NotificationsMetrics()

template_dir = Path(__file__).parent.parent.parent / "templates/email"


def get_loaded_config() -> Configuration:
    """Get the reference for the application config."""
    return config


def get_mail_client() -> FastMail:
    """Get the reference for the email client."""
    mail_connection_config = ConnectionConfig(
        MAIL_USERNAME=config.NOTIFICATION_SERVICE_MAIL_USERNAME,
        MAIL_PASSWORD=config.NOTIFICATION_SERVICE_MAIL_PASSWORD,
        MAIL_PORT=config.NOTIFICATION_SERVICE_MAIL_PORT,
        MAIL_SERVER=config.NOTIFICATION_SERVICE_MAIL_HOSTNAME,
        MAIL_STARTTLS=config.NOTIFICATION_SERVICE_MAIL_STARTTLS,
        MAIL_SSL_TLS=config.NOTIFICATION_SERVICE_MAIL_SSL_TLS,
        MAIL_FROM=config.NOTIFICATION_SERVICE_MAIL_SENDER_MAIL,
        MAIL_FROM_NAME=config.NOTIFICATION_SERVICE_MAIL_SENDER_NAME,
        USE_CREDENTIALS=config.NOTIFICATION_SERVICE_MAIL_USE_CREDENTIALS,
        VALIDATE_CERTS=config.NOTIFICATION_SERVICE_MAIL_VALIDATE_CERTS,
        TEMPLATE_FOLDER=template_dir,
    )
    return FastMail(mail_connection_config)


def get_metrics() -> NotificationsMetrics:
    """Get a metrics object for monitoring."""
    return metrics


MailClient = Annotated[FastMail, Depends(get_mail_client)]
Metrics = Annotated[NotificationsMetrics, Depends(get_metrics)]
