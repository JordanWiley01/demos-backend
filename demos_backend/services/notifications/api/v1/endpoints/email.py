# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Endpoints for requesting email sending."""

from fastapi import APIRouter, HTTPException
from fastapi_mail import MessageSchema, MessageType
from fastapi_mail.errors import ConnectionErrors
from starlette import status
from starlette.responses import JSONResponse

from demos_backend.services.notifications.api.v1.dependencies import MailClient, Metrics
from demos_backend.services.notifications.schemas import (
    AccountVerification,
    ResetPassword,
)

router = APIRouter(prefix="/mail", tags=["Mail"])


@router.post("/user_verification")
async def send_verification(
    verification_information: AccountVerification,
    mail_client: MailClient,
    metrics: Metrics,
):
    """Send a verification mail to a user."""
    language = verification_information.dict().get("language")
    message = MessageSchema(
        subject="Demos - Account Verification",
        recipients=verification_information.dict().get("user_email"),
        template_body=verification_information.dict(),
        subtype=MessageType.html,
    )
    try:
        await mail_client.send_message(
            message=message, template_name=f"user_verification_{language}.jinja2"
        )
    except ConnectionErrors as e:
        raise HTTPException(
            status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
            detail="mail connection error",
        ) from e

    metrics.sent_mails.labels(mail_type="verification").inc()
    return JSONResponse(
        status_code=status.HTTP_200_OK, content={"message": "email has been sent"}
    )


@router.post("/reset_password")
async def send_reset_password(
    reset_information: ResetPassword,
    mail_client: MailClient,
    metrics: Metrics,
):
    """Send a mail with instructions for resetting passwords to a user."""
    language = reset_information.dict().get("language")
    message = MessageSchema(
        subject="Demos - Reset Password",
        recipients=reset_information.dict().get("user_email"),
        template_body=reset_information.dict(),
        subtype=MessageType.html,
    )

    try:
        await mail_client.send_message(
            message=message, template_name=f"reset_password_{language}.jinja2"
        )
    except ConnectionErrors as e:
        raise HTTPException(
            status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
            detail="mail connection error",
        ) from e

    metrics.sent_mails.labels(mail_type="password_reset").inc()
    return JSONResponse(
        status_code=status.HTTP_200_OK, content={"message": "email has been sent"}
    )
