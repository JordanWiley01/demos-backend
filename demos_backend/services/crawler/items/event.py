# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Event item to create by spiders and use by pipelines."""
from scrapy.item import Field, Item


class EventItem(Item):
    """Event Item for scrappy to carry event data."""

    name = Field()
    short_description = Field()
    description = Field()
    start_time = Field()
    end_time = Field()
    location = Field()
    address_language = Field()
    raw_location = Field()
    raw_estimated_participants = Field()
    raw_organiser = Field()
