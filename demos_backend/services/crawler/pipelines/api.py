# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains a pipeline to send data to the backend."""
from __future__ import annotations

import asyncio
import logging

from itemadapter import ItemAdapter
from scrapy.crawler import Crawler

from demos_backend.core.clients.http.services import ServiceClient
from demos_backend.core.configuration import Configuration
from demos_backend.core.schemas.event import EventCreateCrawler
from demos_backend.core.schemas.location import LocationCreateCrawler
from demos_backend.services.crawler.items.event import EventItem
from demos_backend.services.crawler.spiders.abc_base import ABCSpider


class APIPipeline:
    """Pipeline to push events to the backend."""

    config: Configuration
    collected_events: dict[str, list[EventCreateCrawler]]
    loop: asyncio.AbstractEventLoop

    def __init__(self, config: Configuration) -> None:
        """Create an instance of the class.

        Args:
            config: A configuration object for the backend.
        """
        self.config = config
        self.collected_events = {}
        self.loop = asyncio.get_event_loop()

    @classmethod
    def from_crawler(cls: type, crawler: Crawler) -> APIPipeline:
        """Pipeline getter for crawlers.

        Args:
            crawler: A Crawler object from scrappy.
        """
        return cls(
            config=crawler.settings.get("DEMOS_CONFIG"),
        )

    def open_spider(self, spider: ABCSpider):
        """Actions to run, when a spider starts.

        Args:
            spider: The spider, which uses the pipeline.
        """
        self.collected_events[spider.name] = []

    def close_spider(self, spider: ABCSpider):
        """Actions to run, when a spider stops.

        Args:
            spider: The spider, which uses the pipeline.
        """
        logging.error(f"### sending spider {spider.name}")

        service_client = ServiceClient(config=self.config)
        self.loop.run_until_complete(
            service_client.crawler_api.get_token(
                username=self.config.CRAWLER_USER_MAIL,
                password=self.config.CRAWLER_USER_PASSWORD,
            )
        )

        chunks_sent = 0
        collected_events = self.collected_events[spider.name]
        while len(collected_events) > 0:
            chunk_to_send = collected_events[0 : self.config.CRAWLER_STORAGE_CHUNK_SIZE]
            collected_events = collected_events[
                self.config.CRAWLER_STORAGE_CHUNK_SIZE :
            ]
            self.loop.run_until_complete(
                service_client.crawler_api.send_new_crawler_events(
                    request=chunk_to_send
                )
            )
            chunks_sent += 1
            logging.info(f"send {chunks_sent} chunks")
            if -1 < self.config.CRAWLER_MAX_CHUNKS_TO_SEND <= chunks_sent:
                break
        self.loop.run_until_complete(
            service_client.crawler_api.cleanup_organisation_events(spider.organizer_id)
        )
        self.loop.run_until_complete(service_client.prepare_deletion())
        logging.error(f"### closing spider {spider.name}")

    def process_item(
        self,
        item: ItemAdapter,
        spider: ABCSpider,
    ) -> ItemAdapter:
        """Actions to run for every item spiders find.

        Args:
            item: The item the spider delivered.
            spider: The spider, which uses the pipeline.
        """
        if isinstance(item, EventItem):
            item_create = EventCreateCrawler(
                name=item["name"],
                short_description=item["short_description"],
                description=item["description"],
                url=spider.display_url,
                start_time=item["start_time"],
                end_time=item["end_time"],
                location=LocationCreateCrawler(**item["location"]),
                type_id=self.config.CRAWLER_EVENT_TYPE_ID,
                topic_ids=[self.config.CRAWLER_EVENT_TOPIC_ID],
                organising_organisation_id=spider.organizer_id,
                raw_location=item["raw_location"],
            )
            self.collected_events[spider.name].append(item_create)
        return item
