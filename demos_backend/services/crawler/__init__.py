# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the crawler to fetch events from public sources."""
import logging
import time

from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from demos_backend.core.clients.http.services import ServiceClient
from demos_backend.core.configuration import Configuration
from demos_backend.services.crawler.spiders.berlin import BerlinSpider
from demos_backend.services.crawler.spiders.dresden import DresdenSpider
from demos_backend.services.crawler.spiders.landsberg_am_lech import (
    LandsbergAmLechSpider,
)
from demos_backend.services.crawler.spiders.lueneburg import LueneburgSpider
from demos_backend.services.crawler.spiders.rostock import RostockSpider

SPIDERS = [
    BerlinSpider,
    DresdenSpider,
    RostockSpider,
    LueneburgSpider,
    LandsbergAmLechSpider,
]


async def run_scrappy(config: Configuration):
    """Run the event crawler.

    Args:
        config: A configuration object for the backend.
    """
    enabled_spiders = set()
    for index, spider_name in enumerate(config.CRAWLER_SPIDERS_PARSED):
        for spider in SPIDERS:
            if spider.name == spider_name:
                spider.organizer_id = config.CRAWLER_ORGANISATION_IDS_PARSED[index]
                enabled_spiders.add(spider)
    settings = Settings(
        values={
            "BOT_NAME": "demos_crawler",
            "USER_AGENT": "Demos Berlin e.V. (https://demonstrations.org)",
            "ROBOTSTXT_OBEY": True,
            "LOG_ENABLED": False,
            "LOG_LEVEL": "ERROR",
            "DEMOS_CONFIG": config,
            "ITEM_PIPELINES": {
                "demos_backend.services.crawler.pipelines.api.APIPipeline": 0,
            },
            "TELNETCONSOLE_ENABLED": False,
            "STATS_DUMP": True,
            "MEMUSAGE_ENABLED": False,
        }
    )
    process = CrawlerProcess(settings)
    for spider in enabled_spiders:
        logging.error(f"adding spider {spider.name}")
        process.crawl(spider)

    logging.debug("Waiting startup")
    while True:
        start = time.time()

        max_tries = 60 * 5  # 5 minutes
        wait_seconds = 1

        @retry(
            stop=stop_after_attempt(max_tries),
            wait=wait_fixed(wait_seconds),
            before=before_log(logging.getLogger(), logging.INFO),
            after=after_log(logging.getLogger(), logging.WARN),
        )
        async def check_connection() -> None:
            client = ServiceClient(config=config)
            await client.crawler_api.get_token(
                username=config.CRAWLER_USER_MAIL,
                password=config.CRAWLER_USER_PASSWORD,
            )
            await client.frontend_api.get_token(
                username=config.CRAWLER_USER_MAIL,
                password=config.CRAWLER_USER_PASSWORD,
            )
            await client.prepare_deletion()

        await check_connection()

        process.start()
        end = time.time()
        duration = end - start
        if duration < config.CRAWLER_INTERVAL_MINUTES * 60:
            logging.info(
                "Sleeping %s minutes",
                (config.CRAWLER_INTERVAL_MINUTES * 60 - duration) / 60,
            )
            time.sleep(config.CRAWLER_INTERVAL_MINUTES * 60 - duration)
