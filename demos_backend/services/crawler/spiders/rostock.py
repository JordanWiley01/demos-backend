# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the spider for Rostock."""
from __future__ import annotations

import logging
from datetime import datetime
from typing import Any

from scrapy import Selector
from scrapy.http import Response

from demos_backend.core.types import AvailableLanguages
from demos_backend.services.crawler.items.event import EventItem
from demos_backend.services.crawler.spiders.abc_base import ABCSpider


class RostockSpider(ABCSpider):
    """Spider for Rostock."""

    name = "Rostock"
    start_urls = [
        "https://rathaus.rostock.de/de/service/aemter/stadtamt/versammlungen/325429"
    ]

    organizer_id = 1
    display_url = (
        "https://rathaus.rostock.de/de/service/aemter/stadtamt/versammlungen/325429"
    )

    def parse(
        self, response: Response, **kwargs: dict[str, Any]
    ) -> list[EventItem] | EventItem:
        """Parse the fetched homepage.

        Args:
            response: The scrappy response to process.
            kwargs: kwargs
        """
        logging.info("Starting Rostock crawl")
        events_raw: list[Selector] = response.xpath("//div[@class='tiny-mce']")
        events = []
        for event in events_raw:
            title = (
                event.xpath("div/p[1]/text()")
                .get()
                .replace("„", "")
                .replace("“", "")
                .strip()
            )
            attributes = event.xpath("div/p[2]/text()").getall()
            attributes = [
                attribute.strip() for attribute in attributes if attribute != "\n"
            ]

            start_time = attributes[0]
            start_time = datetime.strptime(start_time, "%d.%m.%Y, %H:%M:%S")
            end_time = attributes[1]
            end_time = datetime.strptime(end_time, "%d.%m.%Y, %H:%M:%S")

            organizer = attributes[2]
            # event_type = attributes[3]
            estimated_participants = attributes[4]

            allowed_location_raw = attributes[7]
            # because they love to use 2 different hyphens, we need to replace them
            # look out for other occurrences
            allowed_location = (
                allowed_location_raw.replace("–", "-").replace(",", "").replace(";", "")
            )
            if " - " in allowed_location:
                # route, take the first location
                split = allowed_location.split(" - ")
                street = split[0]
                street = street.replace("(Auftaktkundgebung)", "")
                street = street.strip()
            else:
                street = self.street_from_string(allowed_location)

            description = title

            location = {
                "country": "Deutschland",
                "city": "Rostock",
                "street": street,
                "zip_code": None,
                "address_language": AvailableLanguages.GERMAN,
            }

            event_information = {
                "name": title[0:100],
                "description": description[0:1000],
                "short_description": title[0:60],
                "start_time": str(start_time),
                "end_time": str(end_time),
                "location": location,
                "raw_location": self.assemble_raw_location([allowed_location_raw]),
                "raw_estimated_participants": estimated_participants,
                "raw_organiser": organizer,
            }
            events.append(EventItem(**event_information))

        return events
