# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the spider for Landsberg am Lech."""
from __future__ import annotations

import logging
from typing import Any

from scrapy import Selector
from scrapy.http import Response

from demos_backend.core.types import AvailableLanguages
from demos_backend.services.crawler.items.event import EventItem
from demos_backend.services.crawler.spiders.abc_base import ABCSpider


class LandsbergAmLechSpider(ABCSpider):
    """Spider for Landsberg am Lech."""

    name = "Landsberg am Lech"
    start_urls = ["https://www.landkreis-landsberg.de/versammlungen/"]

    organizer_id = 5
    display_url = "https://www.landkreis-landsberg.de/versammlungen/"

    def parse(
        self, response: Response, **kwargs: dict[str, Any]
    ) -> list[EventItem] | EventItem:
        """Parse the fetched homepage.

        Args:
            response: The scrappy response to process.
            kwargs: kwargs
        """
        events_raw: list[Selector] = response.xpath("//table[@class='contenttable']")

        logging.info("Starting Landsberg am Lech crawl")

        events = []
        for event in events_raw:
            tdata = event.xpath("tbody/tr/td[2]/text()").getall()
            if len(tdata) < 4:
                continue
            organizer = tdata[0]
            raw_topic = tdata[1]
            raw_location = tdata[2]
            raw_time = tdata[3]
            if not raw_topic or not raw_location or not raw_time:
                continue
            street = self.street_from_string(raw_location)
            parsed_datetimes = self.time_tuples_from_string(raw_time=raw_time)

            description = raw_topic

            for time_tuple in parsed_datetimes:
                location = {
                    "country": "Deutschland",
                    "city": "Landsberg am Lech",
                    "street": street,
                    "zip_code": None,
                    "address_language": AvailableLanguages.GERMAN,
                }

                event_information = {
                    "name": raw_topic[0:100],
                    "description": description[0:1000],
                    "short_description": raw_topic[0:60],
                    "start_time": str(time_tuple[0]),
                    "end_time": str(time_tuple[1]),
                    "location": location,
                    "raw_location": self.assemble_raw_location([raw_location]),
                    "raw_estimated_participants": None,
                    "raw_organiser": organizer,
                }
                events.append(EventItem(**event_information))

        return events
