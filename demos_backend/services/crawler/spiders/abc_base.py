# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains a ABC spider to derive from."""
from __future__ import annotations

import logging
from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from typing import Any

import scrapy
from dateparser.search import search_dates
from scrapy.http import Response

from demos_backend.services.crawler.items.event import EventItem


class ABCSpider(ABC, scrapy.Spider):
    """ABC Spider to derive from."""

    @property
    @abstractmethod
    def name(self) -> str:
        """The name of the spider."""
        pass

    @property
    @abstractmethod
    def start_urls(self) -> list[str]:
        """The url the crawler starts."""
        pass

    @property
    @abstractmethod
    def organizer_id(self) -> int:
        """The ID of the according organisation in the database."""
        pass

    @property
    @abstractmethod
    def display_url(self) -> str:
        """The url to show in the frontend for events."""
        pass

    @abstractmethod
    def parse(
        self, response: Response, **kwargs: dict[str, Any]
    ) -> list[EventItem] | EventItem:
        """Parse the fetched homepage.

        Args:
            response: The scrappy response to process.
            kwargs: kwargs
        """
        pass

    @staticmethod
    def assemble_raw_location(data: list[str | None]) -> str:
        """Assemble a list of strings into one raw location string to display in the frontend.

        Args:
            data: The data to concat.

        Returns:
            str: The assembled data
        """
        locations = [loc for loc in data if loc is not None]
        return " | ".join(locations)[0:1000]

    @staticmethod
    def street_from_string(raw_location: str) -> str:
        """Parse a string containing location information into a street name for events.

        Args:
            raw_location: The raw data to parse

        Returns:
            str: The parsed data.
        """
        raw_location = raw_location.replace("–", "-").replace(",", "").replace(";", "")
        if (
            "straße" in raw_location
            or "-Straße" in raw_location
            or "platz" in raw_location
        ):
            split_data = raw_location.split(" ")
            for split in split_data:
                if "platz" in split:
                    if split.endswith("latzes"):
                        raw_location = split[:-2]
                    else:
                        raw_location = split
                    break
            else:
                for split in split_data:
                    if "straße" in split or "-Straße" in split:
                        raw_location = split
                        break
            raw_location = raw_location.strip()

        return raw_location[0:150]

    @staticmethod
    def time_tuples_from_string(raw_time: str) -> list[tuple[datetime, datetime]]:
        """Parse a string containing time into a list of start/end times.

        Args:
            raw_time: The raw data to parse

        Returns:
            list[tuple[datetime, datetime]]: A list containing tuples with start and end time.
        """
        parsed_dates = search_dates(
            text=raw_time,
            languages=["de"],
            settings={
                "RELATIVE_BASE": datetime(1970, 1, 1, 13, 13),
                "DATE_ORDER": "DMY",
            },
        )
        logging.error(parsed_dates)
        if not parsed_dates:
            return []
        dates = []
        times = []
        parsed_datetimes = []

        for date in parsed_dates:
            date_time = date[1]
            if date_time.year == 1970:
                times.append(date_time)
            else:
                dates.append(date_time)

        if len(times) == 0:
            times.append(datetime(1970, 1, 1, 0, 0, 0))
            times.append(datetime(1970, 1, 1, 23, 59, 59))

        if len(times) == 1:
            times.append(times[0] + timedelta(hours=1))

        if len(times) > 2:
            times = times[0:2]
        for date in dates:
            start_time = datetime(
                year=date.year,
                month=date.month,
                day=date.day,
                hour=times[0].hour,
                minute=times[0].minute,
            )
            end_time = datetime(
                year=date.year,
                month=date.month,
                day=date.day,
                hour=times[1].hour,
                minute=times[1].minute,
            )
            parsed_datetimes.append((start_time, end_time))

        return parsed_datetimes
