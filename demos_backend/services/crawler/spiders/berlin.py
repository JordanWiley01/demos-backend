# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the spider for Berlin."""
from __future__ import annotations

import logging
from datetime import datetime
from typing import Any

from scrapy.http import Response

from demos_backend.core.types import AvailableLanguages
from demos_backend.services.crawler.items.event import EventItem
from demos_backend.services.crawler.spiders.abc_base import ABCSpider


class BerlinSpider(ABCSpider):
    """Spider for Berlin."""

    name = "Berlin"
    start_urls = [
        "https://www.berlin.de/polizei/service/versammlungsbehoerde/versammlungen-aufzuege/"
    ]

    organizer_id = 1
    display_url = (
        "https://www.berlin.de/polizei/service/"
        "versammlungsbehoerde/versammlungen-aufzuege/"
    )

    def parse(
        self, response: Response, **kwargs: dict[str, Any]
    ) -> list[EventItem] | EventItem:
        """Parse the fetched homepage.

        Args:
            response: The scrappy response to process.
            kwargs: kwargs
        """
        tbody = response.xpath("//tbody")
        events_raw = tbody.xpath(".//tr")

        logging.info("Starting Berlin crawl")

        events = []

        for _index, event in enumerate(events_raw):
            aufzugstrecke = event.xpath('.//td[@headers="Aufzugsstrecke"]/text()').get()
            versammlungsort = event.xpath(
                './/td[@headers="Versammlungsort"]/text()'
            ).get()
            datum = event.xpath('.//td[@headers="Datum"]/text()').get()
            start_time = event.xpath('.//td[@headers="Von"]/text()').get()
            end_time = event.xpath('.//td[@headers="Bis"]/text()').get()
            # Street by default Versammlungsort
            street_string = str(versammlungsort)
            zip_code = str(event.xpath('.//td[@headers="PLZ"]/text()').get())
            zip_code = zip_code if zip_code != "None" else None
            event_name = str(event.xpath('.//td[@headers="Thema"]/text()').get())[
                :100
            ]  # Trimmed to max 30 char
            event_description_short = str(
                event.xpath('.//td[@headers="Thema"]/text()').get()
            )[:60]
            event_description = str(
                event.xpath('.//td[@headers="Thema"]/text()').get()
            )[:1000]

            # No Date is available
            if datum is None:
                continue

            # No Address is available
            if versammlungsort is None and aufzugstrecke is None:
                continue

            if versammlungsort is None:
                street_string = (
                    str(aufzugstrecke).split("-")[0].strip().split("(")[0].strip()
                )

            start_time_string = str(datum) + " " + str(start_time)
            start_time = datetime.strptime(start_time_string, "%d.%m.%Y %H:%M")

            end_time_string = str(datum) + " " + str(end_time)
            end_time = datetime.strptime(end_time_string, "%d.%m.%Y %H:%M")

            if (
                end_time.time()
                == datetime.now()
                .replace(hour=0, minute=0, second=0, microsecond=0)
                .time()
            ):
                end_time = end_time.replace(hour=23, minute=59)

            # This is not necessary anymore
            if "S-Bhf." in street_string.split(" ")[0]:
                street_string = "Bahnhof " + street_string.split(" ")[-1]

            location = {
                "country": "Deutschland",
                "city": "Berlin",
                "street": street_string,
                "zip_code": zip_code,
                "address_language": AvailableLanguages.GERMAN,
            }

            event_information = {
                "name": event_name,
                "description": event_description,
                "short_description": event_description_short,
                "start_time": str(start_time),
                "end_time": str(end_time),
                "location": location,
                "raw_location": self.assemble_raw_location(
                    [versammlungsort, aufzugstrecke]
                ),
                "raw_estimated_participants": None,
                "raw_organiser": None,
            }
            events.append(EventItem(**event_information))

        return events
