# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the spider for Lüneburg."""
from __future__ import annotations

import logging
from typing import Any

from scrapy import Selector
from scrapy.http import Response

from demos_backend.core.types import AvailableLanguages
from demos_backend.services.crawler.items.event import EventItem
from demos_backend.services.crawler.spiders.abc_base import ABCSpider


class LueneburgSpider(ABCSpider):
    """Spider for Lüneburg."""

    name = "Lüneburg"
    start_urls = ["https://www.hansestadt-lueneburg.de/rathaus/bekanntmachungen.html"]

    organizer_id = 4
    display_url = "https://www.hansestadt-lueneburg.de/rathaus/bekanntmachungen.html#versammlungen"

    def parse(
        self, response: Response, **kwargs: dict[str, Any]
    ) -> list[EventItem] | EventItem:
        """Parse the fetched homepage.

        Args:
            response: The scrappy response to process.
            kwargs: kwargs
        """
        events_raw: list[Selector] = response.xpath(
            "//div[@class='AccordionItem-content']"
        )

        logging.info("Starting Lüneburg crawl")

        events = []
        for event in events_raw:
            table_header = event.xpath("p[1]/strong/text()").get()
            if not table_header:
                continue
            title = event.xpath("p[1]/text()").get()
            if not title:
                continue
            title = title.replace("„", "").replace("“", "").replace('"', "").strip()
            route = event.xpath("p[2]/text()").get()
            if not route:
                continue
            time_strings = event.xpath("p[3]/text()").getall()

            street = self.street_from_string(route)

            for raw_string in time_strings:
                parsed_datetimes = self.time_tuples_from_string(raw_time=raw_string)

                for time_tuple in parsed_datetimes:
                    location = {
                        "country": "Deutschland",
                        "city": "Lüneburg",
                        "street": street,
                        "zip_code": None,
                        "address_language": AvailableLanguages.GERMAN,
                    }

                    event_information = {
                        "name": title[0:100],
                        "description": title[0:1000],
                        "short_description": title[0:60],
                        "start_time": str(time_tuple[0]),
                        "end_time": str(time_tuple[1]),
                        "location": location,
                        "raw_location": self.assemble_raw_location([route]),
                        "raw_estimated_participants": None,
                        "raw_organiser": None,
                    }
                    events.append(EventItem(**event_information))
        return events
