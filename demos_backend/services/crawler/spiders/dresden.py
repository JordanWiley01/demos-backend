# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the spider for Dresden."""
from __future__ import annotations

import json
import logging
from datetime import datetime
from typing import Any

from scrapy.http import Response

from demos_backend.core.types import AvailableLanguages
from demos_backend.services.crawler.items.event import EventItem
from demos_backend.services.crawler.spiders.abc_base import ABCSpider


class DresdenSpider(ABCSpider):
    """Spider for Dresden."""

    name = "Dresden"
    start_urls = [
        "https://www.dresden.de/data_ext/versammlungsuebersicht/Versammlungen.json"
    ]

    organizer_id = 1
    display_url = (
        "https://www.dresden.de/de/rathaus/dienstleistungen/versammlungsuebersicht.php"
    )

    def parse(
        self, response: Response, **kwargs: dict[str, Any]
    ) -> list[EventItem] | EventItem:
        """Parse the fetched homepage.

        Args:
            response: The scrappy response to process.
            kwargs: kwargs
        """
        logging.info("Starting Dresden crawl")

        events = []

        json_data = json.loads(response.text)

        for _index, item in enumerate(json_data["Versammlungen"]):
            if item["Status"] != "beschieden":
                continue
            if item["Ort"] is None and item["Startpunkt"] is None:
                continue
            place = item["Ort"] if item["Startpunkt"] is None else item["Startpunkt"]
            organizer = (
                item["Veranstalter"]
                if item["Veranstalter"] is not None
                else "Unbekannt"
            )
            participants = item["Teilnehmer"]
            topic = item["Thema"]
            event_name = str(topic)[:100]  # Trimmed to max 30 char
            event_description_short = str(topic)[:59]
            event_description = str(topic)[:1000]

            date = item["Datum"]
            time = item["Zeit"]

            start_time = time.split(" - ")[0].replace(".", ":")
            date = date.replace("-", ".")
            date = datetime.strptime(date, "%Y.%m.%d")
            date = date.strftime("%d.%m.%Y")

            start_time_string = date + " " + start_time
            start_time = datetime.strptime(start_time_string, "%d.%m.%Y %H:%M")

            end_time = time.split(" - ")[1].strip(" Uhr").replace(".", ":")
            end_time_string = date + " " + end_time
            end_time = datetime.strptime(end_time_string, "%d.%m.%Y %H:%M")

            if (
                end_time.time()
                == datetime.now()
                .replace(hour=0, minute=0, second=0, microsecond=0)
                .time()
            ):
                end_time = end_time.replace(hour=23, minute=59)

            street_string = place

            location = {
                "country": "Deutschland",
                "city": "Dresden",
                "street": street_string,
                "zip_code": None,
                "address_language": AvailableLanguages.GERMAN,
            }

            event_information = {
                "name": event_name,
                "description": event_description,
                "short_description": event_description_short,
                "start_time": str(start_time),
                "end_time": str(end_time),
                "location": location,
                "raw_location": self.assemble_raw_location(
                    [item["Ort"], item["Startpunkt"]]
                ),
                "raw_estimated_participants": participants,
                "raw_organiser": organizer,
            }
            events.append(EventItem(**event_information))

        return events
