# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from typing import List, Optional

from sqlmodel import Field, Relationship, SQLModel


class Location(SQLModel, table=True):
    """Database model / table."""

    id: Optional[int] = Field(default=None, primary_key=True)
    latitude: Optional[float] = Field(default=None)
    longitude: Optional[float] = Field(default=None)
    country: Optional[str] = Field(default=None)
    city: Optional[str] = Field(default=None)
    street: Optional[str] = Field(default=None)
    zip_code: Optional[str] = Field(default=None)
    house_number: Optional[str] = Field(default=None)

    events: List["Event"] = Relationship(back_populates="location")
    organisations: List["Organisation"] = Relationship(back_populates="location")
