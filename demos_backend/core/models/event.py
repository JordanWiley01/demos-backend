# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from datetime import datetime
from typing import List, Optional

from sqlmodel import Field, Relationship, SQLModel

from .event_type import EventType
from .link_tables import EventCoOrganisationLink, EventTopicLink
from .location import Location
from .user import User


class Event(SQLModel, table=True):
    """Database model / table."""

    id: Optional[int] = Field(default=None, primary_key=True)
    name: str
    short_description: Optional[str] = Field(default=None)
    description: Optional[str] = Field(default=None)
    url: Optional[str] = Field(default=None)
    start_time: datetime
    end_time: datetime
    created: datetime
    updated: datetime
    visible: bool = Field(default=True)

    raw_location: Optional[str] = Field(default=None)
    raw_estimated_participants: Optional[str] = Field(default=None)
    raw_organiser: Optional[str] = Field(default=None)

    creator_id: int = Field(foreign_key="user.id")
    creator: User = Relationship(back_populates="created_events")
    organising_organisation_id: int = Field(foreign_key="organisation.id")
    organising_organisation: "Organisation" = Relationship(
        back_populates="organised_events"
    )
    location_id: int = Field(foreign_key="location.id")
    location: Location = Relationship(back_populates="events")
    type_id: int = Field(foreign_key="eventtype.id")
    type: EventType = Relationship(back_populates="events")

    reports: List["EventReport"] = Relationship(back_populates="reported_event")

    topics: List["Topic"] = Relationship(
        back_populates="events",
        link_model=EventTopicLink,
    )
    co_organisers: List["Organisation"] = Relationship(
        back_populates="co_organized_events",
        link_model=EventCoOrganisationLink,
    )
