# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from __future__ import annotations

from datetime import datetime
from typing import Type

from beanie import Document, Indexed
from pydantic import Field

from demos_backend.core.schemas.image import ImageCreate
from demos_backend.core.util import get_random_uid


class Image(Document):
    """Database model / table."""

    name: Indexed(str)
    creator_user_id: str
    created: datetime = Field(default_factory=datetime.now)
    image: bytes

    @classmethod
    async def find_by_name(cls: Type[Image], image_name: str) -> Image | None:
        """Find an image by name."""
        return await cls.find_one(cls.name == image_name)

    @classmethod
    async def new(cls: Type[Image], schema: ImageCreate) -> Image:
        """Create new image with schema."""
        if schema.desired_name:
            image_name = schema.desired_name
        else:
            image_name = get_random_uid()
        while await cls.find_by_name(image_name):
            image_name = get_random_uid()

        image = cls(
            creator_user_id=schema.creator_user_id, image=schema.image, name=image_name
        )
        await image.create()
        return image
