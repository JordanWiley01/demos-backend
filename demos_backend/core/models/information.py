# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from datetime import datetime
from typing import Optional

from sqlmodel import Field, SQLModel


class Information(SQLModel, table=True):
    """Database model / table."""

    id: Optional[int] = Field(default=None, primary_key=True)
    name_native: str
    name_english: Optional[str] = Field(default=None)
    url_native: str
    url_english: Optional[str] = Field(default=None)
    country: str
    city: str
    zip_code: str
    created: datetime
    visible: bool = Field(default=False)
