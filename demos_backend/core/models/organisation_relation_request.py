# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from datetime import datetime
from typing import Optional

from sqlmodel import Field, Relationship, SQLModel

from demos_backend.core.types import RequestStatus

from .organisation import Organisation


class OrganisationRelationRequest(SQLModel, table=True):
    """Database model / table."""

    # id: Optional[int] = Field(default=None, primary_key=True)
    organisation_requester_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )
    organisation_requested_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )
    organisation_requester: Organisation = Relationship(
        back_populates="outgoing_relation_requests",
        sa_relationship_kwargs={
            "primaryjoin": "OrganisationRelationRequest.organisation_requester_id == "
            "Organisation.id",
        },
    )
    organisation_requested: Organisation = Relationship(
        back_populates="incoming_relation_requests",
        sa_relationship_kwargs={
            "primaryjoin": "OrganisationRelationRequest.organisation_requested_id == "
            "Organisation.id",
        },
    )
    created: datetime
    status: RequestStatus
