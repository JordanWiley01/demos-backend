# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from __future__ import annotations

from datetime import datetime
from typing import Type

import pymongo
from beanie import Document, Indexed
from pydantic import Field, constr

from demos_backend.core.schemas.internal_document import InternalDocumentCreate
from demos_backend.core.types import AvailableLanguages, InternalDocumentType


class InternalDocument(Document):
    """Database model / table."""

    file_name: Indexed(constr(min_length=1, max_length=100))
    display_name: Indexed(constr(min_length=1, max_length=100))
    creator_user_id: str
    created: datetime = Field(default_factory=datetime.now)
    language: AvailableLanguages
    type: InternalDocumentType
    data: bytes

    @classmethod
    async def find_by_file_name(
        cls: Type[InternalDocument], file_name: str
    ) -> InternalDocument | None:
        """Find a document by file name."""
        return await cls.find_one(cls.file_name == file_name)

    @classmethod
    async def find_by_display_name(
        cls: Type[InternalDocument], display_name: str
    ) -> InternalDocument | None:
        """Find a document by display name."""
        return await cls.find_one(cls.display_name == display_name)

    @classmethod
    async def new(
        cls: Type[InternalDocument], schema: InternalDocumentCreate
    ) -> InternalDocument:
        """Create new document with schema."""
        if await cls.find_by_file_name(schema.file_name):
            raise ValueError("file name already exists")
        if await cls.find_by_display_name(schema.display_name):
            raise ValueError("display name already exists")

        document = cls(
            file_name=schema.file_name,
            display_name=schema.display_name,
            creator_user_id=schema.creator_user_id,
            language=schema.language,
            type=schema.type,
            data=schema.data,
        )
        await document.create()
        return document

    @classmethod
    async def get_all_names(cls: Type[InternalDocument]) -> dict[str, str]:
        """Create new image with schema.

        Returns:
            dict[str,str]: A dictionary with display_name:file_name
        """
        documents: list[InternalDocument] = (
            await cls.find_all().sort([(cls.display_name, pymongo.ASCENDING)]).to_list()
        )

        return {document.display_name: document.file_name for document in documents}
