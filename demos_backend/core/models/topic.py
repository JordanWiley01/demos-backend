# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from typing import List, Optional

from sqlmodel import Field, Relationship, SQLModel

from .link_tables import EventTopicLink, TopicOrganisationLink


class Topic(SQLModel, table=True):
    """Database model / table."""

    id: Optional[int] = Field(default=None, primary_key=True)
    name_en: str = Field(index=True)
    name_de: str = Field(index=True)

    events: List["Event"] = Relationship(
        back_populates="topics",
        link_model=EventTopicLink,
    )
    organisations: List["Organisation"] = Relationship(
        back_populates="topics",
        link_model=TopicOrganisationLink,
    )
