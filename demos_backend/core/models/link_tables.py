# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database N:M link tables."""

from typing import Optional

from sqlmodel import Field, SQLModel


class ReporterOrganisationOrganisationReportLink(SQLModel, table=True):
    """Database N:M link table."""

    organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )
    organisation_report_id: Optional[int] = Field(
        default=None, foreign_key="organisationreport.id", primary_key=True
    )


class ReportedOrganisationOrganisationReportLink(SQLModel, table=True):
    """Database N:M link table."""

    organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )
    organisation_report_id: Optional[int] = Field(
        default=None, foreign_key="organisationreport.id", primary_key=True
    )


class AdminOrganisationLink(SQLModel, table=True):
    """Database N:M link table."""

    user_id: Optional[int] = Field(
        default=None, foreign_key="user.id", primary_key=True
    )
    organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )


class FollowerOrganisationLink(SQLModel, table=True):
    """Database N:M link table."""

    user_id: Optional[int] = Field(
        default=None, foreign_key="user.id", primary_key=True
    )
    organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )


class EventTopicLink(SQLModel, table=True):
    """Database N:M link table."""

    topic_id: Optional[int] = Field(
        default=None, foreign_key="topic.id", primary_key=True
    )
    event_id: Optional[int] = Field(
        default=None, foreign_key="event.id", primary_key=True
    )


class EventCoOrganisationLink(SQLModel, table=True):
    """Database N:M link table."""

    organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )
    event_id: Optional[int] = Field(
        default=None, foreign_key="event.id", primary_key=True
    )


class TopicOrganisationLink(SQLModel, table=True):
    """Database N:M link table."""

    topic_id: Optional[int] = Field(
        default=None, foreign_key="topic.id", primary_key=True
    )
    organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )


class OrganisationRelationLink(SQLModel, table=True):
    """Database N:M link table."""

    organisation1_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )
    organisation2_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id", primary_key=True
    )
