# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from datetime import datetime
from typing import List, Optional

from sqlmodel import Field, Relationship, SQLModel, UniqueConstraint

from ..types import RequestStatus
from .event import Event
from .link_tables import (
    AdminOrganisationLink,
    EventCoOrganisationLink,
    FollowerOrganisationLink,
    OrganisationRelationLink,
    ReportedOrganisationOrganisationReportLink,
    ReporterOrganisationOrganisationReportLink,
    TopicOrganisationLink,
)
from .location import Location
from .organisation_type import OrganisationType
from .user import User


class Organisation(SQLModel, table=True):
    """Database model / table."""

    __table_args__ = (UniqueConstraint("name"),)
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str = Field(index=True)
    description: Optional[str] = Field(default=None)
    url: Optional[str] = Field(default=None)
    public_mail_contact: Optional[str] = Field(default=None)
    internal_point_of_contact: str
    created: datetime
    image_name: Optional[str]
    approved: RequestStatus = Field(default=RequestStatus.PENDING)
    visible: bool = Field(default=False)

    # 1:m relations
    creator_id: int = Field(foreign_key="user.id")
    creator: User = Relationship(back_populates="created_organisations")
    location_id: Optional[int] = Field(default=None, foreign_key="location.id")
    location: Location = Relationship(back_populates="organisations")
    type_id: int = Field(foreign_key="organisationtype.id")
    type: OrganisationType = Relationship(back_populates="organisations")
    event_reports: List["EventReport"] = Relationship(
        back_populates="reporter_organisation"
    )
    event_type_requests: List["EventTypeRequest"] = Relationship(
        back_populates="requester_organisation"
    )
    organisation_type_requests: List["OrganisationTypeRequest"] = Relationship(
        back_populates="requester_organisation"
    )
    topic_requests: List["TopicRequest"] = Relationship(
        back_populates="requester_organisation"
    )
    organised_events: List[Event] = Relationship(
        back_populates="organising_organisation"
    )

    # n:m relations
    relations: List["Organisation"] = Relationship(
        back_populates="relations",
        link_model=OrganisationRelationLink,
        sa_relationship_kwargs={
            "primaryjoin": "Organisation.id == "
            "OrganisationRelationLink.organisation2_id",
            "secondaryjoin": "Organisation.id == "
            "OrganisationRelationLink.organisation1_id",
        },
    )

    admins: List["User"] = Relationship(
        back_populates="administrated_organisations",
        link_model=AdminOrganisationLink,
    )

    followers: List["User"] = Relationship(
        back_populates="followed_organisations",
        link_model=FollowerOrganisationLink,
    )
    co_organized_events: List["Event"] = Relationship(
        back_populates="co_organisers",
        link_model=EventCoOrganisationLink,
    )
    organisation_reports: List["OrganisationReport"] = Relationship(
        back_populates="reporter_organisation",
        link_model=ReporterOrganisationOrganisationReportLink,
    )
    reports: List["OrganisationReport"] = Relationship(
        back_populates="reported_organisation",
        link_model=ReportedOrganisationOrganisationReportLink,
    )
    topics: List["Topic"] = Relationship(
        back_populates="organisations",
        link_model=TopicOrganisationLink,
    )

    outgoing_relation_requests: List["OrganisationRelationRequest"] = Relationship(
        back_populates="organisation_requester",
        sa_relationship_kwargs={
            "primaryjoin": "Organisation.id == "
            "OrganisationRelationRequest.organisation_requester_id",
        },
    )
    incoming_relation_requests: List["OrganisationRelationRequest"] = Relationship(
        back_populates="organisation_requested",
        sa_relationship_kwargs={
            "primaryjoin": "Organisation.id == "
            "OrganisationRelationRequest.organisation_requested_id",
        },
    )
