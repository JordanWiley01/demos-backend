# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from datetime import datetime
from typing import Optional

from sqlmodel import Field, Relationship, SQLModel

from demos_backend.core.types import ReportStatus

from .event import Event
from .organisation import Organisation
from .user import User


class EventReport(SQLModel, table=True):
    """Database model / table."""

    id: Optional[int] = Field(default=None, primary_key=True)
    created: datetime
    status: ReportStatus
    description: str

    reported_event_id: int = Field(foreign_key="event.id")
    reported_event: Event = Relationship(back_populates="reports")
    reporter_user_id: Optional[int] = Field(default=None, foreign_key="user.id")
    reporter_user: Optional[User] = Relationship(back_populates="event_reports")
    reporter_organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id"
    )
    reporter_organisation: Optional[Organisation] = Relationship(
        back_populates="event_reports"
    )
