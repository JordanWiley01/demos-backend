# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""
from datetime import datetime
from typing import List, Optional

from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel, UniqueConstraint

from .link_tables import AdminOrganisationLink, FollowerOrganisationLink


class User(SQLModel, table=True):
    """Database model / table."""

    __table_args__ = (UniqueConstraint("email"), UniqueConstraint("username"))
    id: Optional[int] = Field(default=None, primary_key=True)
    email: EmailStr = Field(index=True)
    username: str = Field(index=True)
    hashed_password: str
    active: bool = Field(default=True)
    admin: bool = Field(default=False)
    verified: bool = Field(default=False)
    privacy_policy: bool = Field(default=False)
    receive_newsletter: bool = Field(default=False)
    created: datetime

    created_events: List["Event"] = Relationship(back_populates="creator")
    created_organisations: List["Organisation"] = Relationship(back_populates="creator")
    event_reports: List["EventReport"] = Relationship(back_populates="reporter_user")
    organisation_reports: List["OrganisationReport"] = Relationship(
        back_populates="reporter_user"
    )
    event_type_requests: List["EventTypeRequest"] = Relationship(
        back_populates="requester_user"
    )
    organisation_type_requests: List["OrganisationTypeRequest"] = Relationship(
        back_populates="requester_user"
    )
    topic_requests: List["TopicRequest"] = Relationship(back_populates="requester_user")

    administrated_organisations: List["Organisation"] = Relationship(
        back_populates="admins",
        link_model=AdminOrganisationLink,
    )
    followed_organisations: List["Organisation"] = Relationship(
        back_populates="followers",
        link_model=FollowerOrganisationLink,
    )
