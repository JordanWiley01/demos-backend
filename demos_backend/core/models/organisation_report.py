# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from datetime import datetime
from typing import Optional

from sqlmodel import Field, Relationship, SQLModel

from demos_backend.core.types import ReportStatus

from .link_tables import (
    ReportedOrganisationOrganisationReportLink,
    ReporterOrganisationOrganisationReportLink,
)
from .organisation import Organisation
from .user import User


class OrganisationReport(SQLModel, table=True):
    """Database model / table."""

    id: Optional[int] = Field(default=None, primary_key=True)
    created: datetime
    status: ReportStatus
    description: str

    reported_organisation: Organisation = Relationship(
        back_populates="reports",
        link_model=ReportedOrganisationOrganisationReportLink,
    )

    reporter_user_id: Optional[int] = Field(default=None, foreign_key="user.id")
    reporter_user: Optional[User] = Relationship(back_populates="organisation_reports")

    reporter_organisation: Optional[Organisation] = Relationship(
        back_populates="organisation_reports",
        link_model=ReporterOrganisationOrganisationReportLink,
    )

    reporter_organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id"
    )

    reported_organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id"
    )
