# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database Model."""

from datetime import datetime
from typing import Optional

from sqlmodel import Field, Relationship, SQLModel

from demos_backend.core.types import RequestStatus

from .organisation import Organisation
from .user import User


class TopicRequest(SQLModel, table=True):
    """Database model / table."""

    id: Optional[int] = Field(default=None, primary_key=True)
    name: str
    created: datetime
    status: RequestStatus

    requester_user_id: Optional[int] = Field(default=None, foreign_key="user.id")
    requester_user: Optional[User] = Relationship(back_populates="topic_requests")
    requester_organisation_id: Optional[int] = Field(
        default=None, foreign_key="organisation.id"
    )
    requester_organisation: Optional[Organisation] = Relationship(
        back_populates="topic_requests"
    )
