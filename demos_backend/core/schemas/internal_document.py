# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for image related routes."""

from pydantic import BaseModel, constr

from demos_backend.core.types import AvailableLanguages, InternalDocumentType


class InternalDocumentCreate(BaseModel):
    """API input schema of an Image."""

    file_name: constr(min_length=1, max_length=100)
    display_name: constr(min_length=1, max_length=100)
    creator_user_id: str
    language: AvailableLanguages
    type: InternalDocumentType
    data: bytes
