# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for image related routes."""

from typing import Optional

from pydantic import BaseModel


class ImageCreate(BaseModel):
    """API input schema of an Image."""

    creator_user_id: str
    image: bytes
    desired_name: Optional[str] = None
