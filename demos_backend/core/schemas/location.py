# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for location related routes."""

from __future__ import annotations

from typing import Optional

from pydantic import BaseModel, constr

from demos_backend.core.types import AvailableLanguages


class LocationCreate(BaseModel):
    """API input schema to create a Location."""

    latitude: Optional[float] = None
    longitude: Optional[float] = None
    country: Optional[constr(max_length=100)] = None
    city: Optional[constr(max_length=100)] = None
    street: Optional[constr(max_length=150)] = None
    zip_code: Optional[constr(max_length=20)] = None
    house_number: Optional[constr(max_length=10)] = None
    address_language: AvailableLanguages = AvailableLanguages.ENGLISH


class LocationCreateCrawler(BaseModel):
    """API input schema to create a Location by a crawler."""

    country: Optional[constr(max_length=100)] = None
    city: Optional[constr(max_length=100)] = None
    street: Optional[constr(max_length=150)] = None
    zip_code: Optional[constr(max_length=20)] = None
    address_language: AvailableLanguages = AvailableLanguages.ENGLISH


class LocationUpdate(BaseModel):
    """API input schema to update a Location."""

    latitude: Optional[float] = None
    longitude: Optional[float] = None
    country: Optional[constr(max_length=100)] = None
    city: Optional[constr(max_length=100)] = None
    street: Optional[constr(max_length=150)] = None
    zip_code: Optional[constr(max_length=20)] = None
    house_number: Optional[constr(max_length=10)] = None
    address_language: AvailableLanguages = AvailableLanguages.ENGLISH


class LocationInDB(BaseModel):
    """API return schema of a Location."""

    id: int
    latitude: Optional[float] = None
    longitude: Optional[float] = None
    country: Optional[str] = None
    city: Optional[str] = None
    street: Optional[str] = None
    zip_code: Optional[str] = None
    house_number: Optional[str] = None

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
