# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for user related routes."""
from __future__ import annotations

from typing import Optional

from pydantic import BaseModel, EmailStr, constr


class UserCreate(BaseModel):
    """API input schema to create a user."""

    email: EmailStr
    username: constr(max_length=100)
    password: constr(min_length=16)


class UserUpdate(BaseModel):
    """API input schema to update a user."""

    email: Optional[EmailStr] = None
    username: Optional[constr(max_length=100)] = None
    password: Optional[constr(min_length=16)] = None
    receive_newsletter: Optional[bool] = None


class UserSuperUserUpdate(UserUpdate):
    """API input schema to update a user as a superuser."""

    active: Optional[bool] = None
    admin: Optional[bool] = None
    verified: Optional[bool] = None


class UserInDB(BaseModel):
    """API return schema of a user."""

    id: int
    username: str
    email: EmailStr
    active: bool
    admin: bool
    receive_newsletter: bool

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True


class UserMail(BaseModel):
    """API input schema for email addresses."""

    email: EmailStr
