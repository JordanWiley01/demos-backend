# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Schemas used to communicate with the notification service."""

from typing import List, Optional

from pydantic import BaseModel, EmailStr


class UserNotificationRequest(BaseModel):
    """Schema for requesting a notification for a user."""

    user_email: List[EmailStr]
    base_domain: str
    username: str
    token: str
    language: Optional[str] = "de"


class UserResetPasswordRequest(BaseModel):
    """Schema for requesting a password reset for a user."""

    user_email: List[EmailStr]
    base_domain: str
    token: str
    language: Optional[str] = "de"
