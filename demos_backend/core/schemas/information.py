# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for information related routes."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from pydantic import BaseModel, conint, constr


class InformationCreate(BaseModel):
    """API input schema to create an Information."""

    name_native: constr(max_length=250)
    name_english: Optional[constr(max_length=250)] = None
    url_native: constr(max_length=250)
    url_english: Optional[constr(max_length=250)] = None
    country: constr(max_length=100)
    city: constr(max_length=100)
    zip_code: constr(max_length=20)


class InformationUpdate(BaseModel):
    """API input schema to update an Information."""

    name_native: Optional[constr(max_length=250)] = None
    name_english: Optional[constr(max_length=250)] = None
    url_native: Optional[constr(max_length=250)] = None
    url_english: Optional[constr(max_length=250)] = None
    country: Optional[constr(max_length=100)] = None
    city: Optional[constr(max_length=100)] = None
    zip_code: Optional[constr(max_length=20)] = None


class InformationQuery(BaseModel):
    """API input schema to search an Information."""

    country: constr(max_length=100)
    city: constr(max_length=100) = ""
    zip_code: Optional[constr(max_length=20)] = None
    limit: conint(ge=1, le=25) = 25
    offset: int = 0


class InformationInDB(BaseModel):
    """API return schema of an Information."""

    id: int
    name_native: str
    name_english: Optional[str] = None
    url_native: str
    url_english: Optional[str] = None
    country: str
    city: str
    zip_code: str
    created: datetime

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
