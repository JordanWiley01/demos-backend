# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for all routes."""


from pydantic import BaseModel


class Message(BaseModel):
    """API input schema of an Image."""

    detail: str
