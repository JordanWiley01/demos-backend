# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for event type requests related routes."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from pydantic import BaseModel, constr

from demos_backend.core.types import RequestStatus


class EventTypeRequestCreate(BaseModel):
    """API input schema to create an Event Type Request."""

    name: constr(max_length=250)
    requester_organisation_id: Optional[int] = None


class EventTypeRequestUpdate(BaseModel):
    """API input schema to update an Event Type Request."""

    status: RequestStatus


class EventTypeRequestInDB(BaseModel):
    """API output schema of an Event Type Request."""

    id: int
    name: str
    created: datetime
    requester_organisation_id: int
    requester_user_id: int
    status: RequestStatus

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
