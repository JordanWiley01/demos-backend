# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for organisation report related routes."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from pydantic import BaseModel, constr

from demos_backend.core.types import ReportStatus


class OrganisationReportCreate(BaseModel):
    """API input schema to create an Organisation Report."""

    reported_organisation_id: int
    description: constr(max_length=250)
    reporter_organisation_id: Optional[int] = None


class OrganisationReportUpdate(BaseModel):
    """API input schema to update an Organisation Report."""

    status: ReportStatus


class OrganisationReportInDB(BaseModel):
    """API return schema of an Organisation Report."""

    id: int
    created: datetime
    status: ReportStatus
    description: str
    reported_organisation_id: int
    reporter_user_id: Optional[int] = None
    reporter_organisation_id: Optional[int] = None

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
