# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Schemas for querying report data."""

from datetime import datetime
from typing import Literal

from pydantic import BaseModel


class AnalysisQuery(BaseModel):
    """A schema for querying analysis data."""

    start_time: datetime
    end_time: datetime


class AnalysisEventQuery(AnalysisQuery):
    """A schema for querying event analysis data."""

    topic_ids: list[int] = []
    type_ids: list[int] = []
    event_time: Literal["start", "create"]


class AnalysisEventTimeseriesQuery(AnalysisEventQuery):
    """A schema for querying event analysis timeseries data."""

    interval: Literal["day", "week", "month", "year"]


class AnalysisEventDensitymapQuery(AnalysisEventQuery):
    """A schema for querying event analysis densitymap data."""


class AnalysisTimeseriesData(BaseModel):
    """A schema to describe data over time."""

    times: list[datetime]
    values: list[float]


class AnalysisDensitymapData(BaseModel):
    """A schema to describe data over area."""

    latitudes: list[float]
    longitudes: list[float]
    values: list[float]
