# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for topic related routes."""

from __future__ import annotations

from pydantic import BaseModel, constr


class TopicCreate(BaseModel):
    """API input schema for creating a Topic."""

    name_en: constr(max_length=100)
    name_de: constr(max_length=100)


class TopicUpdate(BaseModel):
    """API input schema for updating a Topic."""

    name_en: constr(max_length=100)
    name_de: constr(max_length=100)


class TopicInDB(BaseModel):
    """API output schema for a Topic."""

    id: int
    name_en: str
    name_de: str

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
