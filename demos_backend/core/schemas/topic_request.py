# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for topic request related routes."""

from __future__ import annotations

from datetime import datetime

from pydantic import BaseModel, constr

from demos_backend.core.types import RequestStatus


class TopicRequestCreate(BaseModel):
    """API input schema for creating a Topic Request."""

    name: constr(max_length=100)
    reporter_organisation_id: int


class TopicRequestUpdate(BaseModel):
    """API input schema for updating a Topic Request."""

    status: RequestStatus


class TopicRequestInDB(BaseModel):
    """API output schema for a Topic Request."""

    id: int
    name: str
    created: datetime
    requester_user_id: int
    requester_organisation_id: int
    status: RequestStatus

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
