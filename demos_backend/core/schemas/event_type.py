# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for event type related routes."""

from __future__ import annotations

from pydantic import BaseModel, constr


class EventTypeCreate(BaseModel):
    """API input schema to create an Event Type."""

    name_en: constr(max_length=250)
    name_de: constr(max_length=250)


class EventTypeUpdate(BaseModel):
    """API input schema to update an Event Type."""

    name_en: constr(max_length=250)
    name_de: constr(max_length=250)


class EventTypeInDB(BaseModel):
    """API output schema of an Event Type."""

    id: int
    name_en: str
    name_de: str

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
