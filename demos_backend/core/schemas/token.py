# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for JWT related routes."""

from typing import Optional

from pydantic import BaseModel


class Token(BaseModel):
    """API output schema for JWTs."""

    access_token: str
    token_type: str


class TokenPayload(BaseModel):
    """Schema for JWT payloads."""

    sub: Optional[int] = None
