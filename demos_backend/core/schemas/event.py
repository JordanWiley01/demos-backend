# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for event related routes."""

from __future__ import annotations

from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, conint, constr

from demos_backend.core.types import ReoccurringEventPeriod

from .event_type import EventTypeInDB
from .location import (
    LocationCreate,
    LocationCreateCrawler,
    LocationInDB,
    LocationUpdate,
)
from .topic import TopicInDB


class EventCreateBase(BaseModel):
    """API input schema to create an Event."""

    name: constr(max_length=100)
    short_description: Optional[constr(max_length=60)] = None
    description: Optional[constr(max_length=1000)] = None
    url: Optional[constr(max_length=200)] = None
    start_time: datetime
    end_time: datetime
    reoccurring: ReoccurringEventPeriod = ReoccurringEventPeriod.NONE
    reoccurring_count: conint(ge=1, le=6) = 1

    type_id: int
    topic_ids: List[int]
    organising_organisation_id: int
    co_organising_organisations_ids: List[int] = []


class EventCreate(EventCreateBase):
    """API input schema to create an Event."""

    location: LocationCreate


class EventCreateCrawler(EventCreateBase):
    """API input schema to create an Event."""

    location: LocationCreateCrawler
    raw_location: constr(max_length=5000)
    raw_estimated_participants: constr(max_length=5000) | None = None
    raw_organiser: constr(max_length=5000) | None = None


class EventUpdate(BaseModel):
    """API input schema to update an Event."""

    name: Optional[constr(max_length=100)] = None
    short_description: Optional[constr(max_length=60)] = None
    description: Optional[constr(max_length=1000)] = None
    url: Optional[constr(max_length=200)] = None
    start_time: Optional[datetime] = None
    end_time: Optional[datetime] = None

    location: Optional[LocationUpdate] = None
    type_id: Optional[int] = None
    topic_ids: List[int] = []
    organising_organisation_id: Optional[int] = None
    co_organising_organisations_ids: List[int] = []


class EventInDB(BaseModel):
    """API return schema of an Event."""

    id: int
    name: str
    short_description: Optional[str] = None
    description: Optional[str] = None
    url: Optional[str] = None
    start_time: datetime
    end_time: datetime
    created: datetime
    location: LocationInDB
    type: EventTypeInDB
    topics: list[TopicInDB]
    organising_organisation_id: int
    raw_location: Optional[constr(max_length=5000)] = None
    raw_estimated_participants: constr(max_length=5000) | None = None
    raw_organiser: constr(max_length=5000) | None = None

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True


class EventSearch(BaseModel):
    """API input schema to search an Event."""

    name: Optional[str] = None
    start_time_start: Optional[datetime] = None
    start_time_end: Optional[datetime] = None
    end_time_start: Optional[datetime] = None
    end_time_end: Optional[datetime] = None
    type_ids: list[int] = []
    topic_ids: list[int] = []
    organiser_ids: list[int] = []
    co_organiser_ids: list[int] = []
    limit: conint(ge=1, le=50) = 50
    offset: conint(ge=0) = 0


class EventPerimeterSearch(EventSearch):
    """API input schema to search an Event by perimeter."""

    latitude: float
    longitude: float
    radius_km: float


class EventAddressSearch(EventSearch):
    """API input schema to search an Event by address."""

    country: constr(max_length=100)
    city: constr(max_length=100)
    zip_code: constr(max_length=20)
