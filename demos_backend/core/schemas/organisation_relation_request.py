# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""API in- and output schemas for organisation relation related routes."""

from datetime import datetime

from pydantic import BaseModel

from demos_backend.core.types import RequestStatus

from .organisation import OrganisationInDB


class OrganisationRelationRequestCreate(BaseModel):
    """API input schema to create an Organisation Relation Request."""

    organisation_requester_id: int
    organisation_requested_id: int


class OrganisationRelationRequestUpdate(BaseModel):
    """API input schema to update an Organisation Relation Request."""

    status: RequestStatus


class OrganisationRelationRequestInDB(BaseModel):
    """API output schema of an Organisation Relation Request."""

    organisation_requester: OrganisationInDB
    organisation_requested: OrganisationInDB
    created: datetime
    status: RequestStatus

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
