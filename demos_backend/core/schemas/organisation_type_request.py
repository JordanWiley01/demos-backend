# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for organisation type request."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from pydantic import BaseModel, constr

from demos_backend.core.types import RequestStatus


class OrganisationTypeRequestCreate(BaseModel):
    """API input schema to create an Organisation Type Request."""

    name: constr(max_length=100)
    requester_organisation_id: Optional[int] = None


# Properties to receive via API on update
class OrganisationTypeRequestUpdate(BaseModel):
    """API input schema to update an Organisation Type Request."""

    status: RequestStatus


class OrganisationTypeRequestInDB(BaseModel):
    """API return schema of an Organisation Type Request."""

    id: int
    name: str
    created: datetime
    requester_user_id: int
    requester_organisation_id: int
    status: RequestStatus

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
