# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for event report related routes."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from pydantic import BaseModel, constr

from demos_backend.core.types import ReportStatus


class EventReportCreate(BaseModel):
    """API input schema to create an Event Report."""

    reported_event_id: int
    description: constr(max_length=250)
    reporter_organisation_id: Optional[int] = None


class EventReportUpdate(BaseModel):
    """API input schema to update an Event Report."""

    status: ReportStatus


class EventReportInDB(BaseModel):
    """API output schema of an Event Report."""

    id: int
    created: datetime
    status: ReportStatus
    description: constr(max_length=250)
    reported_event_id: int
    reporter_user_id: Optional[int] = None
    reporter_organisation_id: Optional[int] = None

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
