# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for organisation related routes."""

from __future__ import annotations

from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, EmailStr, conint, constr

from ..types import RequestStatus
from .location import LocationCreate, LocationInDB, LocationUpdate
from .organisation_type import OrganisationTypeInDB
from .topic import TopicInDB


class OrganisationCreate(BaseModel):
    """API input schema to create an Organisation."""

    name: constr(max_length=100)
    description: constr(max_length=1000)
    url: Optional[constr(max_length=200)] = None
    public_mail_contact: Optional[EmailStr] = None
    internal_point_of_contact: EmailStr
    image_name: constr(min_length=32, max_length=32)
    location: LocationCreate
    type_id: int
    topic_ids: list[int]


class OrganisationUpdateUser(BaseModel):
    """API input schema to update an Organisation as organisation admin."""

    name: Optional[constr(min_length=1, max_length=200)] = None
    description: Optional[constr(max_length=1000)] = None
    url: Optional[constr(min_length=1, max_length=200)] = None
    public_mail_contact: Optional[EmailStr] = None
    internal_point_of_contact: Optional[EmailStr] = None
    image_name: Optional[constr(min_length=32, max_length=32)] = None
    location: Optional[LocationUpdate] = None
    type_id: Optional[int] = None
    topic_ids: List[int] = None


class OrganisationUpdateAdmin(OrganisationUpdateUser):
    """API input schema to update an Organisation as demos admin."""

    approved: Optional[RequestStatus] = None


class OrganisationInDB(BaseModel):
    """API return schema of an Organisation."""

    id: int
    name: str
    description: Optional[str] = None
    url: Optional[str] = None
    public_mail_contact: Optional[EmailStr] = None
    location: LocationInDB
    type: OrganisationTypeInDB
    topics: list[TopicInDB]
    created: datetime
    image_name: Optional[str] = None
    approved: RequestStatus

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True


class OrganisationAddressSearch(BaseModel):
    """API input schema to search an Organisation by address."""

    name: Optional[str]
    country: str
    city: str
    zip_code: str
    type_ids: list[int] = []
    topic_ids: list[int] = []
    limit: conint(ge=1, le=25) = 25
    offset: int = 0
