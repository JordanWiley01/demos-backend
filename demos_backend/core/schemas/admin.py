# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Schemas for querying admin data."""

from pydantic import BaseModel


class AdminStatistics(BaseModel):
    """A schema for returning admin data."""

    dangling_locations: int
    organisation_count: int
    event_count: int
    user_count: int


class CleanupTasks(BaseModel):
    """A schema for scheduling cleanup tasks."""

    dangling_locations: bool = False
    future_events: bool = False
