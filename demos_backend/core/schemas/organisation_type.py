# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains API in- and output schemas for organisation type related routes."""

from __future__ import annotations

from pydantic import BaseModel, constr


class OrganisationTypeCreate(BaseModel):
    """API input schema to create an Organisation Type."""

    name_en: constr(max_length=100)
    name_de: constr(max_length=100)


class OrganisationTypeUpdate(BaseModel):
    """API input schema to update an Organisation Type."""

    name_en: constr(max_length=100)
    name_de: constr(max_length=100)


class OrganisationTypeInDB(BaseModel):
    """API return schema of an Organisation Type."""

    id: int
    name_en: str
    name_de: str

    class Config:
        """Pydantic's BaseModel config."""

        orm_mode = True
        arbitrary_types_allowed = True
