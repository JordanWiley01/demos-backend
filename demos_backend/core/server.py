# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later
"""This module provides a gunicorn server to run the APIs."""
from multiprocessing import cpu_count

from gunicorn.app.base import BaseApplication
from gunicorn.util import import_app

from demos_backend.core.configuration import Configuration
from demos_backend.core.logging import SupportedLogLevels


class FastAPIServer(BaseApplication):
    """A Server class to run a fastapi application with uvicorn workers."""

    def __init__(
        self, app_uri: str, hostname: str, port: int, config: Configuration
    ) -> None:
        """Create an instance of the class.

        Args:
            app_uri: The import path to run the application.
            hostname: The hostname to run the application on.
            port: The port to run the application on
            config: The application configuration object.
        """
        self.app_uri = app_uri
        cores = cpu_count()
        workers_per_core = 1
        web_concurrency = max(workers_per_core * cores, 2)

        self.options = {
            "workers": web_concurrency,
            "worker_class": "uvicorn.workers.UvicornWorker",
            "bind": f"{hostname}:{port}",
        }

        if config.LOG_LEVEL == SupportedLogLevels.DEBUG:
            self.options["loglevel"] = "debug"
        elif config.LOG_LEVEL == SupportedLogLevels.INFO:
            self.options["loglevel"] = "info"
        elif config.LOG_LEVEL == SupportedLogLevels.WARNING:
            self.options["loglevel"] = "warning"
        elif config.LOG_LEVEL == SupportedLogLevels.ERROR:
            self.options["loglevel"] = "error"
        elif config.LOG_LEVEL == SupportedLogLevels.CRITICAL:
            self.options["loglevel"] = "critical"

        super().__init__()

    def load_config(self):
        """Load the config."""
        config = {
            key: value
            for key, value in self.options.items()
            if key in self.cfg.settings and value is not None
        }
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        """Load the application."""
        return import_app(self.app_uri)
