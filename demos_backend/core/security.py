# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module hold all security related code."""
from __future__ import annotations

from datetime import datetime, timedelta
from typing import Any

from jose import jwt
from passlib.context import CryptContext

from demos_backend.core.configuration import get_config

ALGORITHM: str = "HS256"
pwd_context: CryptContext = CryptContext(schemes=["bcrypt"], deprecated="auto")
config = get_config()


def create_access_token(subject: str | Any, expires_delta: timedelta) -> str:
    """Create an access token for a given subject.

    Args:
        subject: The subject of the token.
        expires_delta: How many minutes the token should be valid.

    Returns:
        str: An encoded JWT.
    """
    expire: datetime = datetime.utcnow() + expires_delta

    to_encode: dict = {"exp": expire, "sub": str(subject)}
    encoded_jwt: str = jwt.encode(to_encode, config.API_SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_password_hash(plain_password: str) -> str:
    """Get a password hash.

    Args:
        plain_password: The plaintext password to hash.

    Returns:
        str: The hashed password.
    """
    return pwd_context.hash(plain_password)


def verify_password(plain_password: str, hashed_password: str) -> bool:
    """Verify, if a password matches the password hash.

    Args:
        plain_password: The plaintext password to verify.
        hashed_password: A hashed password to check against.

    Returns:
        bool: If the password matches the password hash.
    """
    return pwd_context.verify(plain_password, hashed_password)
