# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Database connection functions."""
from typing import Union

from sqlalchemy.future import Engine
from sqlmodel import Session, create_engine

from demos_backend.core.configuration import Environment, get_config

_SQLALCHEMY_DATABASE_URI = None
_ENGINE = None


def get_database_uri() -> str:
    """Generate the uri to connect to the database."""
    config = get_config()
    global _SQLALCHEMY_DATABASE_URI  # pylint: disable=W0603
    if not _SQLALCHEMY_DATABASE_URI:
        _SQLALCHEMY_DATABASE_URI = (
            f"mysql+pymysql://{config.MARIADB_USERNAME}:"
            f"{config.MARIADB_PASSWORD}@{config.MARIADB_HOSTNAME}/"
            f"{config.MARIADB_DATABASE}"
        )

    return _SQLALCHEMY_DATABASE_URI


def get_engine() -> Union[Engine, Engine]:
    """Generate a database engine."""
    global _ENGINE  # pylint: disable=W0603
    if not _ENGINE:
        config = get_config()
        if config.ENVIRONMENT == Environment.DEVELOPMENT:
            _ENGINE = create_engine(
                get_database_uri(), echo=False, pool_size=20, max_overflow=40
            )
        else:
            _ENGINE = create_engine(
                get_database_uri(), echo=False, pool_size=20, max_overflow=40
            )
    return _ENGINE


def get_session() -> Session:
    """Get a database session."""
    return Session(get_engine())
