# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Package for MariaDB handling."""

from typing import Any

from sqlalchemy.ext.declarative import as_declarative, declared_attr


@as_declarative()
class TableBaseClass:
    """Base class for SQL tables."""

    id: Any
    __name__: str

    @declared_attr
    # pylint: disable=E0213
    def __tablename__(cls) -> str:
        """Autogenerate SQL table name from class name."""
        return cls.__name__.lower()
