# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Generate initial data."""
from __future__ import annotations

import logging
from datetime import datetime, timedelta

from sqlmodel import Session

from demos_backend.core.configuration import get_config
from demos_backend.core.types import AvailableLanguages, RequestStatus
from demos_backend.core.util import (
    get_geolocation_from_location_create,
    translate_information_create,
    translate_location_create,
)

from ...schemas.event import EventCreate
from ...schemas.event_report import EventReportCreate
from ...schemas.event_type import EventTypeCreate
from ...schemas.event_type_request import EventTypeRequestCreate
from ...schemas.information import InformationCreate
from ...schemas.location import LocationCreate
from ...schemas.organisation import OrganisationCreate
from ...schemas.organisation_report import OrganisationReportCreate
from ...schemas.organisation_type import OrganisationTypeCreate
from ...schemas.organisation_type_request import OrganisationTypeRequestCreate
from ...schemas.topic import TopicCreate
from ...schemas.topic_request import TopicRequestCreate
from ...schemas.user import UserCreate
from . import crud
from .initial_data import (
    initial_event_types,
    initial_info,
    initial_organisation_types,
    initial_topics,
)
from .session import get_session

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28

config = get_config()


async def init_mariadb_data(
    test_mode: bool = False, initial_data: bool = False
) -> None:
    """Fill MariaDB with initial data."""
    with get_session() as session:
        create_admin_user(session=session)
        create_crawler_user(session=session)

        if initial_data:
            create_event_types(session=session)
            create_organisation_types(session=session)
            create_information(session=session)
            create_topics(session=session)
        if test_mode:
            with get_session() as session:
                # the order matters here because they depend on each other
                location_schemas = create_test_location(session=session)
                create_test_information(session=session)

                create_test_topic(session=session)
                create_test_event_type(session=session)
                create_test_organisation_type(session=session)

                create_test_users(
                    session=session, location_create_schemas=location_schemas
                )
                create_test_organisation(
                    session=session, location_create_schemas=location_schemas
                )

                create_test_organisation_type_request(session=session)
                create_test_organisation_report(session=session)
                create_test_organisation_relation_request(
                    session=session, location_create_schemas=location_schemas
                )

                create_test_topic_request(session=session)

                create_test_event_type_request(session=session)
                create_test_event(
                    session=session, location_create_schemas=location_schemas
                )
                create_test_event_report(session=session)


def create_test_location(session: Session) -> list[LocationCreate]:
    """Create dummy data for testing."""
    locations = [
        ("Puschkinallee", "12435", "46"),
        ("Pariser Platz", "10117", "1"),
        ("Prenzlauer Allee", "10405", "80"),
        ("Bismarckstraße", "10627", "60"),
        ("Alt-Kaulsdorf", "12621", "20"),
    ]
    location_create_schemas = []
    for location_number in range(5):
        for location in locations:
            new_location = LocationCreate(
                country="Deutschland",
                city="Berlin",
                street=location[0],
                zip_code=location[1],
                house_number=str(int(location[2]) + location_number * 2),
            )
            location_create_schemas.append(new_location)

            schema_in = get_geolocation_from_location_create(
                location_create=new_location
            )
            schema_in = translate_location_create(
                schema_in=schema_in, src_language=AvailableLanguages.GERMAN
            )
            new_location = crud.location.create(session=session, schema_in=schema_in)
            if location:
                logging.debug("Created test location %s", new_location.id)
            else:
                raise RuntimeError("Location was not created.")
    return location_create_schemas


def create_test_organisation_report(session: Session) -> None:
    """Create dummy data for testing."""
    report = OrganisationReportCreate(
        reported_organisation_id=1,
        description="Reported",
        reporter_organisation_id=1,
    )
    location = crud.organisation_report.create(
        session=session, schema_in=report, reporter_user_id=1
    )
    if location:
        logging.info("Created test org report.")
    else:
        raise RuntimeError("Test org report was not created.")


def create_test_event_report(session: Session) -> None:
    """Create dummy data for testing."""
    report = EventReportCreate(
        reported_event_id=1,
        description="Reported",
        reporter_organisation_id=1,
    )
    location = crud.event_report.create(
        session=session, schema_in=report, reporter_user_id=1
    )
    if location:
        logging.info("Created test event report.")
    else:
        raise RuntimeError("Test event report was not created.")


def create_test_information(session: Session) -> None:
    """Create dummy data for testing."""
    information = InformationCreate(
        name_native="Klima Demo",
        name_english="Climate Demo",
        url_native="url_native",
        url_english="url_english",
        country="Deutschland",
        city="Hamburg",
        zip_code="12345",
    )
    information = translate_information_create(
        schema_in=information, src_language=AvailableLanguages.GERMAN
    )

    information = crud.information.create(session=session, schema_in=information)
    if information:
        logging.info("Created test information %s.", information.name_native)
    else:
        raise RuntimeError("Information was not created.")


def create_information(session: Session) -> None:
    """Create initial information data from legacy."""
    for info in initial_info:
        if crud.information.get_by_native_name(session=session, name=info[0]):
            logging.info("Info %s already exists: skipping", info[0])
            continue
        information = InformationCreate(
            name_native=info[0],
            name_english=info[1],
            url_native=info[2],
            url_english=info[3],
            country=info[5],
            city=info[4],
            zip_code="",
        )
        information = translate_information_create(
            schema_in=information, src_language=AvailableLanguages.GERMAN
        )

        information = crud.information.create(session=session, schema_in=information)
        if info:
            logging.info("Created information %s.", information.name_native)
        else:
            raise RuntimeError("Information was not created.")


def create_test_topic(session: Session) -> None:
    """Create dummy data for testing."""
    topic = TopicCreate(name_de="Klima", name_en="Climate")
    topic = crud.topic.create(session=session, schema_in=topic)
    if topic:
        logging.info("Created test topic %s.", topic.name_en)
    else:
        raise RuntimeError("Topic was not created.")
    topic = TopicCreate(name_de="Korruption", name_en="Corruption")
    topic = crud.topic.create(session=session, schema_in=topic)
    if topic:
        logging.info("Created test topic %s.", topic.name_en)
    else:
        raise RuntimeError("Topic was not created.")
    topic = TopicCreate(name_de="Frieden", name_en="Peace")
    topic = crud.topic.create(session=session, schema_in=topic)
    if topic:
        logging.info("Created test topic %s.", topic.name_en)
    else:
        raise RuntimeError("Topic was not created.")


def create_topics(session: Session) -> None:
    """Create initial data from legacy."""
    for topic in initial_topics:
        if crud.topic.get_by_name(session=session, topic_name=topic[0]):
            logging.info("Event type %s already exists: skipping", topic[0])
            continue
        topic = TopicCreate(name_en=topic[0], name_de=topic[1])
        topic = crud.topic.create(session=session, schema_in=topic)
        if topic:
            logging.info("Created topic %s.", topic.name_en)
        else:
            raise RuntimeError("Topic was not created.")


def create_test_event_type(session: Session) -> None:
    """Create dummy data for testing."""
    new_type = EventTypeCreate(name_de="Demo", name_en="Demo")
    event_type = crud.event_type.create(session=session, schema_in=new_type)
    if event_type:
        logging.info("Created test event type %s", event_type.name_en)
    else:
        raise RuntimeError("Event type was not created.")
    new_type = EventTypeCreate(name_de="Infostand", name_en="Infobooth")
    event_type = crud.event_type.create(session=session, schema_in=new_type)
    if event_type:
        logging.info("Created test event type %s", event_type.name_en)
    else:
        raise RuntimeError("Event type was not created.")
    new_type = EventTypeCreate(name_de="Sitzblockade", name_en="Sit-In")
    event_type = crud.event_type.create(session=session, schema_in=new_type)
    if event_type:
        logging.info("Created test event type %s", event_type.name_en)
    else:
        raise RuntimeError("Event type was not created.")


def create_event_types(session: Session) -> None:
    """Create data from legacy."""
    for event_type in initial_event_types:
        if crud.event_type.get_by_name(session=session, event_type_name=event_type[0]):
            logging.info("Event type %s already exists: skipping", event_type[0])
            continue
        new_type = EventTypeCreate(name_en=event_type[0], name_de=event_type[1])
        event_type = crud.event_type.create(session=session, schema_in=new_type)
        if event_type:
            logging.info("Created event type %s", event_type.name_en)
        else:
            raise RuntimeError("Event type was not created.")


def create_test_organisation(
    session: Session, location_create_schemas: list[LocationCreate]
) -> None:
    """Create dummy data for testing."""
    creator = crud.user.get_by_id(session=session, user_id=1)

    organisation1 = OrganisationCreate(
        name="AdminBabos",
        description="Crazyest bois in da hood",
        url="best_url",
        public_mail_contact="admin@demonstrations.org",
        internal_point_of_contact="admin@demonstrations.org",
        type_id=1,
        image_name="0" * 32,
        location=location_create_schemas[0],
        topic_ids=[1],
    )
    topics = crud.topic.get_by_ids(session=session, topic_ids=organisation1.topic_ids)
    location = crud.location.get_by_id(session=session, location_id=1)
    org1 = crud.organisation.create(
        session=session,
        schema_in=organisation1,
        creator=creator,
        topics=topics,
        approved=RequestStatus.APPROVED,
        location=location,
    )

    if org1:
        logging.info("Created test org %s.", org1.name)
    else:
        raise RuntimeError("Org was not created.")

    organisation2 = OrganisationCreate(
        name="AdminBabos2",
        description="Crazyest bois in da hood",
        url="best_url",
        public_mail_contact="admin@demonstrations.org",
        internal_point_of_contact="admin@demonstrations.org",
        type_id=2,
        image_name="0" * 32,
        location=location_create_schemas[1],
        topic_ids=[2],
    )
    topics = crud.topic.get_by_ids(session=session, topic_ids=organisation2.topic_ids)
    location = crud.location.get_by_id(session=session, location_id=2)
    org2 = crud.organisation.create(
        session=session,
        schema_in=organisation2,
        creator=creator,
        topics=topics,
        approved=RequestStatus.APPROVED,
        location=location,
    )

    if org2:
        logging.info("Created test org %s.", org2.name)
    else:
        raise RuntimeError("Org was not created.")

    crud.organisation.add_relation(session=session, model_in=org1, org_to_add=org2)
    crud.organisation.add_relation(session=session, model_in=org2, org_to_add=org1)


def create_test_event(
    session: Session, location_create_schemas: list[LocationCreate]
) -> None:
    """Create dummy data for testing."""
    creator = crud.user.get_by_id(session=session, user_id=1)

    now = datetime.now()
    time_oscillator = 1
    for event_number in range(1, 101):
        time_oscillator *= -1
        location_id = event_number % 25 + 1
        type_id = event_number % 3 + 1
        topic_id = event_number % 3 + 1
        organiser_id = event_number % 2 + 1
        co_organiser_id = (event_number + 1) % 2 + 1

        organising_organisation = crud.organisation.get_by_id(
            session=session, organisation_id=organiser_id
        )
        if not organising_organisation:
            raise ValueError("org not found")

        event = EventCreate(
            name=f"DemoDemo{event_number}",
            description="Crazyest bois in da hood",
            short_description="123456789 " * (1 + (event_number % 6)),
            url="best_url",
            type_id=type_id,
            start_time=now
            + timedelta(
                hours=(20 - 10 * time_oscillator) * event_number * time_oscillator
            ),
            end_time=now
            + timedelta(
                hours=(20 + 10 * time_oscillator) * event_number * time_oscillator
            ),
            location=location_create_schemas[location_id - 1],
            organising_organisation_id=organiser_id,
            topic_ids=[topic_id],
            co_organising_organisations_ids=[co_organiser_id],
        )

        topics = crud.topic.get_by_ids(session=session, topic_ids=event.topic_ids)
        co_organisers = crud.organisation.get_by_ids(
            session=session, organisation_ids=event.co_organising_organisations_ids
        )
        location = crud.location.get_by_id(session=session, location_id=location_id)

        event = crud.event.create(
            session=session,
            schema_in=event,
            creator=creator,
            organising_organisation=organising_organisation,
            topics=topics,
            co_organizers=co_organisers,
            location=location,
        )

        if event:
            logging.info("Created test event.")
        else:
            raise RuntimeError("Event was not created.")


def create_test_organisation_relation_request(
    session: Session, location_create_schemas: list[LocationCreate]
) -> None:
    """Create dummy data for testing."""
    creator = crud.user.get_by_id(session=session, user_id=1)

    organisation1 = OrganisationCreate(
        name="RelationRequest1",
        description="Crazyest bois in da hood",
        url="best_url",
        public_mail_contact="admin@demonstrations.org",
        internal_point_of_contact="admin@demonstrations.org",
        type_id=1,
        location=location_create_schemas[0],
        topic_ids=[1],
        image_name="0" * 32,
    )
    organisation2 = OrganisationCreate(
        name="RelationRequest2",
        description="Crazyest bois in da hood",
        url="best_url",
        public_mail_contact="admin@demonstrations.org",
        internal_point_of_contact="admin@demonstrations.org",
        type_id=1,
        location=location_create_schemas[1],
        topic_ids=[1],
        image_name="0" * 32,
    )
    topics = crud.topic.get_by_ids(session=session, topic_ids=organisation1.topic_ids)
    location1 = crud.location.get_by_id(session=session, location_id=1)
    location2 = crud.location.get_by_id(session=session, location_id=2)
    org1 = crud.organisation.create(
        session=session,
        schema_in=organisation1,
        creator=creator,
        topics=topics,
        approved=RequestStatus.APPROVED,
        location=location1,
    )
    org2 = crud.organisation.create(
        session=session,
        schema_in=organisation2,
        creator=creator,
        topics=topics,
        approved=RequestStatus.APPROVED,
        location=location2,
    )

    if not org1 or not org2:
        raise RuntimeError("Cannot create test org relation requests.")
    request = crud.organisation_relation_request.create(
        session=session, requester_organisation=org1, requested_organisation=org2
    )

    if request:
        logging.info("Created test org relation request.")
    else:
        raise RuntimeError("Test org relation request was not created.")


def create_test_topic_request(session: Session) -> None:
    """Create dummy data for testing."""
    topic = TopicRequestCreate(name="Frauenrechte", reporter_organisation_id=1)
    topic = crud.topic_request.create(
        session=session, schema_in=topic, requester_user_id=1
    )
    if topic:
        logging.info("Created test topic request.")
    else:
        raise RuntimeError("Topic request was not created.")


def create_test_organisation_type(session: Session) -> None:
    """Create dummy data for testing."""
    org_type = OrganisationTypeCreate(name_de="Frauenrechte", name_en="Womenrights")
    org_type = crud.organisation_type.create(session=session, schema_in=org_type)
    if org_type:
        logging.info("Created test organisation type %s.", org_type.name_en)
    else:
        raise RuntimeError("Organisation type was not created.")
    org_type = OrganisationTypeCreate(name_de="kapitalismus", name_en="Capitalism")
    org_type = crud.organisation_type.create(session=session, schema_in=org_type)
    if org_type:
        logging.info("Created test organisation type %s.", org_type.name_en)
    else:
        raise RuntimeError("Organisation type was not created.")


def create_organisation_types(session: Session) -> None:
    """Create data from legacy."""
    for organisation_type in initial_organisation_types:
        if crud.organisation_type.get_by_name(
            session=session, organisation_type_name=organisation_type[0]
        ):
            logging.info("Org type %s already exists: skipping", organisation_type[0])
            continue
        org_type = OrganisationTypeCreate(
            name_en=organisation_type[0], name_de=organisation_type[1]
        )
        org_type = crud.organisation_type.create(session=session, schema_in=org_type)
        if org_type:
            logging.info("Created organisation type %s.", org_type.name_en)
        else:
            raise RuntimeError("Organisation type was not created.")


def create_test_organisation_type_request(session: Session) -> None:
    """Create dummy data for testing."""
    request = OrganisationTypeRequestCreate(
        name="Frauenrechte", requester_organisation_id=1
    )
    request = crud.organisation_type_request.create(
        session=session, schema_in=request, requester_user_id=1
    )
    if request:
        logging.info("Created test organisation type request.")
    else:
        raise RuntimeError("Organisation type request was not created.")


def create_test_event_type_request(session: Session) -> None:
    """Create dummy data for testing."""
    request = EventTypeRequestCreate(name="Sit-In", requester_organisation_id=1)
    request = crud.event_type_request.create(
        session=session, schema_in=request, requester_user_id=1
    )
    if request:
        logging.info("Created test event type request.")
    else:
        raise RuntimeError("Event type request was not created.")


def create_test_users(
    session: Session, location_create_schemas: list[LocationCreate]
) -> None:
    """Create dummy data for testing."""
    delete_user = UserCreate(
        email="delete_user@demonstrations.org",
        username="delete_user",
        password="delete_userdelete_userdelete_user",
    )
    delete_user = crud.user.create(
        session=session, schema_in=delete_user, verified=True
    )
    if delete_user:
        logging.info("Created test delete_user user.")
    else:
        raise RuntimeError("Test delete_user was not created.")
    solo_user = UserCreate(
        email="solo_user@demonstrations.org",
        username="solo_user",
        password="solo_usersolo_usersolo_user",
    )
    solo_user = crud.user.create(session=session, schema_in=solo_user, verified=True)
    if solo_user:
        logging.info("Created test solo_user user.")
    else:
        raise RuntimeError("Test solo_user was not created.")
    org_user = UserCreate(
        email="org_user@demonstrations.org",
        username="org_user",
        password="org_userorg_userorg_user",
    )
    org_user = crud.user.create(session=session, schema_in=org_user, verified=True)
    if org_user:
        logging.info("Created test org_user user.")
    else:
        raise RuntimeError("Test org_user was not created.")
    org_user_org = OrganisationCreate(
        name="Org User Org",
        description="testing org for org_user",
        url="best_url",
        public_mail_contact="org_user@demonstrations.org",
        internal_point_of_contact="org_user@demonstrations.org",
        type_id=1,
        location=location_create_schemas[0],
        topic_ids=[1],
        image_name="0" * 32,
    )
    topics = crud.topic.get_by_ids(session=session, topic_ids=org_user_org.topic_ids)
    location = crud.location.get_by_id(session=session, location_id=1)
    org_user_org = crud.organisation.create(
        session=session,
        schema_in=org_user_org,
        creator=org_user,
        topics=topics,
        approved=RequestStatus.APPROVED,
        location=location,
    )
    if org_user_org:
        logging.info("Created test org for org_user.")
    else:
        raise RuntimeError("Org for org_user was not created.")

    admin = crud.user.get_by_id(session=session, user_id=1)
    crud.organisation.add_admin(session=session, model_in=org_user_org, new_admin=admin)


def create_admin_user(session: Session) -> None:
    """Create application admin."""
    users = crud.user.get_by_emails(session=session, emails=[config.DEFAULT_USER_MAIL])
    if not users:
        user_in = UserCreate(
            email=config.DEFAULT_USER_MAIL,
            password=config.DEFAULT_USER_PASSWORD,
            username=config.DEFAULT_USER_NAME,
        )
        user = crud.user.create(
            session=session, schema_in=user_in, admin=True, verified=True
        )
        if user:
            logging.info("Created default user.")
        else:
            logging.error("Could not create default user.")


def create_crawler_user(session: Session) -> None:
    """Create account used for crawlers."""
    users = crud.user.get_by_emails(session=session, emails=[config.CRAWLER_USER_MAIL])
    if not users:
        user_in = UserCreate(
            email=config.CRAWLER_USER_MAIL,
            password=config.CRAWLER_USER_PASSWORD,
            username=config.CRAWLER_USER_NAME,
        )
        user = crud.user.create(session=session, schema_in=user_in, verified=True)
        if user:
            logging.info("Created crawler user.")
        else:
            logging.error("Could not create crawler user.")
