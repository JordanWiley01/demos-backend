# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Information related database operations."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from sqlmodel import Session, col, select

from demos_backend.core.models.information import Information
from demos_backend.core.schemas.information import (
    InformationCreate,
    InformationQuery,
    InformationUpdate,
)


def get_all(session: Session) -> list[Information]:
    """Get all information."""
    statement = select(Information)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, link_id: int) -> Optional[Information]:
    """Get an information by ID."""
    return session.get(Information, link_id)


def get_by_native_name(session: Session, name: str) -> list[Information]:
    """Get an information by native name."""
    statement = select(Information).where(Information.name_native == name)
    results = session.exec(statement)
    return results.all()


def get_by_query(
    session: Session,
    query: InformationQuery,
) -> list[Information]:
    """Find information."""
    statement = select(Information).limit(query.limit).offset(query.offset)

    statement = statement.where(Information.country == query.country)
    statement = statement.where(Information.city == query.city)
    if query.zip_code:
        statement = statement.where(
            col(Information.zip_code).startswith(query.zip_code)
        )
    statement = statement.order_by(Information.name_native)
    results = session.exec(statement)
    return results.all()


def create(session: Session, schema_in: InformationCreate) -> Information:
    """Create an information."""
    new_information = Information(
        name_native=schema_in.name_native,
        name_english=schema_in.name_english,
        url_native=schema_in.url_native,
        url_english=schema_in.url_english,
        country=schema_in.country,
        city=schema_in.city,
        zip_code=schema_in.zip_code,
        created=datetime.now(),
    )
    session.add(new_information)
    session.commit()
    session.refresh(new_information)
    return new_information


def update(
    session: Session,
    schema_in: InformationUpdate,
    model_in: Information,
) -> Information:
    """Update an information."""
    model_updated = model_in

    # can be set by self
    for name in schema_in.__fields_set__:
        attr = getattr(schema_in, name)
        if attr is not None:
            setattr(model_updated, name, attr)

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)
    session.refresh(model_updated)

    return model_updated


def delete_by_model(
    session: Session, model_to_delete: Information
) -> Optional[Information]:
    """Delete an information."""
    session.delete(model_to_delete)
    session.commit()
    return model_to_delete
