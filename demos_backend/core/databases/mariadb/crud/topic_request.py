# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Topic request related database operations."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from sqlmodel import Session, select

from demos_backend.core.models.topic_request import TopicRequest
from demos_backend.core.schemas.topic_request import (
    TopicRequestCreate,
    TopicRequestUpdate,
)
from demos_backend.core.types import RequestStatus


def get_all(session: Session) -> list[TopicRequest]:
    """Get all topic requests."""
    statement = select(TopicRequest)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, request_id: int) -> Optional[TopicRequest]:
    """Get topic request by ID."""
    return session.get(TopicRequest, request_id)


def get_by_status(
    session: Session, request_status: RequestStatus
) -> list[TopicRequest]:
    """Get topic request by status."""
    statement = select(TopicRequest).where(TopicRequest.status == request_status)
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: TopicRequestCreate,
    requester_user_id: int | None,
) -> TopicRequest:
    """Create topic request."""
    new_topic_request = TopicRequest(
        name=schema_in.name,
        created=datetime.now(),
        status=RequestStatus.PENDING,
        requester_user_id=requester_user_id,
        requester_organisation_id=schema_in.reporter_organisation_id,
    )
    session.add(new_topic_request)
    session.commit()
    return new_topic_request


def update(
    session: Session,
    schema_in: TopicRequestUpdate,
    model_in: TopicRequest,
) -> TopicRequest:
    """Update topic request."""
    model_updated = model_in

    # can be set by self
    model_updated.status = schema_in.status

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated
