# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Topic related database operations."""

from __future__ import annotations

from typing import Optional

from sqlmodel import Session, col, select

from demos_backend.core.models.topic import Topic
from demos_backend.core.schemas.topic import TopicCreate, TopicUpdate


def get_all(session: Session) -> list[Topic]:
    """Get all topics."""
    statement = select(Topic)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, topic_id: int) -> Optional[Topic]:
    """Get topic by ID."""
    return session.get(Topic, topic_id)


def delete_by_id(session: Session, topic: Topic) -> Topic:
    """Delete topic by ID."""
    session.delete(topic)
    session.commit()
    return topic


def get_by_name(session: Session, topic_name: str) -> list[Topic]:
    """Get topic by name."""
    statement = select(Topic).where(Topic.name_en == topic_name)
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: TopicCreate,
) -> Topic:
    """Create topic."""
    new_topic = Topic(name_en=schema_in.name_en, name_de=schema_in.name_de)
    session.add(new_topic)
    session.commit()
    session.refresh(new_topic)
    return new_topic


def update(
    session: Session,
    schema_in: TopicUpdate,
    model_in: Topic,
) -> Topic:
    """Update topic."""
    model_updated = model_in

    # can be set by self
    model_updated.name_en = schema_in.name_en
    model_updated.name_de = schema_in.name_de

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated


def get_by_ids(session: Session, topic_ids: list[int]) -> list[Topic]:
    """Get topics by IDs."""
    statement = select(Topic).where(col(Topic.id).in_(topic_ids))
    results = session.exec(statement)
    return results.all()
