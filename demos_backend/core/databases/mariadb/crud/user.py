# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""User related database operations."""
from __future__ import annotations

from datetime import datetime
from typing import Optional, Union

from pydantic import EmailStr
from sqlmodel import Session, col, select

from demos_backend.core import security
from demos_backend.core.models.user import User
from demos_backend.core.schemas.user import UserCreate, UserSuperUserUpdate, UserUpdate


def authenticate(session: Session, email: EmailStr, password: str) -> Optional[User]:
    """Authenticate a user."""
    users = get_by_emails(session=session, emails=[email])
    if not users:
        return None
    user = users[0]
    if not security.verify_password(password, user.hashed_password):
        return None
    return user


def get_all(session: Session) -> list[User]:
    """Get all users."""
    statement = select(User)
    results = session.exec(statement)
    return results.all()


def count(session: Session) -> int:
    """Get number of users."""
    return session.query(User).count()


def get_admins(session: Session) -> list[User]:
    """Get all admins."""
    statement = select(User).where(User.admin)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, user_id: int) -> Optional[User]:
    """Get user by ID."""
    return session.get(User, user_id)


def get_by_username(session: Session, username: str) -> Optional[User]:
    """Get user by username."""
    statement = select(User).where(User.username == username)
    results = session.exec(statement)
    return results.first()


def get_by_email(session: Session, email: EmailStr) -> Optional[User]:
    """Get user by email."""
    statement = select(User).where(User.email == email)
    results = session.exec(statement)
    return results.first()


def get_by_emails(session: Session, emails: list[EmailStr]) -> list[User]:
    """Get users by emails."""
    statement = select(User).where(col(User.email).in_(emails))
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: UserCreate,
    admin: bool = False,
    verified: bool = False,
) -> User:
    """Create a user."""
    new_user = User(
        email=schema_in.email,
        username=schema_in.username,
        hashed_password=security.get_password_hash(schema_in.password),
        admin=admin,
        verified=verified,
        created=datetime.now(),
    )
    session.add(new_user)
    session.commit()
    return new_user


def update(
    session: Session,
    schema_in: Union[UserSuperUserUpdate, UserUpdate],
    model_in: User,
) -> User:
    """Update a user."""
    model_updated = model_in

    # can be set by self
    if schema_in.username is not None:
        model_updated.username = schema_in.username
    if schema_in.email is not None:
        model_updated.email = schema_in.email
    if schema_in.receive_newsletter is not None:
        model_updated.receive_newsletter = schema_in.receive_newsletter
    if schema_in.password is not None:
        model_updated.hashed_password = security.get_password_hash(schema_in.password)

    # can only be set by superuser
    if isinstance(schema_in, UserSuperUserUpdate):
        if schema_in.active is not None:
            model_updated.active = schema_in.active
        if schema_in.admin is not None:
            model_updated.admin = schema_in.admin
        if schema_in.verified is not None:
            model_updated.verified = schema_in.verified

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated


def delete_by_model(session: Session, model_to_delete: User) -> Optional[User]:
    """Delete a user."""
    session.delete(model_to_delete)
    session.commit()
    return model_to_delete
