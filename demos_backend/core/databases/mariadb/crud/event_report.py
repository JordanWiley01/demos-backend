# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Event report related database operations."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from sqlmodel import Session, select

from demos_backend.core.models.event_report import EventReport
from demos_backend.core.schemas.event_report import EventReportCreate, EventReportUpdate
from demos_backend.core.types import ReportStatus


def get_all(session: Session) -> list[EventReport]:
    """Get all event reports."""
    statement = select(EventReport)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, report_id: int) -> Optional[EventReport]:
    """Get an event report by ID."""
    return session.get(EventReport, report_id)


def get_by_status(
    session: Session, event_report_status: ReportStatus
) -> list[EventReport]:
    """Get event reports by status."""
    statement = select(EventReport).where(EventReport.status == event_report_status)
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: EventReportCreate,
    reporter_user_id: int | None,
) -> EventReport:
    """Create an event report."""
    new_event_report = EventReport(
        reported_event_id=schema_in.reported_event_id,
        description=schema_in.description,
        created=datetime.now(),
        status=ReportStatus.PENDING,
        reporter_user_id=reporter_user_id,
        reporter_organisation_id=schema_in.reporter_organisation_id,
    )
    session.add(new_event_report)
    session.commit()
    return new_event_report


def update(
    session: Session,
    schema_in: EventReportUpdate,
    model_in: EventReport,
) -> EventReport:
    """Update an event report."""
    model_updated = model_in

    # can be set by self
    model_updated.status = schema_in.status

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated
