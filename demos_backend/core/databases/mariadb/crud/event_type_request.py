# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Event type request database operations."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from sqlmodel import Session, select

from demos_backend.core.models.event_type_request import EventTypeRequest
from demos_backend.core.schemas.event_type_request import (
    EventTypeRequestCreate,
    EventTypeRequestUpdate,
)
from demos_backend.core.types import RequestStatus


def get_all(session: Session) -> list[EventTypeRequest]:
    """Get all event type requests."""
    statement = select(EventTypeRequest)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, request_id: int) -> Optional[EventTypeRequest]:
    """Get an event type request by ID."""
    return session.get(EventTypeRequest, request_id)


def get_by_status(
    session: Session, request_status: RequestStatus
) -> list[EventTypeRequest]:
    """Get event type requests by status."""
    statement = select(EventTypeRequest).where(
        EventTypeRequest.status == request_status
    )
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: EventTypeRequestCreate,
    requester_user_id: int | None,
) -> EventTypeRequest:
    """Create an event type request."""
    new_event_type_request = EventTypeRequest(
        name=schema_in.name,
        created=datetime.now(),
        status=RequestStatus.PENDING,
        requester_user_id=requester_user_id,
        requester_organisation_id=schema_in.requester_organisation_id,
    )
    session.add(new_event_type_request)
    session.commit()
    session.refresh(new_event_type_request)
    return new_event_type_request


def update(
    session: Session,
    schema_in: EventTypeRequestUpdate,
    model_in: EventTypeRequest,
) -> EventTypeRequest:
    """Update an event type request."""
    model_updated = model_in

    # can be set by self
    model_updated.status = schema_in.status

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated
