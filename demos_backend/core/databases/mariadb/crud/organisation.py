# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Organisation related database operations."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from sqlalchemy.exc import NoResultFound
from sqlmodel import Session, col, select

from demos_backend.core.models.link_tables import TopicOrganisationLink
from demos_backend.core.models.location import Location
from demos_backend.core.models.organisation import Organisation
from demos_backend.core.models.topic import Topic
from demos_backend.core.models.user import User
from demos_backend.core.schemas.organisation import (
    OrganisationCreate,
    OrganisationUpdateAdmin,
)
from demos_backend.core.types import RequestStatus


def get_all(
    session: Session, approved: RequestStatus = RequestStatus.APPROVED
) -> list[Organisation]:
    """Get all organisations."""
    statement = select(Organisation).where(Organisation.approved == approved)
    results = session.exec(statement)
    return results.all()


def count(session: Session) -> int:
    """Get number of organisations."""
    return session.query(Organisation).count()


def get_by_id(
    session: Session,
    organisation_id: int,
    approved: RequestStatus | None = RequestStatus.APPROVED,
) -> Optional[Organisation]:
    """Get an organisation by ID."""
    statement = select(Organisation)
    statement = statement.where(Organisation.id == organisation_id)
    if approved is not None:
        statement = statement.where(Organisation.approved == approved)
    try:
        return session.exec(statement).one()
    except NoResultFound:
        return None


def get_by_ids(
    session: Session,
    organisation_ids: list[int],
    approved: RequestStatus = RequestStatus.APPROVED,
) -> list[Organisation]:
    """Get organisations by IDs."""
    statement = (
        select(Organisation)
        .where(col(Organisation.id).in_(organisation_ids))
        .where(Organisation.approved == approved)
    )
    results = session.exec(statement)
    return results.all()


def get_by_name(
    session: Session,
    organisation_name: str,
    approved: RequestStatus = RequestStatus.APPROVED,
) -> Organisation:
    """Get an organisation by name."""
    statement = select(Organisation).where(Organisation.name == organisation_name)
    results = session.exec(statement)
    return results.first()


def create(
    session: Session,
    schema_in: OrganisationCreate,
    creator: User,
    topics: list[Topic],
    location: Location,
    approved: RequestStatus = RequestStatus.PENDING,
) -> Organisation:
    """Create an organisation."""
    new_organisation = Organisation(
        name=schema_in.name,
        description=schema_in.description,
        created=datetime.now(),
        url=schema_in.url,
        public_mail_contact=schema_in.public_mail_contact,
        internal_point_of_contact=schema_in.internal_point_of_contact,
        location=location,
        type_id=schema_in.type_id,
        creator_id=creator.id,
        image_name=schema_in.image_name,
        approved=approved,
    )
    new_organisation.admins.append(creator)
    new_organisation.topics = topics
    session.add(new_organisation)
    session.commit()
    session.refresh(new_organisation)
    return new_organisation


def update(
    session: Session,
    schema_in: OrganisationUpdateAdmin,
    model_in: Organisation,
    admins: list[User] = None,
    topics: list[Topic] = None,
    location: Location = None,
) -> Organisation:
    """Update an organisation."""
    model_updated = model_in

    if schema_in.name is not None:
        model_updated.name = schema_in.name
    if schema_in.description is not None:
        model_updated.description = schema_in.description
    if schema_in.url is not None:
        model_updated.url = schema_in.url
    if schema_in.public_mail_contact is not None:
        model_updated.public_mail_contact = schema_in.public_mail_contact
    if schema_in.internal_point_of_contact is not None:
        model_updated.internal_point_of_contact = schema_in.internal_point_of_contact
    if location is not None:
        model_updated.location = location
    if schema_in.type_id is not None:
        model_updated.type_id = schema_in.type_id
    if schema_in.image_name is not None:
        model_updated.image_name = schema_in.image_name
    if schema_in.approved is not None:
        model_updated.approved = schema_in.approved
    if admins is not None:
        model_updated.admins = admins
    if topics is not None:
        model_updated.topics = topics

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated


def add_admin(
    session: Session, model_in: Organisation, new_admin: User
) -> Organisation:
    """Add an admin to an organisation."""
    if new_admin not in model_in.admins:
        model_in.admins.append(new_admin)
        session.add(model_in)
        session.commit()
        session.refresh(model_in)

    return model_in


def remove_admin(
    session: Session, model_in: Organisation, admin_to_remove: User
) -> Organisation:
    """Remove an admin from an organisation."""
    if admin_to_remove in model_in.admins:
        model_in.admins.remove(admin_to_remove)
        session.add(model_in)
        session.commit()
        session.refresh(model_in)

    return model_in


def search(
    session: Session,
    limit: int,
    offset: int,
    organisation_name: str | None = None,
    type_id_list: list[int] | None = None,
    topic_id_list: list[int] | None = None,
    location_list: list[Location] | None = None,
    approved: RequestStatus = RequestStatus.APPROVED,
) -> list[Organisation]:
    """Search an organisation."""
    statement = select(Organisation).limit(limit).offset(offset)
    if approved is not None:
        statement = statement.where(Organisation.approved == approved)
    if organisation_name:
        statement = statement.where(col(Organisation.name).contains(organisation_name))
    if type_id_list:
        statement = statement.where(col(Organisation.type_id).in_(type_id_list))
    if topic_id_list:
        links = session.exec(
            select(TopicOrganisationLink).where(
                col(TopicOrganisationLink.topic_id).in_(topic_id_list)
            )
        ).all()
        organisation_ids = {link.organisation_id for link in links}
        statement = statement.where(col(Organisation.id).in_(organisation_ids))

    if location_list is not None:
        statement = statement.where(
            col(Organisation.location_id).in_(
                [location.id for location in location_list]
            )
        )
    results = session.exec(statement)
    return results.all()


def add_relation(
    session: Session, model_in: Organisation, org_to_add: Organisation
) -> Organisation:
    """Relate two organisations."""
    if org_to_add not in model_in.relations:
        model_in.relations.append(org_to_add)
        session.add(model_in)
        session.commit()
        session.refresh(model_in)

    return model_in


def add_follower(
    session: Session, model_in: Organisation, follower: User
) -> Organisation:
    """Add a follower to an organisation."""
    if follower not in model_in.followers:
        model_in.followers.append(follower)
        session.add(model_in)
        session.commit()
        session.refresh(model_in)

    return model_in


def remove_relation(
    session: Session, model_in: Organisation, org_to_remove: Organisation
) -> Organisation:
    """Remove a relation between two organisations."""
    if org_to_remove in model_in.relations:
        model_in.relations.remove(org_to_remove)
        session.add(model_in)
        session.commit()
        session.refresh(model_in)

    return model_in
