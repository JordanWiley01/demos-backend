# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Organisation type request related database operations."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from sqlmodel import Session, select

from demos_backend.core.models.organisation_type_request import OrganisationTypeRequest
from demos_backend.core.schemas.organisation_type_request import (
    OrganisationTypeRequestCreate,
    OrganisationTypeRequestUpdate,
)
from demos_backend.core.types import RequestStatus


def get_all(session: Session) -> list[OrganisationTypeRequest]:
    """Get all organisation type requests."""
    statement = select(OrganisationTypeRequest)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, request_id: int) -> Optional[OrganisationTypeRequest]:
    """Get organisation type request by ID."""
    return session.get(OrganisationTypeRequest, request_id)


def get_by_status(
    session: Session, request_status: RequestStatus
) -> list[OrganisationTypeRequest]:
    """Get organisation type request by status."""
    statement = select(OrganisationTypeRequest).where(
        OrganisationTypeRequest.status == request_status
    )
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: OrganisationTypeRequestCreate,
    requester_user_id: int | None,
) -> OrganisationTypeRequest:
    """Create organisation type request."""
    new_organisation_type_request = OrganisationTypeRequest(
        name=schema_in.name,
        created=datetime.now(),
        status=RequestStatus.PENDING,
        requester_user_id=requester_user_id,
        requester_organisation_id=schema_in.requester_organisation_id,
    )
    session.add(new_organisation_type_request)
    session.commit()
    return new_organisation_type_request


def update(
    session: Session,
    schema_in: OrganisationTypeRequestUpdate,
    model_in: OrganisationTypeRequest,
) -> OrganisationTypeRequest:
    """Update organisation type request."""
    model_updated = model_in

    # can be set by self
    model_updated.status = schema_in.status

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated
