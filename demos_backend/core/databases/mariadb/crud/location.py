# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Location related database operations."""

from __future__ import annotations

import math
from typing import Optional

from sqlmodel import Session, col, select

from demos_backend.core.models.location import Location
from demos_backend.core.schemas.location import LocationCreate, LocationUpdate


def get_by_id(session: Session, location_id: int) -> Optional[Location]:
    """Get a location by ID."""
    return session.get(Location, location_id)


def get_all(session: Session) -> list[Location]:
    """Get all locations."""
    statement = select(Location)

    results = session.exec(statement)
    return results.all()


def get_by_radius(
    session: Session, mid_lat: float, mid_long: float, radius_km: float
) -> list[Location]:
    """Find locations within a radius of a point."""
    # latitudes are apart ~111km
    radius_latitude = radius_km / 111
    # distance between longitudes depends on the latitude.
    # ~111km at equator / 0km at poles
    lat_rad = (math.pi / 180) * mid_lat
    radius_longitude = radius_km / (math.cos(lat_rad) * 111)

    min_latitude = mid_lat - radius_latitude
    max_latitude = mid_lat + radius_latitude
    min_longitude = mid_long - radius_longitude
    max_longitude = mid_long + radius_longitude

    statement = (
        select(Location)
        .where(Location.latitude >= min_latitude)
        .where(Location.latitude <= max_latitude)
        .where(Location.longitude >= min_longitude)
        .where(Location.longitude <= max_longitude)
    )

    results = session.exec(statement)
    return results.all()


def get_by_address(
    session: Session, country: str, city: str, zip_code: str
) -> list[Location]:
    """Find locations with a certain address."""
    statement = select(Location)

    statement = statement.where(col(Location.country).contains(country))
    statement = statement.where(col(Location.city).contains(city))
    statement = statement.where(col(Location.zip_code).contains(zip_code))

    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: LocationCreate,
) -> Location:
    """Create a location."""
    new_location_type = Location(
        latitude=schema_in.latitude,
        longitude=schema_in.longitude,
        country=schema_in.country,
        city=schema_in.city,
        street=schema_in.street,
        zip_code=schema_in.zip_code,
        house_number=schema_in.house_number,
    )
    session.add(new_location_type)
    session.commit()
    session.refresh(new_location_type)
    return new_location_type


def update(
    session: Session,
    schema_in: LocationUpdate,
    model_in: Location,
) -> Location:
    """Update a location."""
    model_updated = model_in

    if schema_in.latitude is not None:
        model_updated.latitude = schema_in.latitude
    if schema_in.longitude is not None:
        model_updated.longitude = schema_in.longitude
    if schema_in.country is not None:
        model_updated.country = schema_in.country
    if schema_in.city is not None:
        model_updated.city = schema_in.city
    if schema_in.zip_code is not None:
        model_updated.zip_code = schema_in.zip_code
    if schema_in.house_number is not None:
        model_updated.house_number = schema_in.house_number
    if schema_in.street is not None:
        model_updated.street = schema_in.street

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated


def delete(session: Session, location: Location) -> Location:
    """Delete a location."""
    session.delete(location)
    session.commit()
    return location
