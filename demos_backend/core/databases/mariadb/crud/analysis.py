# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Report related database operations."""

from __future__ import annotations

from sqlmodel import Session, col, select

from demos_backend.core.models.event import Event
from demos_backend.core.models.link_tables import EventTopicLink
from demos_backend.core.schemas.analysis import AnalysisEventQuery


def get_event_analysis_data(session: Session, query: AnalysisEventQuery) -> list[Event]:
    """Get event report data."""
    statement = select(Event)

    if query.event_time == "start":
        statement = statement.where(Event.start_time >= query.start_time)
        statement = statement.where(Event.start_time <= query.end_time)
        statement = statement.order_by(Event.start_time)
    else:
        statement = statement.where(Event.created >= query.start_time)
        statement = statement.where(Event.created <= query.end_time)
        statement = statement.order_by(Event.created)

    if query.type_ids:
        statement = statement.where(col(Event.type_id).in_(query.type_ids))

    if query.topic_ids:
        links = session.exec(
            select(EventTopicLink).where(
                col(EventTopicLink.topic_id).in_(query.topic_ids)
            )
        ).all()
        event_ids = {link.event_id for link in links}
        statement = statement.where(col(Event.id).in_(event_ids))

    results = session.exec(statement)
    return results.all()
