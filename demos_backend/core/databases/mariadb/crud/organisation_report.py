# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Organisation report related database operations."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from sqlmodel import Session, select

from demos_backend.core.models.organisation_report import OrganisationReport
from demos_backend.core.schemas.organisation_report import (
    OrganisationReportCreate,
    OrganisationReportUpdate,
)
from demos_backend.core.types import ReportStatus


def get_all(session: Session) -> list[OrganisationReport]:
    """Get all organisation reports."""
    statement = select(OrganisationReport)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, report_id: int) -> Optional[OrganisationReport]:
    """Get an organisation report by ID."""
    return session.get(OrganisationReport, report_id)


def get_by_status(
    session: Session, organisation_report_status: ReportStatus
) -> list[OrganisationReport]:
    """Get organisation reports by approval status."""
    statement = select(OrganisationReport).where(
        OrganisationReport.status == organisation_report_status
    )
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: OrganisationReportCreate,
    reporter_user_id: int,
) -> OrganisationReport:
    """Create an organisation reports."""
    new_organisation_report = OrganisationReport(
        reported_organisation_id=schema_in.reported_organisation_id,
        description=schema_in.description,
        created=datetime.now(),
        status=ReportStatus.PENDING,
        reporter_user_id=reporter_user_id,
        reporter_organisation_id=schema_in.reporter_organisation_id,
    )
    session.add(new_organisation_report)
    session.commit()
    session.refresh(new_organisation_report)
    return new_organisation_report


def update(
    session: Session,
    schema_in: OrganisationReportUpdate,
    model_in: OrganisationReport,
) -> OrganisationReport:
    """Update an organisation reports."""
    model_updated = model_in

    # can be set by self
    model_updated.status = schema_in.status

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated
