# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Organisation type related database operations."""

from __future__ import annotations

from typing import Optional

from sqlmodel import Session, col, select

from demos_backend.core.models.organisation_type import OrganisationType
from demos_backend.core.schemas.organisation_type import (
    OrganisationTypeCreate,
    OrganisationTypeUpdate,
)


def get_all(session: Session) -> list[OrganisationType]:
    """Get all organisation types."""
    statement = select(OrganisationType)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, type_id: int) -> Optional[OrganisationType]:
    """Get organisation type by ID."""
    return session.get(OrganisationType, type_id)


def get_by_ids(session: Session, type_ids: list[int]) -> list[OrganisationType]:
    """Get organisation types by IDs."""
    statement = select(OrganisationType).where(col(OrganisationType.id).in_(type_ids))
    results = session.exec(statement)
    return results.all()


def get_by_name(
    session: Session, organisation_type_name: str
) -> list[OrganisationType]:
    """Get organisation type by name."""
    statement = select(OrganisationType).where(
        OrganisationType.name_en == organisation_type_name
    )
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: OrganisationTypeCreate,
) -> OrganisationType:
    """Create organisation type."""
    new_organisation_type = OrganisationType(
        name_en=schema_in.name_en, name_de=schema_in.name_de
    )
    session.add(new_organisation_type)
    session.commit()
    session.refresh(new_organisation_type)
    return new_organisation_type


def update(
    session: Session,
    schema_in: OrganisationTypeUpdate,
    model_in: OrganisationType,
) -> OrganisationType:
    """Update organisation type."""
    model_updated = model_in

    # can be set by self
    model_updated.name_en = schema_in.name_en
    model_updated.name_de = schema_in.name_de

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated


def delete_by_id(
    session: Session, organisation_type: OrganisationType
) -> OrganisationType:
    """Delete organisation type by ID."""
    session.delete(organisation_type)
    session.commit()
    return organisation_type
