# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Organisation relation request related database operations."""

from __future__ import annotations

from datetime import datetime
from typing import Optional

from sqlmodel import Session, select

from demos_backend.core.models.organisation import Organisation
from demos_backend.core.models.organisation_relation_request import (
    OrganisationRelationRequest,
)
from demos_backend.core.schemas.organisation_relation_request import (
    OrganisationRelationRequestUpdate,
)
from demos_backend.core.types import RequestStatus


def get_all(session: Session) -> list[OrganisationRelationRequest]:
    """Get all organisation relation requests."""
    statement = select(OrganisationRelationRequest)
    results = session.exec(statement)
    return results.all()


def get_by_orgs(
    session: Session, requester: Organisation, requested: Organisation
) -> Optional[OrganisationRelationRequest]:
    """Get an organisation relation request by involved organisations."""
    statement = select(OrganisationRelationRequest)
    statement = statement.where(
        OrganisationRelationRequest.organisation_requester == requester
    )
    statement = statement.where(
        OrganisationRelationRequest.organisation_requested == requested
    )

    return session.exec(statement).one_or_none()


def create(
    session: Session,
    requester_organisation: Organisation,
    requested_organisation: Organisation,
) -> OrganisationRelationRequest:
    """Create an organisation relation request."""
    new_organisation_relation_request = OrganisationRelationRequest(
        organisation_requester=requester_organisation,
        organisation_requested=requested_organisation,
        created=datetime.now(),
        status=RequestStatus.PENDING,
    )
    session.add(new_organisation_relation_request)
    session.commit()
    session.refresh(new_organisation_relation_request)
    return new_organisation_relation_request


def update(
    session: Session,
    schema_in: OrganisationRelationRequestUpdate,
    model_in: OrganisationRelationRequest,
) -> OrganisationRelationRequest:
    """Update an organisation relation request."""
    model_updated = model_in

    if schema_in.status is not None:
        model_updated.status = schema_in.status

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated
