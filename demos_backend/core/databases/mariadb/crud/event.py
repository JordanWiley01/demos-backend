# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Event related database operations."""

from __future__ import annotations

from datetime import datetime
from typing import Literal, Optional

from sqlmodel import Session, col, select

from demos_backend.core.models.event import Event
from demos_backend.core.models.link_tables import (
    EventCoOrganisationLink,
    EventTopicLink,
)
from demos_backend.core.models.location import Location
from demos_backend.core.models.organisation import Organisation
from demos_backend.core.models.topic import Topic
from demos_backend.core.models.user import User
from demos_backend.core.schemas.event import (
    EventCreate,
    EventCreateCrawler,
    EventSearch,
    EventUpdate,
)


def get_all(session: Session) -> list[Event]:
    """Get all events."""
    statement = select(Event)
    results = session.exec(statement)
    return results.all()


def count(session: Session) -> int:
    """Get number of events."""
    return session.query(Event).count()


def get_preexisting_event(
    session: Session,
    schema_in: EventCreateCrawler,
) -> Event:
    """Find events by create schema."""
    statement = (
        select(Event)
        .where(Event.name == schema_in.name)
        .where(Event.start_time == schema_in.start_time)
        .where(Event.end_time == schema_in.end_time)
        .where(Event.description == schema_in.description)
        .where(Event.raw_location == schema_in.raw_location)
    )
    results = session.exec(statement)
    return results.first()


def get_by_id(session: Session, event_id: int) -> Optional[Event]:
    """Get an event by ID."""
    return session.get(Event, event_id)


def get_by_name(session: Session, event_name: str) -> list[Event]:
    """Get an event by name."""
    statement = select(Event).where(Event.name == event_name)
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: EventCreate | EventCreateCrawler,
    creator: User,
    organising_organisation: Organisation,
    topics: list[Topic],
    co_organizers: list[Organisation],
    location: Location,
) -> Event:
    """Create an event."""
    raw_location = None
    raw_estimated_participants = None
    raw_organiser = None
    if isinstance(schema_in, EventCreateCrawler):
        raw_location = schema_in.raw_location
        raw_estimated_participants = schema_in.raw_estimated_participants
        raw_organiser = schema_in.raw_organiser
    new_event_type = Event(
        name=schema_in.name,
        short_description=schema_in.short_description,
        description=schema_in.description,
        created=datetime.now(),
        updated=datetime.now(),
        url=schema_in.url,
        start_time=schema_in.start_time,
        end_time=schema_in.end_time,
        creator=creator,
        location=location,
        type_id=schema_in.type_id,
        organising_organisation=organising_organisation,
        topics=topics,
        co_organisers=co_organizers,
        raw_location=raw_location,
        raw_estimated_participants=raw_estimated_participants,
        raw_organiser=raw_organiser,
        visible=True,
    )
    session.add(new_event_type)
    session.commit()
    session.refresh(new_event_type)
    return new_event_type


def update(
    session: Session,
    schema_in: EventUpdate,
    model_in: Event,
    co_organizers: list[Organisation] = None,
    topics: list[Topic] = None,
    location: Location = None,
    visible: bool = None,
) -> Event:
    """Update an event."""
    model_updated = model_in

    if schema_in.name is not None:
        model_updated.name = schema_in.name
    if schema_in.short_description is not None:
        model_updated.short_description = schema_in.short_description
    if schema_in.description is not None:
        model_updated.description = schema_in.description
    if schema_in.url is not None:
        model_updated.url = schema_in.url
    if schema_in.start_time is not None:
        model_updated.start_time = schema_in.start_time
    if schema_in.end_time is not None:
        model_updated.end_time = schema_in.end_time
    if location is not None:
        model_updated.location = location
    if schema_in.type_id is not None:
        model_updated.type_id = schema_in.type_id
    if schema_in.organising_organisation_id is not None:
        model_updated.organising_organisation_id = schema_in.organising_organisation_id
    if co_organizers:
        model_updated.co_organisers = co_organizers
    if topics:
        model_updated.topics = topics
    if visible is not None:
        model_updated.visible = visible
    model_updated.updated = datetime.now()

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated


def delete_by_id(session: Session, model_in: Event) -> Optional[Event]:
    """Delete an event."""
    session.delete(model_in)
    session.commit()
    return model_in


def search(
    session: Session,
    schema_in: EventSearch,
    limit: int | None = None,
    offset: int | None = None,
    location_list: Optional[list[Location]] = None,
    time_sorting: Literal["asc", "desc"] = "asc",
    visible: Optional[bool] = None,
) -> list[Event]:
    """Search events."""
    statement = select(Event)
    if limit is not None:
        statement = statement.limit(limit)
    if offset is not None:
        statement = statement.offset(offset)

    if schema_in.name:
        statement = statement.where(col(Event.name).contains(schema_in.name))

    if schema_in.start_time_start:
        statement = statement.where(Event.start_time > schema_in.start_time_start)
    if schema_in.start_time_end:
        statement = statement.where(Event.start_time < schema_in.start_time_end)
    if schema_in.end_time_start:
        statement = statement.where(Event.end_time > schema_in.end_time_start)
    if schema_in.end_time_end:
        statement = statement.where(Event.end_time < schema_in.end_time_end)

    if schema_in.topic_ids:
        links = session.exec(
            select(EventTopicLink).where(
                col(EventTopicLink.topic_id).in_(schema_in.topic_ids)
            )
        ).all()
        event_ids = {link.event_id for link in links}
        statement = statement.where(col(Event.id).in_(event_ids))

    if schema_in.co_organiser_ids:
        links = session.exec(
            select(EventCoOrganisationLink).where(
                col(EventCoOrganisationLink.organisation_id).in_(
                    schema_in.co_organiser_ids
                )
            )
        ).all()
        event_ids = {link.event_id for link in links}
        statement = statement.where(col(Event.id).in_(event_ids))

    if schema_in.type_ids:
        statement = statement.where(col(Event.type_id).in_(schema_in.type_ids))

    if location_list is not None:
        statement = statement.where(
            col(Event.location_id).in_([location.id for location in location_list])
        )

    if schema_in.organiser_ids:
        statement = statement.where(
            (col(Event.organising_organisation_id).in_(schema_in.organiser_ids))
        )
    if visible is not None:
        statement = statement.where(Event.visible == visible)

    if time_sorting == "asc":
        statement = statement.order_by(Event.start_time)
    if time_sorting == "desc":
        statement = statement.order_by(col(Event.start_time).desc())

    results = session.exec(statement)
    return results.all()
