# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Event type related database operations."""

from __future__ import annotations

from typing import Optional

from sqlmodel import Session, col, select

from demos_backend.core.models.event_type import EventType
from demos_backend.core.schemas.event_type import EventTypeCreate, EventTypeUpdate


def get_all(session: Session) -> list[EventType]:
    """Get all event types."""
    statement = select(EventType)
    results = session.exec(statement)
    return results.all()


def get_by_id(session: Session, type_id: int) -> Optional[EventType]:
    """Get an event type by ID."""
    return session.get(EventType, type_id)


def get_by_ids(session: Session, type_ids: list[int]) -> list[EventType]:
    """Get event types by IDs."""
    statement = select(EventType).where(col(EventType.id).in_(type_ids))
    results = session.exec(statement)
    return results.all()


def get_by_name(session: Session, event_type_name: str) -> list[EventType]:
    """Get an event type by name."""
    statement = select(EventType).where(EventType.name_en == event_type_name)
    results = session.exec(statement)
    return results.all()


def create(
    session: Session,
    schema_in: EventTypeCreate,
) -> EventType:
    """Create an event type."""
    new_event_type = EventType(
        name_en=schema_in.name_en,
        name_de=schema_in.name_de,
    )
    session.add(new_event_type)
    session.commit()
    session.refresh(new_event_type)
    return new_event_type


def update(
    session: Session,
    schema_in: EventTypeUpdate,
    model_in: EventType,
) -> EventType:
    """Update an event type."""
    model_updated = model_in

    # can be set by self
    model_updated.name_en = schema_in.name_en
    model_updated.name_de = schema_in.name_de

    session.add(model_updated)
    session.commit()
    session.refresh(model_updated)

    return model_updated


def delete_by_id(session: Session, event_type: EventType) -> EventType:
    """Delete an event type."""
    session.delete(event_type)
    session.commit()
    return event_type
