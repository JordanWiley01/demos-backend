# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains initial data taken from the prototype."""

initial_info = [
    (
        "Bürgersprechstunde - Bürgermeister Büro",
        "Citizens Consultation (Bürgersprechstunde) - Mayor's Office",
        "https://frankfurt.de/service-und-rathaus/stadtpolitik/oberbuergermeister/buergerbuero/buergersprechstunde",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Ideenplattform - Frankfurt am Main",
        "Idea Platform - Frankfurt am Main",
        "https://www.ffm.de/frankfurt/de/ideaPtf/45035",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Offene Daten Portal - Stadt Frankfurt am Main",
        "Open Data Portal - Frankfurt am Main",
        "https://offenedaten.frankfurt.de/dataset?organization=buergeramt-statistik-und-wahlen&_groups_limit=0",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Verfassung - Hessen",
        "Constitution - Hessen",
        "https://www.rv.hessenrecht.hessen.de/bshe/document/jlr-VerfHErahmen",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Service-Portal - Frankfurt am Main",
        "Service Portal - Frankfurt am Main",
        "https://frankfurt.de/service-und-rathaus/service",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Landesregierung - Hessen",
        "Senate of Hessen",
        "https://www.hessen.de/regierung",
        "https://english.hessen.de/about-us/cabinet",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Bürgeramt - Bergen-Enkheim",
        "District Office - Bergen-Enkheim",
        "https://frankfurt.de/adressen/buergeramt-statistik-und-wahlen/buergeramt/dezentrale-buergeraemter/buergeramt-bergen-enkheim",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Bürgeramt - Nieder-Eschbach",
        "District Office - Nieder-Eschbach",
        "https://frankfurt.de/adressen/buergeramt-statistik-und-wahlen/buergeramt/dezentrale-buergeraemter/buergeramt-nieder-eschbach",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Bürgeramt - Sachsenhausen",
        "District Office - Sachsenhausen",
        "https://frankfurt.de/adressen/buergeramt-statistik-und-wahlen/buergeramt/dezentrale-buergeraemter/buergeramt-sachsenhausen",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Bürgeramt - Dornbusch",
        "District Office - Dornbusch",
        "https://frankfurt.de/adressen/buergeramt-statistik-und-wahlen/buergeramt/dezentrale-buergeraemter/buergeramt-dornbusch",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Bürgeramt - Nordwest",
        "District Office - Nordwest",
        "https://frankfurt.de/adressen/buergeramt-statistik-und-wahlen/buergeramt/dezentrale-buergeraemter/buergeramt-nordwest",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Bürgeramt - Höchst",
        "District Office - Höchst",
        "https://frankfurt.de/adressen/buergeramt-statistik-und-wahlen/buergeramt/dezentrale-buergeraemter/buergeramt-hoechst",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Zentrales Bürgeramt - Frankfurt am Main",
        "Central Citizen Office - Frankfurt am Main",
        "https://frankfurt.de/adressen/buergeramt-statistik-und-wahlen/buergeramt/zentrales-buergeramt",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Haushaltspläne - Frankfurt am Main",
        "Budgets - Frankfurt am Main",
        "https://frankfurt.haushaltsdaten.de/",
        "",
        "Frankfurt am Main",
        "Germany",
    ),
    (
        "Haushaltspläne - Berlin",
        "Budgets - Berlin",
        "https://www.berlin.de/sen/finanzen/haushalt/downloads/haushaltsplan-2022-23/artikel.1232802.php",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Haushaltspläne - Hamburg",
        "Budgets - Hamburg",
        "https://www.hamburg.de/fb/haushaltsplaene/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksversammlung – Hamburg-Mitte",
        "Bezirksversammlung / District Assembly – Hamburg-Mitte",
        "https://www.hamburg.de/mitte/bezirksversammlung/2402838/bezirksversammlung/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksamt – Hamburg-Mitte",
        "District Office – Hamburg-Mitte",
        "https://www.hamburg.de/mitte/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksamt – Harburg",
        "District Office – Harburg",
        "https://www.hamburg.de/harburg/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksamt – Bergedorf",
        "District Office – Bergedorf",
        "https://www.hamburg.de/bergedorf/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksamt – Wandsbek",
        "District Office – Wandsbek",
        "https://www.hamburg.de/wandsbek/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksamt – Hamburg-Nord",
        "District Office – Hamburg-Nord",
        "https://www.hamburg.de/hamburg-nord/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksamt – Eimsbüttel",
        "District Office – Eimsbüttel",
        "https://www.hamburg.de/eimsbuettel/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksamt - Altona",
        "District Office - Altona",
        "https://www.hamburg.de/altona/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksversammlung – Harburg",
        "Bezirksversammlung / District Assembly – Harburg",
        "https://www.hamburg.de/harburg/bezirksversammlung-harburg/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksversammlung – Bergedorf",
        "Bezirksversammlung / District Assembly – Bergedorf",
        "https://www.hamburg.de/bergedorf/bezirksversammlung/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksversammlung – Wandsbek",
        "Bezirksversammlung / District Assembly – Wandsbek",
        "https://www.hamburg.de/wandsbek/bezirksversammlung/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksversammlung – Hamburg-Nord",
        "Bezirksversammlung / District Assembly – Hamburg Nord",
        "https://www.hamburg.de/hamburg-nord/bezirksversammlung/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksversammlung – Eimsbüttel",
        "Bezirksversammlung / District Assembly – Eimsbüttel",
        "https://www.hamburg.de/eimsbuettel/bezirksversammlung/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bezirksversammlung - Altona",
        "Bezirksversammlung / District Assembly - Altona",
        "https://www.hamburg.de/bezirksversammlung-altona/74710/bezirksversammlung-altona/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Harburg",
        "Citizens Consultation (Bürgersprechstunde) - Harburg",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "Hamburg",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Bergedorf",
        "Citizens Consultation (Bürgersprechstunde) - Bergedorf",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "Hamburg",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Wandsbek",
        "Citizens Consultation (Bürgersprechstunde) - Wandsbek",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "Hamburg",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Hamburg-Nord",
        "Citizens Consultation (Bürgersprechstunde) - Hamburg-Nord",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "Hamburg",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Eimsbüttel",
        "Citizens Consultation (Bürgersprechstunde) - Eimsbüttel",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "Hamburg",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Hamburg-Mitte",
        "Citizens Consultation (Bürgersprechstunde) - Hamburg-Mitte",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "Hamburg",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Altona",
        "Citizens Consultation (Bürgersprechstunde) – Altona",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "https://www.hamburg.de/behoerdenfinder/hamburg/11255665/",
        "Hamburg",
        "Germany",
    ),
    (
        "Landesregierung - Hamburg",
        "Senate of Hamburg",
        "https://www.hamburg.de/senatoren/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Service-Portal - Hamburg",
        "Service-Portal - Hamburg",
        "https://serviceportal.hamburg.de/HamburgGateway/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Verfassung - Hamburg",
        "Constitution - Hamburg",
        "http://www.landesrecht-hamburg.de/jportal/portal/page/bshaprod.psml?showdoccase=1&st=lr&doc.id=jlr-VerfHArahmen",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Volksbegehren - Hamburg",
        "People Initiatives - Hamburg",
        "https://www.hamburg.de/volksabstimmungen/54020/volksabstimmungen/",
        "",
        "Hamburg",
        "Germany",
    ),
    (
        "Volksbegehren - Berlin",
        "People Initiatives",
        "https://www.berlin.de/sen/inneres/buerger-und-staat/wahlen-und-abstimmungen/volksinitiative-volksbegehren-volksentscheid/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Verfassung - Berlin",
        "Constitution - Berlin",
        "https://www.berlin.de/rbmskzl/regierender-buergermeister/verfassung/",
        "https://www.berlin.de/rbmskzl/en/the-governing-mayor/the-constitution-of-berlin/",
        "Berlin",
        "Germany",
    ),
    (
        "Statistisches Bundesamt",
        "Federal Statistical Office",
        "https://www.destatis.de/DE/Home/_inhalt.html",
        "https://www.destatis.de/EN/Home/_node.html",
        "",
        "Germany",
    ),
    (
        "Service-Portal - Berlin",
        "Service Portal - Berlin",
        "https://service.berlin.de/dienstleistungen/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Petitionsforum",
        "Petition Forum",
        "https://epetitionen.bundestag.de/epet/startseite.nc.html",
        "",
        "",
        "Germany",
    ),
    (
        "Petitionsausschuss",
        "Petition Committee",
        "https://www.bundestag.de/petitionen",
        "",
        "",
        "Germany",
    ),
    (
        "Mieterinformationen - Berlin",
        "Information for Tenants - Berlin",
        "https://www.berlin.de/willkommenszentrum/wohnen/mieterinformationen/",
        "https://www.bundesregierung.de/breg-en/federal-cabinet",
        "Berlin",
        "Germany",
    ),
    (
        "Landesregierung - Berlin",
        "Senate of Berlin",
        "https://www.berlin.de/rbmskzl/regierender-buergermeister/senat/",
        "https://www.berlin.de/rbmskzl/en/the-governing-mayor/senate-chancellery/artikel.16708.en.php",
        "Berlin",
        "Germany",
    ),
    (
        "Grundgesetz",
        "Basic Law for the Federal Republic of Germany",
        "http://www.gesetze-im-internet.de/gg/GG.pdf",
        "https://www.gesetze-im-internet.de/englisch_gg/",
        "",
        "Germany",
    ),
    (
        "Eurostat",
        "Eurostat",
        "https://ec.europa.eu/eurostat/de/home",
        "https://ec.europa.eu/eurostat/home",
        "",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Treptow-Köpenick",
        "Citizens Consultation (Bürgersprechstunde) - Treptow-Köpenick",
        "https://www.berlin.de/ba-treptow-koepenick/politik-und-verwaltung/bezirksamt/artikel.4962.php",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Tempelhof-Schöneberg",
        "Citizens Consultation (Bürgersprechstunde) - Tempelhof-Schöneberg",
        "https://www.berlin.de/ba-tempelhof-schoeneberg/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Steglitz-Zehlendorf",
        "Citizens Consultation (Bürgersprechstunde) - Steglitz-Zehlendorf",
        "https://www.berlin.de/ba-steglitz-zehlendorf/politik-und-verwaltung/bezirksamt/buergersprechstunden/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Spandau",
        "Citizens Consultation (Bürgersprechstunde) - Spandau",
        "https://www.berlin.de/ba-spandau/aktuelles/pressemitteilungen/2019/pressemitteilung.804510.php",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Reinickendorf",
        "Citizens Consultation (Bürgersprechstunde) - Reinickendorf",
        "",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Pankow",
        "Citizens Consultation (Bürgersprechstunde) - Pankow",
        "https://www.berlin.de/ba-pankow/politik-und-verwaltung/bezirksamt/sprechstunden/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Neukölln",
        "Citizens Consultation (Bürgersprechstunde) - Neukölln",
        "https://www.berlin.de/ba-neukoelln/aktuelles/pressemitteilungen/2020/pressemitteilung.895823.php",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Mitte",
        "Citizens Consultation (Bürgersprechstunde) - Mitte",
        "https://www.berlin.de/ba-mitte/politik-und-verwaltung/bezirksamt/buergersprechstunden/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Marzahn-Hellersdorf",
        "Citizens Consultation (Bürgersprechstunde) - Marzahn-Hellersdorf",
        "https://www.berlin.de/ba-marzahn-hellersdorf/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Lichtenberg",
        "Citizens Consultation (Bürgersprechstunde) - Lichtenberg",
        "",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bürgersprechstunde - Friedrichshain-Kreuzberg",
        "Citizens Consultation (Bürgersprechstunde) - Friedrichshain-Kreuzberg",
        "https://www.berlin.de/ba-friedrichshain-kreuzberg/politik-und-verwaltung/bezirksamt/oliver-noell/buerger-innen-sprechstunde-1200386.php#:~:text=Mittwoch%2C%2026.%20Oktober%202022%2C",
        "62%2C%2010243%20Berlin%2DFriedrichshain)",
        "Friedrichshain-Kreuzberg",
        "Berlin",
    ),
    (
        "Bürgersprechstunde - Charlottenburg-Wilmersdorf",
        "Citizens Consultation (Bürgersprechstunde) - Charlottenburg-Wilmersdorf",
        "https://www.berlin.de/ba-charlottenburg-wilmersdorf/politik/bezirksamt/artikel.266309.php",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bundesverfassungsgericht",
        "Federal Constitutional Court",
        "https://www.bundesverfassungsgericht.de/DE/Homepage/homepage_node.html",
        "https://www.bundesverfassungsgericht.de/EN/Homepage/home_node.html",
        "",
        "Germany",
    ),
    (
        "Bundestag - Abgeordnete",
        "Bundestag - Representatives",
        "https://www.bundestag.de/abgeordnete",
        "https://www.bundestag.de/en/members",
        "",
        "Germany",
    ),
    (
        "Bundestag",
        "Bundestag - Parliament",
        "https://www.bundestag.de/",
        "https://www.bundestag.de/en",
        "",
        "Germany",
    ),
    (
        "Bundesregierung - Kabinett",
        "Federal Government - Cabinet",
        "https://www.bundesregierung.de/breg-de/bundesregierung/bundeskabinett",
        "https://www.bundesregierung.de/breg-en/federal-cabinet",
        "",
        "Germany",
    ),
    (
        "Bundesrat - Mitglieder",
        "Bundesrat - Members",
        "https://www.bundesrat.de/DE/bundesrat/mitglieder/mitglieder-node.html",
        "https://www.bundesrat.de/DE/bundesrat/mitglieder/mitglieder-node.html",
        "",
        "Germany",
    ),
    (
        "Bundesrat",
        "Bundesrat - Federal Council",
        "https://www.bundesrat.de/DE/homepage/homepage-node.html",
        "https://www.bundesrat.de/EN/homepage/homepage-node.html",
        "",
        "Germany",
    ),
    (
        "Bundeshaushalt",
        "Federal Budget",
        "https://www.bundeshaushalt.de/#",
        "",
        "",
        "Germany",
    ),
    (
        "Bundesgerichtshof - BGH",
        "Federal Court of Justice",
        "https://www.bundesgerichtshof.de/DE/Home/home_node.html",
        "https://www.bundesgerichtshof.de/EN/Home/homeBGH_node.html",
        "",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Treptow-Köpenick",
        "BVV / District Parliament - Treptow-Köpenick",
        "https://www.berlin.de/ba-treptow-koepenick/politik-und-verwaltung/bezirksverordnetenversammlung/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Tempelhof-Schöneberg",
        "BVV / District Parliament - Tempelhof-Schöneberg",
        "https://www.berlin.de/ba-tempelhof-schoeneberg/politik-und-verwaltung/bezirksverordnetenversammlung/online/allris.net.asp?",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Steglitz-Zehlendorf",
        "BVV / District Parliament - Steglitz-Zehlendorf",
        "https://www.berlin.de/ba-steglitz-zehlendorf/politik-und-verwaltung/bezirksverordnetenversammlung/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Spandau",
        "BVV / District Parliament - Spandau",
        "https://www.berlin.de/ba-spandau/politik-und-verwaltung/bezirksverordnetenversammlung/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Reinickendorf",
        "BVV / District Parliament - Reinickendorf",
        "https://www.berlin.de/ba-reinickendorf/politik-und-verwaltung/bezirksverordnetenversammlung/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Pankow",
        "BVV / District Parliament - Pankow",
        "https://www.berlin.de/ba-pankow/politik-und-verwaltung/bezirksverordnetenversammlung/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Neukölln",
        "BVV / District Parliament - Neukölln",
        "https://www.berlin.de/ba-neukoelln/politik-und-verwaltung/bezirksverordnetenversammlung/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Mitte",
        "BVV / District Parliament - Mitte",
        "https://www.berlin.de/ba-mitte/politik-und-verwaltung/bezirksverordnetenversammlung/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Marzahn-Hellersdorf",
        "BVV / District Parliament - Marzahn-Hellersdorf",
        "https://www.berlin.de/ba-marzahn-hellersdorf/politik-und-verwaltung/bezirksverordnetenversammlung/online/allris.net.asp?",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Lichtenberg",
        "BVV / District Parliament - Marzahn-Hellersdorf",
        "https://www.berlin.de/ba-lichtenberg/politik-und-verwaltung/bezirksverordnetenversammlung/online/allris.net.asp?",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Friedrichshain-Kreuzberg",
        "BVV / District Parliament - Friedrichshain-Kreuzberg",
        "https://www.berlin.de/ba-friedrichshain-kreuzberg/politik-und-verwaltung/bezirksverordnetenversammlung/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksverordnetenversammlung - Charlottenburg-Wilmersdorf",
        "BVV / District Parliament - Charlottenburg-Wilmersdorf",
        "https://www.berlin.de/ba-charlottenburg-wilmersdorf/politik/bezirksverordnetenversammlung/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Treptow-Köpenick",
        "District Office - Treptow-Köpenick",
        "https://www.berlin.de/ba-treptow-koepenick/politik-und-verwaltung/bezirksamt/artikel.5752.php",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Tempelhof-Schöneberg",
        "District Office - Tempelhof-Schöneberg",
        "https://www.berlin.de/ba-tempelhof-schoeneberg/politik-und-verwaltung/bezirksamt/das-kollegium/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Steglitz-Zehlendorf",
        "District Office - Steglitz-Zehlendorf",
        "https://www.berlin.de/ba-steglitz-zehlendorf/politik-und-verwaltung/bezirksamt/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Spandau",
        "District Office - Spandau",
        "https://www.berlin.de/ba-spandau/politik-und-verwaltung/bezirksamt/das-kollegium/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Reinickendorf",
        "District Office - Reinickendorf",
        "https://www.berlin.de/ba-reinickendorf/politik-und-verwaltung/bezirksamt/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Pankow",
        "District Office - Pankow",
        "https://www.berlin.de/ba-pankow/politik-und-verwaltung/bezirksamt/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Neukölln",
        "District Office - Neukölln",
        "https://www.berlin.de/ba-neukoelln/politik-und-verwaltung/bezirksamt/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Mitte",
        "District Office - Mitte",
        "https://www.berlin.de/ba-mitte/politik-und-verwaltung/bezirksamt/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Marzahn-Hellersdorf",
        "District Office - Marzahn-Hellersdorf",
        "https://www.berlin.de/ba-marzahn-hellersdorf/politik-und-verwaltung/bezirksamt/kollegium/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Lichtenberg",
        "District Office - Lichtenberg",
        "https://www.berlin.de/ba-lichtenberg/politik-und-verwaltung/bezirksamt/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Friedrichshain-Kreuzberg",
        "District Office - Friedrichshain-Kreuzberg",
        "https://www.berlin.de/ba-friedrichshain-kreuzberg/politik-und-verwaltung/bezirksamt/",
        "",
        "Berlin",
        "Germany",
    ),
    (
        "Bezirksamt - Charlottenburg-Wilmersdorf",
        "District Office - Marzahn-Hellersdorf",
        "https://www.berlin.de/ba-charlottenburg-wilmersdorf/politik/bezirksamt/",
        "",
        "Berlin",
        "Germany",
    ),
]
initial_topics = [
    ("Judiciary", "Justiz"),
    ("Migration", "Migration"),
    ("Advertising", "Werbung"),
    ("Economy", "Wirtschaft"),
    ("Sexual Assault", "Sexuelle Gewalt"),
    ("Transportation", "Transport"),
    ("Foreign Policy", "Auslandspolitik"),
    ("Journalism", "Journalismus"),
    ("Mental Health", "Mentale Gesundheit"),
    ("Criminality", "Kriminalität"),
    ("Participatory Democracy", "Partizipatorische Demokratie"),
    ("Refugees", "Geflüchtete"),
    ("Technology", "Technologie"),
    ("Homelessness", "Obdachlosigkeit"),
    ("Financial Policy", "Finanzpolitik"),
    ("Art", "Kunst"),
    ("Family", "Familie"),
    ("Labor Rights", "Arbeitnehmerrechte"),
    ("Youth Protection", "Jugendschutz"),
    ("Women's Rights", "Frauenrechte"),
    ("Unconditional Basic Income (UBI)", "Bedingungsloses Grundeinkommen"),
    ("Transparency", "Transparenz"),
    ("Tenancy Laws", "Mietrecht"),
    ("Taxes", "Steuern"),
    ("Sustainability", "Nachhaltigkeit"),
    ("Sport", "Sport"),
    ("Social Democracy", "Sozialdemokratie"),
    ("Food", "Nahrungsmittel"),
    ("Slavery", "Sklaverei"),
    ("Sexism", "Sexismus"),
    ("Sexuality", "Sexualität"),
    ("Sex Workers Rights", "Sex Workers Rights"),
    ("Science", "Wissenschaft"),
    ("Rural Areas", "Ländlich Räume"),
    ("Abortion", "Abtreibung"),
    ("Religion", "Religion"),
    ("Reform", "Reform"),
    ("Privacy", "Datenschutz"),
    ("Prison Reform", "Gefängnisreform"),
    ("Press Freedom", "Pressefreiheit"),
    ("Poverty", "Armut"),
    ("Pornography", "Pornographie"),
    ("Open Source", "Open Source"),
    ("Open Access", "Open Access"),
    ("Music", "Musik"),
    ("Inclusion", "Inklusion"),
    ("Human Trafficking", "Menschenhandel"),
    ("Mad Pride", "Mad Pride"),
    ("Lobbyism", "Lobbyismus"),
    ("Literacy", "Alphabetisierung"),
    ("LGBTQ", "LGBTQ"),
    ("Internet", "Internet"),
    ("Integration", "Integration"),
    ("Indigenous", "Indigene"),
    ("Immigration", "Immigration"),
    ("Human Rights", "Menschenrechte"),
    ("Housing", "Wohnraum"),
    ("Health Care", "Gesundheitswesen"),
    ("Gun Laws", "Waffenrechte"),
    ("Green Energy", "Grüne Energie"),
    ("Global Justice", "Globale Gerechtigkeit"),
    ("Gentrification", "Gentrifikation"),
    ("Addiction", "Sucht"),
    ("Freedom of Speech", "Meinungsfreiheit"),
    ("Freedom of Information", "Informationsfreiheit"),
    ("Feminism", "Feminismus"),
    ("Fair Trade", "Fair Trade"),
    ("European Union", "Europäische Union"),
    ("Equal Rights", "Gleichberechtigung"),
    ("Environmental Justice", "Umweltgerechtigkeit"),
    ("Environment", "Umwelt"),
    ("Seniors", "Senioren"),
    ("Education", "Bildung"),
    ("Drugs", "Drogen"),
    ("Domestic Violence", "Häusliche Gewalt"),
    ("Disability Rights", "Behindertenrechte"),
    ("Disability", "Behinderung"),
    ("Democracy", "Demokratie"),
    ("Deforestation", "Abholzung"),
    ("Cycling", "Fahrrad"),
    ("Culture", "Kultur"),
    ("Consumer Rights", "Verbraucherrechte"),
    ("Climate Change", "Klimawandel"),
    ("Civil Rights", "Bürgerrechte"),
    ("Children Rights", "Kinderrechte"),
    ("Bullying", "Bullying"),
    ("Arms Exports", "Waffenexport"),
    ("War", "War"),
    ("Racism", "Rassimus"),
    ("Nuclear Weapons", "Atomwaffen"),
    ("Corruption", "Korruption"),
    ("Consumerism", "Konsumismus"),
    ("Capitalism", "Kapitalismus"),
    ("Animal Rights", "Tierrechte"),
    ("Agriculture", "Landwirtschaft"),
    ("Access to Knowledge", "Access to Knowledge"),
]
initial_event_types = [
    ("Vigil", "Mahnwache"),
    ("Workshop", "Workshop"),
    ("Art Installation", "Kunstinstallation"),
    ("Boycott", "Boykott"),
    ("Critical Mass (Bike)", "Critical Mass (Fahrrad)"),
    ("Motorcade", "Fahrzeugkolonne"),
    ("Info Booth", "Infostand"),
    ("Strike", "Streik"),
    ("Round Table", "Runder Tisch"),
    ("Rally", "Kundgebung"),
    ("Protest", "Protest"),
    ("Demonstration", "Demonstration"),
    ("March", "Marsch"),
]
initial_organisation_types = [
    ("Initiative", "Initiative"),
    ("Other", "Sonstige"),
    ("Foundation", "Stiftung"),
    ("Syndicate", "Syndikat"),
    ("Union", "Gewerkschaft"),
    ("Alliance", "Allianz"),
    ("Association", "Verein"),
    ("NGO", "Gemeinnütziger Verein"),
]
