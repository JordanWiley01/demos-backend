# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module handles connections to a MongoDB."""
from motor.motor_asyncio import AsyncIOMotorClient

from demos_backend.core.configuration import get_config


def get_client() -> AsyncIOMotorClient:
    """Get a client for a mongodb."""
    config = get_config()
    return AsyncIOMotorClient(
        config.MONGODB_HOSTNAME,
        config.MONGODB_PORT,
        username=config.MONGODB_USERNAME,
        password=config.MONGODB_PASSWORD,
    )


def get_database():
    """Get the database within a mongodb."""
    return get_client()["demosdb"]
