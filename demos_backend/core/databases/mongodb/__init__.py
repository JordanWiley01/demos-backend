# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This file only exposes certain objects in a different way for easier usage."""
from __future__ import annotations

import logging
from pathlib import Path

from beanie import init_beanie

from demos_backend.core.databases.mongodb.collection import get_database
from demos_backend.core.models.image import Image
from demos_backend.core.models.internal_document import InternalDocument
from demos_backend.core.schemas.image import ImageCreate
from demos_backend.core.schemas.internal_document import InternalDocumentCreate
from demos_backend.core.types import AvailableLanguages, InternalDocumentType


async def initialize_beanie():
    """Initialize beanie ODM."""
    await init_beanie(
        database=get_database(), document_models=[Image, InternalDocument]
    )


async def initialize_mongo(test_mode: bool = False, initial_data: bool = False) -> None:
    """Do all required steps for setting up MongoDB."""
    await initialize_beanie()
    if test_mode:
        await _create_demo_image()
        await _create_demo_documents()
    logging.debug("MongoDB finished initializing.")


async def _create_demo_image() -> None:
    with open(Path(__file__).parent / "test_data/test_image.png", "rb") as file:
        file_bytes = file.read()
    data = ImageCreate(creator_user_id="1", image=file_bytes, desired_name="0" * 32)
    await Image.new(data)


async def _create_demo_documents() -> None:
    with open(Path(__file__).parent / "test_data/Demo.pdf", "rb") as file:
        file_bytes = file.read()
    data = InternalDocumentCreate(
        creator_user_id="1",
        data=file_bytes,
        language=AvailableLanguages.GERMAN,
        type=InternalDocumentType.FINANCIAL_REPORT,
        file_name="Demo.pdf",
        display_name="Financial Report 2000",
    )
    await InternalDocument.new(data)

    with open(Path(__file__).parent / "test_data/Modem.pdf", "rb") as file:
        file_bytes = file.read()
    data = InternalDocumentCreate(
        creator_user_id="1",
        data=file_bytes,
        language=AvailableLanguages.ENGLISH,
        type=InternalDocumentType.CONSTITUTION,
        file_name="Modem.pdf",
        display_name="Constitution",
    )
    await InternalDocument.new(data)
