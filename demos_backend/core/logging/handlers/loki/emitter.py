# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the actual emitter to send logs to Loki."""
from __future__ import annotations

import asyncio
import logging
import re
import sys
from logging.config import ConvertingDict
from typing import Tuple

import aiohttp
from aiohttp.client_exceptions import ClientError

#: Success HTTP status code from Loki API.
SUCCESS_RESPONSE_CODE = 204

# these characters will be replaced within the labels
REPLACED_CHARACTERS = re.compile(r"\s|-|\.")
REPLACED_BY = "_"
# these characters are the ones, that won't be removed from labels
PERSISTENT_CHARACTERS = re.compile(r"[^a-zA-Z_]")

SESSION_TIMEOUT = aiohttp.ClientTimeout(total=1.0)


class LokiEmitError(BaseException):
    """Error dispatching log to Loki."""


class LokiAsyncEmitter:
    """Base Loki emitter class."""

    basic_auth: aiohttp.BasicAuth = None

    def __init__(self, url: str, tags: dict = None, auth: Tuple = None) -> None:
        """Create new Loki emitter.

        Args:
            url: Endpoint used to send log entries to Loki.
            tags: Default tags added to every log record.
            auth: Optional tuple with username and password for basic HTTP
                  authentication.
        """
        #: Tags that will be added to all records handled by this handler.
        self.tags = dict(tags) if isinstance(tags, ConvertingDict) else tags or {}
        #: Loki JSON push endpoint
        self.url = url
        #: Optional tuple with username and password for basic authentication.
        if auth:
            self.basic_auth = aiohttp.BasicAuth(
                login=auth[0],
                password=auth[1],
            )

        self._reconnects = 0
        self._session = None

    async def __call__(
        self, log_message: str, record: logging.LogRecord, raise_exc: bool = False
    ) -> None:
        """Send log record to Loki.

        Args:
            log_message (str): Log message.
            record (logging.LogRecord): The log record
            raise_exc (bool, optional): Raise an exception. Defaults to False.

        Raises:
            LokiEmitError: Raised, only when raise_exc is set.
        """
        payload = self.build_payload(record=record, log_message=log_message)
        try:
            async with self.session.post(self.url, json=payload) as response:
                if response.status != SUCCESS_RESPONSE_CODE:
                    raise ValueError(
                        f"Unexpected Loki API response status code: {response.status}"
                    )
        except (ClientError, RuntimeError) as exc:
            sys.stderr.write(f"Loki: Cannot connect to the Loki endpoint.\n{exc}\n")
            if raise_exc:
                raise LokiEmitError from exc
        except ValueError as exc:
            sys.stderr.write(f"{exc}\n")
            if raise_exc:
                raise LokiEmitError from exc
        except TimeoutError as exc:
            sys.stderr.write(f"Loki: Timeout after {SESSION_TIMEOUT.total} seconds\n")
            if raise_exc:
                raise LokiEmitError from exc
        except Exception as exc:  # pylint: disable=broad-except
            sys.stderr.write(f"Loki: Unknown error\n{exc}\n")
            if raise_exc:
                raise LokiEmitError from exc

    def build_payload(self, record: logging.LogRecord, log_message: str) -> dict:
        """Build JSON payload with a log entry.

        Args:
            record: The LogRecord to send to Loki.
            log_message: The formatted log message.

        Returns:
            dict: A build log payload.
        """
        labels = self.build_tags(record)
        nano_seconds_multiplier = 1e9
        time_string = str(int(record.created * nano_seconds_multiplier))
        stream = {
            "stream": labels,
            "values": [[time_string, log_message]],
        }
        return {"streams": [stream]}

    @property
    def session(self) -> aiohttp.ClientSession:
        """Create the HTTP session."""
        if self._session is None:
            self._session = aiohttp.ClientSession(
                auth=self.basic_auth, timeout=SESSION_TIMEOUT
            )
        return self._session

    def close(self) -> None:
        """Close the HTTP session."""
        if self._session is not None:
            asyncio.create_task(self._session.close())
            self._session = None

    def format_label(self, label: str) -> str:
        """Build label to match prometheus format.

        Label format:
        https://prometheus.io/docs/concepts/data_model/#measurement-names-and-labels

        Args:
            label: The label to format.

        Returns:
            str: A formatted label.
        """
        label = REPLACED_CHARACTERS.sub(REPLACED_BY, label)
        label = PERSISTENT_CHARACTERS.sub("", label)
        return label

    def build_tags(self, record: logging.LogRecord) -> dict:
        """Return tags that must be sent to Loki with a log record.

        Extends the default tags by tags sent with the record.

        Args:
            record: The LogRecord to add the tags from.

        Returns:
            dict: A complete, sanitized dict to send to Loki.
        """
        tags = self.tags.copy()

        tags["level"] = record.levelname.lower()
        tags["levelno"] = record.levelno

        extra_tags = getattr(record, "tags", {})
        if not isinstance(extra_tags, dict):
            return tags

        for tag_name, tag_value in extra_tags.items():
            if not isinstance(tag_value, str):
                continue
            if cleared_name := self.format_label(tag_name):
                tags[cleared_name] = tag_value

        return tags
