# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""A handler for the Python logging module to integrate into Grafana Loki.

Unofficial fork from:
https://github.com/cuerty/loki-handler
https://pypi.org/project/loki-handler/
"""
