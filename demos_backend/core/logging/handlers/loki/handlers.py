# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains a handler for Grafana Loki."""
from __future__ import annotations

import asyncio
import logging
import sys

from .emitter import LokiAsyncEmitter


class LokiHandler(logging.Handler):
    """Log handler that sends log records to Loki.

    `Loki API <https://github.com/grafana/loki/blob/master/docs/api.md>`_
    """

    def __init__(self, url: str, tags: dict = None, auth: tuple = None) -> None:
        """Create new Loki logging handler.

        Args:
            url: Endpoint used to send log entries to Loki.
            tags: Default tags added to every log record.
            auth: Optional tuple with username and password for basic HTTP
                  authentication.
        """
        super().__init__()
        self.emitter = LokiAsyncEmitter(url=url, tags=tags, auth=auth)

    def handleError(self, record: logging.LogRecord) -> None:
        """Close emitter and let default handler take actions on error."""
        self.emitter.close()
        super().handleError(record)

    def emit(self, record: logging.LogRecord) -> None:
        """Send log record to Loki."""
        # noinspection PyBroadException
        try:
            loop = asyncio.get_running_loop()
            loop.create_task(self.emitter(self.format(record), record=record))
        except RuntimeError:
            sys.stderr.write(
                f"{self.__class__.__name__} cannot dispatch log, "
                f"no running event loop.\n"
            )
