# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the logger of the backend."""
from __future__ import annotations

import logging
from enum import IntEnum
from logging.handlers import TimedRotatingFileHandler
from typing import TYPE_CHECKING

from demos_backend.core.logging.handlers.loki.handlers import LokiHandler

if TYPE_CHECKING:
    from demos_backend.core.configuration import Configuration


LOG_FORMAT = "%(asctime)s [%(levelname)-8s] %(module)s.%(funcName)s(): %(message)s"


class SupportedLogLevels(IntEnum):
    """An Enum representing supported log levels."""

    CRITICAL = logging.CRITICAL
    ERROR = logging.ERROR
    WARNING = logging.WARNING
    INFO = logging.INFO
    DEBUG = logging.DEBUG
    NOTSET = logging.NOTSET


def prepare_logger(service_name: str, config: "Configuration") -> None:
    """Prepare the root logger for further usage.

    Handlers will be added and log level and log format will be set.

    Args:
        service_name: The name of the service currently running.
        config: A Configuration for connection details.
    """
    handlers = []
    file_handler = TimedRotatingFileHandler(
        filename=config.LOG_LOCATION.format(service_name=service_name),
        when="midnight",
        backupCount=7,
    )
    file_handler.setLevel(config.LOG_LEVEL)
    handlers.append(file_handler)

    if config.LOG_LOKI_ENABLE:
        loki_handler = LokiHandler(
            url=config.LOG_LOKI_URL,
            tags={"service": service_name, "environment": config.ENVIRONMENT},
            auth=(config.LOG_LOKI_USERNAME, config.LOG_LOKI_PASSWORD),
        )
        loki_handler.setFormatter(logging.Formatter("%(message)s"))
        loki_handler.setLevel(config.LOG_LOKI_LEVEL)
        handlers.append(loki_handler)

    logging.basicConfig(
        format=LOG_FORMAT,
    )
    logging.root.setLevel(config.LOG_LEVEL)
    logging.root.handlers += handlers
