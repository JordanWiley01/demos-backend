# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Provides a prometheus base class for monitoring."""


class PrometheusBaseMetrics:
    """Base class for others to inherit from."""

    prefix = "demos_"
