# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains functionalities to add prometheus monitoring to FastAPI."""

from __future__ import annotations

from fastapi import FastAPI
from starlette_exporter import PrometheusMiddleware, handle_metrics

from demos_backend.core.configuration import Configuration


def add_endpoint(app_name: str, app: FastAPI, config: Configuration):
    """Add a prometheus monitoring endpoint to a FastAPI application.

    Args:
        app_name: The name of the monitored application for sorting within prometheus.
        app: The application to add the endpoint to.
        config: A configuration for reading certain values.
    """
    app.add_middleware(
        PrometheusMiddleware,
        app_name=app_name,
        prefix="demos",
        group_paths=True,
        filter_unhandled_paths=True,
        labels={"service": app_name},
        buckets=[
            0.01,
            0.025,
            0.05,
            0.075,
            0.1,
            0.25,
            0.5,
            0.75,
            1.0,
            2.5,
            5.0,
            7.5,
            10.0,
        ],
    )

    app.add_route(config.METRICS_PROMETHEUS_ENDPOINT, handle_metrics)
