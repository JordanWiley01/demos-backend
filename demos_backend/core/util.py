# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module containing utility functions."""
from __future__ import annotations

import secrets

from deep_translator import GoogleTranslator
from fastapi import FastAPI
from fastapi.routing import APIRoute
from geopy.geocoders import Nominatim
from geopy.location import Location as GeoPyLocation
from geopy.point import Point

from demos_backend.core.models.location import Location
from demos_backend.core.schemas.event import EventAddressSearch
from demos_backend.core.schemas.information import InformationCreate, InformationQuery
from demos_backend.core.schemas.location import (
    LocationCreate,
    LocationCreateCrawler,
    LocationUpdate,
)
from demos_backend.core.schemas.organisation import OrganisationAddressSearch
from demos_backend.core.types import AvailableLanguages


# pylint: disable=R0912
def get_geolocation_from_location_create(
    location_create: LocationCreate,
) -> LocationCreate:
    """Enrich LocationCreate schema with additional data.

    Args:
        location_create: The schema to enrich.

    Returns:
        LocationCreate: The enriched schema.
    """
    geolocator = Nominatim(user_agent="demonstrations.org")

    # Nominatim style query dict
    # see https://nominatim.org/release-docs/develop/api/Search/

    if location_create.latitude and location_create.longitude:
        city = "unknown"
        street = "unknown"
        zip_code = "unknown"
        house_number = "unknown"
        country = "unknown"
        point = Point(
            latitude=location_create.latitude, longitude=location_create.longitude
        )
        geo_location: GeoPyLocation = None
        try:
            geo_location: GeoPyLocation = geolocator.reverse(
                point, exactly_one=True, language="en"
            )
        # pylint: disable=W0703
        except Exception:
            pass
        if geo_location:
            if "address" in geo_location.raw:
                address_dict = geo_location.raw["address"]
                if "city" in address_dict:
                    city = address_dict["city"]
                if "road" in address_dict:
                    street = address_dict["road"]
                if "postcode" in address_dict:
                    zip_code = address_dict["postcode"]
                if "house_number" in address_dict:
                    house_number = address_dict["house_number"]
                if "country" in address_dict:
                    country = address_dict["country"]
        return LocationCreate(
            city=city,
            street=street,
            zip_code=zip_code,
            house_number=house_number,
            country=country,
            latitude=location_create.latitude,
            longitude=location_create.longitude,
        )
    query_dict = {}
    if location_create.country:
        query_dict["country"] = location_create.country
    if location_create.city:
        query_dict["city"] = location_create.city
    if location_create.zip_code:
        query_dict["postalcode"] = location_create.zip_code
    if location_create.street:
        query_dict[
            "street"
        ] = f"{location_create.house_number} {location_create.street}"

    if len(query_dict) < 2:
        raise RuntimeError("Insufficient Data for geolocation.")

    geo_location: GeoPyLocation = None
    try:
        geo_location: GeoPyLocation = geolocator.geocode(query_dict, exactly_one=True)
    # pylint: disable=W0703
    except Exception:
        pass
    if geo_location:
        return LocationCreate(
            city=location_create.city,
            street=location_create.street,
            zip_code=location_create.zip_code,
            house_number=location_create.house_number,
            country=location_create.country,
            latitude=geo_location.latitude,
            longitude=geo_location.longitude,
        )
    location_create.latitude = 0
    location_create.longitude = 0
    return location_create


# pylint: disable=R0912
def get_geolocation_from_location_update(
    location_update: LocationUpdate,
) -> LocationUpdate:
    """Enrich LocationUpdate schema with additional data.

    Args:
        location_update: The schema to enrich.

    Returns:
        LocationUpdate: The enriched schema.
    """
    geolocator = Nominatim(user_agent="demonstrations.org")

    # Nominatim style query dict
    # see https://nominatim.org/release-docs/develop/api/Search/

    query_dict = {}
    if location_update.country:
        query_dict["country"] = location_update.country
    if location_update.city:
        query_dict["city"] = location_update.city
    if location_update.zip_code:
        query_dict["postalcode"] = location_update.zip_code
    if location_update.street:
        query_dict[
            "street"
        ] = f"{location_update.house_number} {location_update.street}"

    if len(query_dict) < 2:
        raise RuntimeError("Insufficient Data for geolocation.")

    geo_location: GeoPyLocation = None
    try:
        geo_location: GeoPyLocation = geolocator.geocode(query_dict, exactly_one=True)
    # pylint: disable=W0703
    except Exception:
        pass
    if geo_location:
        return LocationUpdate(
            city=location_update.city,
            street=location_update.street,
            zip_code=location_update.zip_code,
            house_number=location_update.house_number,
            country=location_update.country,
            latitude=geo_location.latitude,
            longitude=geo_location.longitude,
        )
    location_update.latitude = 0
    location_update.longitude = 0
    return location_update


# pylint: disable=R0912,R0915
def get_geolocation_from_crawler_location_create(
    location_create_crawler: LocationCreateCrawler,
) -> LocationCreate:
    """Enrich LocationCreateCrawler schema with additional data.

    Args:
        location_create_crawler: The schema to enrich.

    Returns:
        LocationCreateCrawler: The enriched schema.
    """
    geolocator = Nominatim(user_agent="demonstrations.org")

    # Nominatim style query dict
    # see https://nominatim.org/release-docs/develop/api/Search/

    query_dict = {}
    latitude = 0
    longitude = 0
    if location_create_crawler.street:
        query_dict["street"] = location_create_crawler.street
        street = location_create_crawler.street
    else:
        street = "unknown"
    if location_create_crawler.city:
        query_dict["city"] = location_create_crawler.city
        city = location_create_crawler.city
    else:
        city = "unknown"
    if location_create_crawler.country:
        query_dict["country"] = location_create_crawler.country
        country = location_create_crawler.country
    else:
        country = "unknown"
    if location_create_crawler.zip_code:
        query_dict["postalcode"] = location_create_crawler.zip_code
        zip_code = location_create_crawler.zip_code
    else:
        zip_code = "unknown"

    house_number = "n/a"

    try:
        geo_location: GeoPyLocation = geolocator.geocode(query_dict, exactly_one=True)
        if geo_location.latitude is not None:
            latitude = geo_location.latitude
        if geo_location.longitude is not None:
            longitude = geo_location.longitude
    # pylint: disable=W0703
    except Exception:
        pass

    if latitude == 0 and longitude == 0 and location_create_crawler.city:
        try:
            geo_location: GeoPyLocation = geolocator.geocode(
                {"city": location_create_crawler.city}, exactly_one=True
            )
            if geo_location.latitude is not None:
                latitude = geo_location.latitude
            if geo_location.longitude is not None:
                longitude = geo_location.longitude
        # pylint: disable=W0703
        except Exception:
            pass

    return LocationCreate(
        city=city,
        street=street,
        zip_code=zip_code,
        house_number=house_number,
        country=country,
        latitude=latitude,
        longitude=longitude,
    )


def get_random_uid(length: int = 16) -> str:
    """Generate a random UID.

    Args:
        length (int): Length of the UID. [default: 16]

    Returns:
        str: A randomly generated UID.
    """
    return secrets.token_hex(length)


def translate_text(
    text: str,
    target_language: AvailableLanguages,
    src_language: AvailableLanguages = "auto",
) -> str:
    """Translate text.

    Args:
        text: The text to translate.
        src_language: The language of the input data.
        target_language: The language the data should be translated to.

    Returns:
        str: The translated text.
    """
    translated = GoogleTranslator(
        source=src_language, target=target_language
    ).translate(text)
    if not translated:
        return text
    return translated


def translate_location_model(
    model_in: Location,
    target_language: AvailableLanguages,
    src_language: AvailableLanguages = "en",
) -> Location:
    """Translate location names of an Location.

    Args:
        model_in: The data to translate.
        src_language: The language of the input data.
        target_language: The language the data should be translated to.

    Returns:
        Location: The translated query.
    """
    model_in.country = translate_text(
        src_language=src_language,
        target_language=target_language,
        text=model_in.country,
    )
    model_in.city = translate_text(
        src_language=src_language, target_language=target_language, text=model_in.city
    )
    return model_in


def translate_location_create(
    schema_in: LocationCreate | LocationCreateCrawler,
    src_language: AvailableLanguages,
    target_language: AvailableLanguages = AvailableLanguages.ENGLISH,
) -> LocationCreate | LocationCreateCrawler:
    """Translate location names of an LocationCreate.

    Args:
        schema_in: The data to translate.
        src_language: The language of the input data.
        target_language: The language the data should be translated to.

    Returns:
        LocationCreate: The translated query.
    """
    schema_in.country = translate_text(
        src_language=src_language,
        target_language=target_language,
        text=schema_in.country,
    )
    schema_in.city = translate_text(
        src_language=src_language, target_language=target_language, text=schema_in.city
    )
    return schema_in


def translate_location_update(
    schema_in: LocationUpdate,
    src_language: AvailableLanguages,
    target_language: AvailableLanguages = AvailableLanguages.ENGLISH,
) -> LocationUpdate:
    """Translate location names of an LocationCreate.

    Args:
        schema_in: The data to translate.
        src_language: The language of the input data.
        target_language: The language the data should be translated to.

    Returns:
        LocationCreate: The translated query.
    """
    schema_in.country = translate_text(
        src_language=src_language,
        target_language=target_language,
        text=schema_in.country,
    )
    schema_in.city = translate_text(
        src_language=src_language, target_language=target_language, text=schema_in.city
    )
    return schema_in


def translate_event_address_search(
    schema_in: EventAddressSearch | OrganisationAddressSearch,
    src_language: AvailableLanguages,
    target_language: AvailableLanguages = AvailableLanguages.ENGLISH,
) -> EventAddressSearch | OrganisationAddressSearch:
    """Translate location names of an *AddressSearch.

    Args:
        schema_in: The data to translate.
        src_language: The language of the input data.
        target_language: The language the data should be translated to.

    Returns:
        EventAddressSearch | OrganisationAddressSearch: The translated query.
    """
    schema_in.country = translate_text(
        src_language=src_language,
        target_language=target_language,
        text=schema_in.country,
    )
    schema_in.city = translate_text(
        src_language=src_language, target_language=target_language, text=schema_in.city
    )
    return schema_in


def translate_information_create(
    schema_in: InformationCreate,
    src_language: AvailableLanguages,
    target_language: AvailableLanguages = AvailableLanguages.ENGLISH,
) -> InformationCreate:
    """Translate location names of an InformationCreate.

    Args:
        schema_in: The data to translate.
        src_language: The language of the input data.
        target_language: The language the data should be translated to.

    Returns:
        InformationCreate: The translated query.
    """
    schema_in.country = translate_text(
        src_language=src_language,
        target_language=target_language,
        text=schema_in.country,
    )
    schema_in.city = translate_text(
        src_language=src_language, target_language=target_language, text=schema_in.city
    )
    return schema_in


def translate_information_query(
    schema_in: InformationQuery,
    src_language: AvailableLanguages,
    target_language: AvailableLanguages = AvailableLanguages.ENGLISH,
) -> InformationQuery:
    """Translate location names of an InformationQuery.

    Args:
        schema_in: The data to translate.
        src_language: The language of the input data.
        target_language: The language the query data should be translated to.

    Returns:
        InformationQuery: The translated query.
    """
    if schema_in.country:
        schema_in.country = translate_text(
            src_language=src_language,
            target_language=target_language,
            text=schema_in.country,
        )
    if schema_in.city:
        schema_in.city = translate_text(
            src_language=src_language,
            target_language=target_language,
            text=schema_in.city,
        )
    return schema_in


def use_route_names_as_operation_ids(app: FastAPI) -> None:
    """Simplify operation IDs so that generated API clients have simpler function names.

    Should be called only after all routes have been added.
    """
    for route in app.routes:
        if isinstance(route, APIRoute):
            route.operation_id = route.name
