# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the shared configuration class for all services."""
from __future__ import annotations

import logging
import os
import secrets
from enum import Enum
from pathlib import Path
from typing import Any

from pydantic import (
    AnyHttpUrl,
    BaseSettings,
    EmailStr,
    Field,
    conint,
    constr,
    validator,
)

from demos_backend.core.logging import SupportedLogLevels

logger = logging.getLogger("configuration")

DEFAULT_CONFIG_FILE_LOCATION = Path(f"{Path.home()}/.config/demos/backend.env")
_LOADED_CONFIG = None


class Environment(str, Enum):
    """Possible application environments."""

    TESTING = "testing"
    DEVELOPMENT = "development"
    STAGING = "staging"
    PRODUCTION = "production"


class Configuration(BaseSettings):
    """Configuration, for the Demos server backend."""

    ENVIRONMENT: Environment = Environment.DEVELOPMENT

    # log vars
    LOG_LEVEL: SupportedLogLevels = SupportedLogLevels.DEBUG
    LOG_LOCATION: str = "demos.{service_name}.log"
    LOG_LOKI_ENABLE: bool = True
    LOG_LOKI_URL: AnyHttpUrl = "http://loki:3100/loki/v1/push"
    LOG_LOKI_USERNAME: constr(min_length=1) = "test"
    LOG_LOKI_PASSWORD: constr(min_length=1) = "test"
    LOG_LOKI_LEVEL: SupportedLogLevels = SupportedLogLevels.DEBUG

    # monitoring vars
    METRICS_PROMETHEUS_ENDPOINT: str = "/metrics/prometheus"

    # db config
    MONGODB_HOSTNAME: str = "mongodb"
    MONGODB_PORT: int = 27017
    MONGODB_USERNAME: str = "test"
    MONGODB_PASSWORD: str = "test"

    MARIADB_HOSTNAME: str = "mariadb"
    MARIADB_PORT: int = 3306
    MARIADB_USERNAME: str = "test"
    MARIADB_PASSWORD: str = "test"
    MARIADB_DATABASE: str = "demosdb"

    DEFAULT_USER_MAIL: EmailStr = "admin@demonstrations.org"
    DEFAULT_USER_NAME: str = "admin"
    DEFAULT_USER_PASSWORD: constr(min_length=16) = "ottoistderbossbeidemos"
    CRAWLER_USER_MAIL: EmailStr = "crawler@demonstrations.org"
    CRAWLER_USER_NAME: str = "crawler"
    CRAWLER_USER_PASSWORD: constr(min_length=16) = "wircrawlendieganzenacht"

    # shared api config
    API_V1_ROOT_PATH: str = "/v1"
    API_ACCESS_TOKEN_EXPIRE_MINUTES: int = 60
    API_SECRET_KEY: str = Field(default_factory=lambda: secrets.token_urlsafe(32))

    # frontend_api config
    FRONTEND_API_PORT: int = 3080
    FRONTEND_API_HOSTNAME: str = "frontend-api"
    FRONTEND_BASE_DOMAIN: str = "http://localhost:3000"

    # crawler_api config
    CRAWLER_API_PORT: int = 5080
    CRAWLER_API_HOSTNAME: str = "crawler-api"

    # notification service config
    NOTIFICATION_SERVICE_API_V1_ROOT_PATH: str = "/v1"
    NOTIFICATION_SERVICE_HOSTNAME: str = "notification-service"
    NOTIFICATION_SERVICE_PORT: int = 4080
    NOTIFICATION_SERVICE_MAIL_USERNAME: str = "your_username"
    NOTIFICATION_SERVICE_MAIL_PASSWORD: str = "strong_password"
    NOTIFICATION_SERVICE_MAIL_PORT = 1025
    NOTIFICATION_SERVICE_MAIL_HOSTNAME: str = "mailhog"
    NOTIFICATION_SERVICE_MAIL_STARTTLS: bool = False
    NOTIFICATION_SERVICE_MAIL_SSL_TLS: bool = False
    NOTIFICATION_SERVICE_MAIL_USE_CREDENTIALS: bool = True
    NOTIFICATION_SERVICE_MAIL_VALIDATE_CERTS: bool = False
    NOTIFICATION_SERVICE_MAIL_SENDER_MAIL: EmailStr = "your@email.com"
    NOTIFICATION_SERVICE_MAIL_SENDER_NAME: str = "Demos"

    # verification config
    USER_VERIFICATION_TOKEN_EXPIRE_MINUTES: int = 60
    PASSWORD_TOKEN_EXPIRE_MINUTES: int = 15

    # crawler config
    CRAWLER_EVENT_TYPE_ID: conint(ge=1) = 1
    CRAWLER_EVENT_TOPIC_ID: conint(ge=1) = 1
    CRAWLER_STORAGE_CHUNK_SIZE: conint(ge=1) = 50
    CRAWLER_MAX_CHUNKS_TO_SEND: conint(ge=-1) = -1
    CRAWLER_SPIDERS: str = "Berlin,Dresden,Rostock,Lüneburg,Landsberg am Lech"
    CRAWLER_SPIDERS_PARSED: list[str] | None = None
    CRAWLER_ORGANISATION_IDS: str = "1,2,3,4,5"
    CRAWLER_ORGANISATION_IDS_PARSED: list[int] | None = None
    CRAWLER_INTERVAL_MINUTES: int = 60

    @validator("CRAWLER_SPIDERS_PARSED", pre=True)
    def assemble_crawlers(cls, v: list | None, values: dict[str, Any]) -> list[str]:
        """Parse the spider string from config."""
        spider_str = values.get("CRAWLER_SPIDERS")
        if not isinstance(spider_str, str):
            return []
        return spider_str.split(",")

    @validator("CRAWLER_ORGANISATION_IDS_PARSED", pre=True)
    def assemble_organisation_ids(
        cls, v: list | None, values: dict[str, Any]
    ) -> list[int]:
        """Parse the organisation string from config."""
        organisation_str = values.get("CRAWLER_ORGANISATION_IDS")
        if not isinstance(organisation_str, str):
            return []
        return [int(value) for value in organisation_str.split(",")]

    class Config:
        """Configuration for configurations."""

        # Loaded environment variables need to have this prefix to be loaded correctly
        # e.g. `export DEMOS_MONGODB_HOSTNAME="localhost"`
        # instead of `export MONGODB_HOSTNAME="localhost"`
        env_prefix = "DEMOS_"
        use_enum_values = True

    # additional functionalities
    def get_env(self) -> str:
        """Print the current config in .env style."""
        config_dict = self.__dict__

        config_str = ""

        for key, value in config_dict.items():
            if key in ["CRAWLER_SPIDERS_PARSED", "CRAWLER_ORGANISATION_IDS_PARSED"]:
                continue
            value = str(value)
            if "," in value:
                value = f'"{value}"'
            config_str += f"{self.Config.env_prefix}{key}={value}\n"

        return config_str

    def write_config_to_file(self, file: Path | None = None):
        """Write the current config to the default config file location."""
        if not file:
            file = Path(DEFAULT_CONFIG_FILE_LOCATION)
        with open(file, mode="w", encoding="utf-8") as config_file:
            config_file.write(self.get_env())


def get_config() -> Configuration:
    """Get a set-up config object.

    Returns:
        Configuration: The initialized configuration.
    """
    global _LOADED_CONFIG  # pylint: disable=W0603
    if not _LOADED_CONFIG:
        dir_path = Path(os.path.dirname(DEFAULT_CONFIG_FILE_LOCATION))
        dir_path.mkdir(exist_ok=True, parents=True)
        if DEFAULT_CONFIG_FILE_LOCATION.is_file():
            config = Configuration(_env_file=DEFAULT_CONFIG_FILE_LOCATION)
        else:
            config = Configuration()

        _LOADED_CONFIG = config

    return _LOADED_CONFIG
