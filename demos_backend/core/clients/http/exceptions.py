# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Contains exceptions, that occur during communication with other services."""
import json
import logging
from abc import ABC, abstractmethod
from json import JSONDecodeError


class HTTPClientError(ConnectionError, ABC):
    """Exception for others to derive from."""

    # pylint: disable=W0231
    @abstractmethod
    def __init__(self, method: str, resource: str, status: int, text: str) -> None:
        """Create an instance of the class."""


class ServiceError(HTTPClientError):
    """An exception to raise, if the communication with the service fails."""

    service_message: str
    status: int

    # pylint: disable=W0231
    def __init__(self, method: str, resource: str, status: int, text: str) -> None:
        """Create an instance of the class."""
        logging.error(
            "Service client encountered an error: \n%s\n%s\n%s\n%s",
            method,
            resource,
            status,
            text,
        )
        try:
            service_message = json.loads(text)["detail"]
        except (JSONDecodeError, KeyError):
            service_message = text

        self.database_message = service_message
        self.status = status
