# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the notification service client V1."""
from __future__ import annotations

from typing import TYPE_CHECKING

from demos_backend.core.clients.http import HTTPClient

if TYPE_CHECKING:
    from demos_backend.core.schemas.notification_request import (
        UserNotificationRequest,
        UserResetPasswordRequest,
    )


class NotificationServiceClient(HTTPClient):
    """A client class for handling networks with the database API."""

    async def send_user_verification_mail(
        self, request: UserNotificationRequest
    ) -> None:
        """Send a list of users a mail for account verification.

        Args:
            request: The information needed to fulfill the request.

        Raises:
            ServiceError: If the request cannot be fulfilled.
        """
        await self._post(
            resource="/mail/user_verification",
            payload=request,
            return_type=dict,
        )

    async def send_user_password_reset_mail(
        self, request: UserResetPasswordRequest
    ) -> None:
        """Send a list of users a mail for account verification.

        Args:
            request: The information needed to fulfill the request.

        Raises:
            ServiceError: If the request cannot be fulfilled.
        """
        await self._post(
            resource="/mail/reset_password",
            payload=request,
            return_type=dict,
        )
