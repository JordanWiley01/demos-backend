# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the frontend api client V1."""
from __future__ import annotations

from demos_backend.core.clients.http import HTTPClient
from demos_backend.core.schemas.token import Token


class FrontendApiClient(HTTPClient):
    """A client class for handling communication with the API."""

    token: str

    async def get_token(self, username: str, password: str) -> Token:
        """Send credentials to the APi to authenticate.

        Args:
            username: The username to auth with.
            password: The password to auth with.

        Raises:
            ServiceException: If the request cannot be fulfilled.
        """
        # payload = f"username={username}&password={password}"
        payload = (
            f"grant_type=&username={username}&password={password}&scope=&client_secret="
        )
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        token: Token = await self._post(
            resource="/user/get_token",
            payload=payload,
            return_type=Token,
            headers=headers,
        )
        self.token = token.access_token
        return token
