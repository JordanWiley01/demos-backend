# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the database client V1."""
from __future__ import annotations

import json
import logging

import aiohttp
from pydantic.json import pydantic_encoder

from demos_backend.core.clients.http.exceptions import ServiceError
from demos_backend.core.clients.http.services.crawler_api import (
    CrawlerApiClient,
)
from demos_backend.core.clients.http.services.frontend_api import FrontendApiClient
from demos_backend.core.clients.http.services.notification import (
    NotificationServiceClient,
)
from demos_backend.core.configuration import Configuration


class ServiceClient:
    """A client class for communicating with other services."""

    _config: Configuration
    _session: aiohttp.ClientSession
    frontend_api: FrontendApiClient
    notification_service: NotificationServiceClient
    crawler_api: CrawlerApiClient

    def __init__(self, config: Configuration) -> None:
        """Create an instance of the class.

        Args:
            config: A configuration set with information where and how to connect to.
        """
        self._config = config

        self._session = aiohttp.ClientSession(
            json_serialize=lambda v: json.dumps(v, default=pydantic_encoder),
        )

        self.frontend_api = FrontendApiClient(
            api_base_url=f"http://{config.FRONTEND_API_HOSTNAME}:"
            f"{config.FRONTEND_API_PORT}/v1",
            session=self._session,
            exception=ServiceError,
            basic_auth=aiohttp.BasicAuth(login="notset"),
        )

        self.notification_service = NotificationServiceClient(
            api_base_url=f"http://{config.NOTIFICATION_SERVICE_HOSTNAME}:"
            f"{config.NOTIFICATION_SERVICE_PORT}/v1",
            session=self._session,
            exception=ServiceError,
            basic_auth=aiohttp.BasicAuth(login="notset"),
        )

        self.crawler_api = CrawlerApiClient(
            api_base_url=f"http://{config.CRAWLER_API_HOSTNAME}:"
            f"{config.CRAWLER_API_PORT}/v1",
            session=self._session,
            exception=ServiceError,
            basic_auth=aiohttp.BasicAuth(login="notset"),
        )

        logging.debug("Service Client created.")

    async def prepare_deletion(self):
        """Tasks to do when stopping."""
        # gracefully close the aiohttp session
        logging.debug("Closing Service Client.")
        await self._session.close()
