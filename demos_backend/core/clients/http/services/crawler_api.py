# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains the crawler api client V1."""
from __future__ import annotations

from demos_backend.core.clients.http import HTTPClient
from demos_backend.core.schemas.event import EventCreate
from demos_backend.core.schemas.message import Message
from demos_backend.core.schemas.token import Token


class CrawlerApiClient(HTTPClient):
    """A client class for handling communication with the API."""

    token: str

    async def get_token(self, username: str, password: str) -> Token:
        """Send credentials to the APi to authenticate.

        Args:
            username: The username to auth with.
            password: The password to auth with.

        Raises:
            ServiceException: If the request cannot be fulfilled.
        """
        # payload = f"username={username}&password={password}"
        payload = (
            f"grant_type=&username={username}&password={password}&scope=&client_secret="
        )
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        token: Token = await self._post(
            resource="/user/get_token",
            payload=payload,
            return_type=Token,
            headers=headers,
        )
        self.token = token.access_token
        return token

    async def send_new_crawler_events(self, request: list[EventCreate]) -> None:
        """Send a list of events to create.

        Args:
            request: The information needed to fulfill the request.

        Raises:
            ServiceException: If the request cannot be fulfilled.
        """
        headers = {"Authorization": f"Bearer {self.token}"}
        await self._post(
            resource="/event/crawler/new",
            payload=request,
            return_type=list,
            headers=headers,
        )

    async def cleanup_organisation_events(self, organisation_id: int) -> None:
        """Clean up an organisations events.

        Args:
            organisation_id: The organisation to clean up.

        Raises:
            ServiceException: If the request cannot be fulfilled.
        """
        headers = {"Authorization": f"Bearer {self.token}"}
        await self._post(
            resource=f"/event/cleanup/{organisation_id}",
            payload=None,
            return_type=Message,
            headers=headers,
        )
