# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""This module contains a base class for API clients."""
from __future__ import annotations

import logging
from typing import Type

import aiohttp
from aiohttp import ContentTypeError
from pydantic import BaseModel

from demos_backend.core.clients.http.exceptions import HTTPClientError


class HTTPClient:
    """A client class for handling HTTP requests."""

    _db_api_base_url: str
    _basic_auth: aiohttp.BasicAuth
    _session: aiohttp.ClientSession
    _exception: Type[HTTPClientError]

    def __init__(
        self,
        api_base_url: str,
        basic_auth: aiohttp.BasicAuth,
        session: aiohttp.ClientSession,
        exception: Type[HTTPClientError],
    ) -> None:
        """Create an instance of the class.

        Args:
            api_base_url: The base URL of the database manager.
            basic_auth: The auth for the database manager.
            session: A shared client session for communication.
            exception: The exception to raise when something goes wrong.
        """
        self._db_api_base_url = api_base_url
        self._basic_auth = basic_auth
        self._session = session
        self._exception = exception

        logging.debug("Users Client created.")

    async def _get(
        self,
        resource: str,
        return_type: Type[BaseModel | list | dict | str],
        get_parameters: dict = None,
    ) -> BaseModel | list | dict | str:
        """Make a generic GET request to the API.

        Args:
            resource: The resource to request (with starting slash).
            get_parameters: The GET parameter to send with the request.
            return_type: The type that is expected to be returned.

        Returns:
            return_type: The data parsed as `return_type`.
        """
        logging.debug("Database client is getting %s.", resource)

        get_parameters = get_parameters or {}
        url = f"{self._db_api_base_url}{resource}"

        async with self._session.get(url=url, params=get_parameters) as response:
            if response.status == 200:
                try:
                    response_data = await response.json()
                    if isinstance(response_data, return_type):
                        return response_data
                    return return_type.parse_obj(response_data)
                except ContentTypeError as e:
                    raise self._exception(
                        method="GET",
                        resource=resource,
                        status=response.status,
                        text=await response.text(),
                    ) from e
            else:
                raise self._exception(
                    method="GET",
                    resource=resource,
                    status=response.status,
                    text=await response.text(),
                )

    async def _post(
        self,
        resource: str,
        payload: BaseModel | list[BaseModel] | dict | str | None,
        return_type: Type[BaseModel | list | dict],
        get_parameters: dict = None,
        headers: dict = None,
    ) -> BaseModel | list | dict:
        """Make a generic POST request to the API.

        Args:
            resource: The resource to create (with starting slash).
            payload: The data to send.
            get_parameters: The GET parameter to send with the request.
            return_type: The type that is expected to be returned.
            headers: The headers to send.

        Returns:
            return_type: The data parsed as `return_type`.
        """
        logging.debug("Database client is POSTing %s.", resource)

        if get_parameters is None:
            get_parameters = {}
        url = f"{self._db_api_base_url}{resource}"

        data_payload = None
        json_payload = None

        if isinstance(payload, str):
            data_payload = payload
        elif isinstance(payload, list):
            json_payload = [payload.dict() for payload in payload]
        elif isinstance(payload, BaseModel):
            json_payload = payload.dict()
        else:
            json_payload = payload

        async with self._session.post(
            url=url,
            json=json_payload,
            params=get_parameters,
            headers=headers,
            data=data_payload,
        ) as response:
            if response.status == 200:
                try:
                    response_data = await response.json()
                    if isinstance(response_data, return_type):
                        return response_data
                    return return_type.parse_obj(response_data)
                except ContentTypeError as e:
                    raise self._exception(
                        method="POST",
                        resource=resource,
                        status=response.status,
                        text=await response.text(),
                    ) from e
            else:
                raise self._exception(
                    method="POST",
                    resource=resource,
                    status=response.status,
                    text=await response.text(),
                )

    async def _put(
        self,
        resource: str,
        payload: BaseModel | dict,
        return_type: Type[BaseModel | list | dict],
        get_parameters: dict = None,
    ) -> BaseModel | list | dict | str:
        """Make a generic PUT request to the API.

        Args:
            resource: The resource to update (with starting slash).
            payload: The data to send.
            get_parameters: The GET parameter to send with the request.
            return_type: The type that is expected to be returned.

        Returns:
            return_type: The data parsed as `return_type`.
        """
        logging.debug("Database client is putting %s.", resource)

        if get_parameters is None:
            get_parameters = {}
        url = f"{self._db_api_base_url}{resource}"

        async with self._session.put(
            url=url, json=payload, params=get_parameters
        ) as response:
            if response.status == 200:
                try:
                    response_data = await response.json()
                    if isinstance(response_data, return_type):
                        return response_data
                    return return_type.parse_obj(response_data)
                except ContentTypeError as e:
                    raise self._exception(
                        method="PUT",
                        resource=resource,
                        status=response.status,
                        text=await response.text(),
                    ) from e
            else:
                raise self._exception(
                    method="PUT",
                    resource=resource,
                    status=response.status,
                    text=await response.text(),
                )

    async def _delete(
        self,
        resource: str,
        return_type: Type[BaseModel | list | dict],
        get_parameter6: dict = None,
    ) -> BaseModel | list | dict | str:
        """Make a generic DELETE request to the API.

        Args:
            resource: The resource to update (with starting slash).
            get_parameter6: The GET parameter to send with the request.
            return_type: The type that is expected to be returned.

        Returns:
            return_type: The data parsed as `return_type`.
        """
        logging.debug("Database client is deleting %s.", resource)

        if get_parameter6 is None:
            get_parameter6 = {}
        url = f"{self._db_api_base_url}{resource}"

        async with self._session.delete(url=url, params=get_parameter6) as response:
            if response.status == 200:
                try:
                    response_data = await response.json()
                    if isinstance(response_data, return_type):
                        return response_data
                    return return_type.parse_obj(response_data)
                except ContentTypeError as e:
                    raise self._exception(
                        method="DELETE",
                        resource=resource,
                        status=response.status,
                        text=await response.text(),
                    ) from e
            else:
                raise self._exception(
                    method="DELETE",
                    resource=resource,
                    status=response.status,
                    text=await response.text(),
                )
