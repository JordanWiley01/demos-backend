# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Module contains enums used for typing."""
from enum import Enum


class RequestStatus(str, Enum):
    """Reflects the status of a request."""

    PENDING = "pending"
    APPROVED = "approved"
    DENIED = "denied"


class ReportStatus(str, Enum):
    """Reflects the status of a Report."""

    PENDING = "pending"
    INVESTIGATING = "investigating"
    APPROVED = "approved"
    DENIED = "denied"


class InformationCountry(str, Enum):
    """Supported countries for information queries."""

    GERMANY = "Germany"


class InformationCity(str, Enum):
    """Supported cities for information queries."""

    BERLIN = "Berlin"
    HAMBURG = "Hamburg"
    FRANKFURT_AM = "Frankfurt am Main"


class AvailableLanguages(str, Enum):
    """Supported languages throughout the application."""

    GERMAN = "de"
    ENGLISH = "en"


class ReoccurringEventPeriod(str, Enum):
    """Supported time periods for reoccurring events."""

    NONE = "none"
    WEEKLY = "weekly"
    BIWEEKLY = "bi-weekly"
    MONTHLY = "monthly"


class InternalDocumentType(str, Enum):
    """Types of internal documents."""

    FINANCIAL_REPORT = "financial report"
    CONSTITUTION = "constitution"
