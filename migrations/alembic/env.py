# SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from logging.config import fileConfig

from alembic import context
from sqlmodel import SQLModel

from demos_backend.core.databases.mariadb.session import get_database_uri, get_engine
from demos_backend.core.models.event import *
from demos_backend.core.models.event_report import *
from demos_backend.core.models.event_type import *
from demos_backend.core.models.event_type_request import *
from demos_backend.core.models.information import *
from demos_backend.core.models.link_tables import *
from demos_backend.core.models.location import *
from demos_backend.core.models.organisation import *
from demos_backend.core.models.organisation_relation_request import *
from demos_backend.core.models.organisation_report import *
from demos_backend.core.models.organisation_type import *
from demos_backend.core.models.organisation_type_request import *
from demos_backend.core.models.topic import *
from demos_backend.core.models.topic_request import *
from demos_backend.core.models.user import *

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = SQLModel.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = get_database_uri()
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = get_engine()

    with connectable.connect() as connection:
        context.configure(connection=connection, target_metadata=target_metadata)

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
