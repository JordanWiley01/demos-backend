<!--
SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Contributing

Contributions are welcome, and they are greatly appreciated!
Every bit helps, and credit will always be given.

You can contribute in many ways:

---

## Table of Content

* [Types of Contribution](#types-of-contributions)
  * [Report Bugs](#report-bugs)
  * [Fix Bugs](#fix-bugs)
  * [Request a Feature](#requests-a-feature)
  * [Implement Features](#implement-features)
  * [Write Documentation](#write-documentation)
  * [Submit Feedback](#submit-feedback)
* [Workflow](#workflow)
* [Merge Request Guidelines](#merge-request-guidelines)
* [Contributor License Agreement](#contributor-license-agreement-cla)

---

## Types of Contributions

### Report Bugs

Report bugs as [issue](https://gitlab.com/demos-berlin-ev/demos-backend/-/issues?sort=created_date&state=opened).

If you are reporting a bug, please include:

* Detailed steps to reproduce the bug.

### Fix Bugs

Look through the GitLab issues for bugs.
Anything tagged with "bug" and "help wanted" is open to whoever wants to implement it.


### Requests a Feature

Create an [issue](https://gitlab.com/demos-berlin-ev/demos-backend/-/issues?sort=created_date&state=opened) with the feature request.

### Implement Features

Look through the GitLab issues for features.
Anything tagged with "enhancement" and "help wanted" is open to whoever wants to implement it.

### Write Documentation

The demos-backend could always use more documentation.
Whether as part of the official docs or in the docstrings.

### Submit Feedback

TBD

---

## Workflow

We follow the [GitLab Workflow](https://docs.gitlab.com/ee/topics/gitlab_flow.html).
Additionally, we follow this workflow internally:

1. Assign an open issue to yourself.
2. Create a Merge Request from the issue.
3. Work on the branch.
4. Assign one of our maintainers as reviewer.
5. Wait for the Merge Request to be processed.

---

## Merge Request Guidelines

Before you submit a merge request, check that it meets these guidelines:

1. The merge request should include tests.
2. If the merge request adds functionality, the docs should be updated.
3. Check your [merge request](https://gitlab.com/demos-berlin-ev/demos-backend/-/merge_requests) and make sure that the tests pass.
4. All changes need to be compatible with the respective [license](README.md#license).

---

## Contributor License Agreement (CLA)

If you commit any changes to this project, you thereby grant all rights and permissions of your code to Demos Berlin e.V. .
