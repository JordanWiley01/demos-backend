<!--
SPDX-FileCopyrightText: 2023 Demos Berlin e.V. (https://demonstrations.org)

SPDX-License-Identifier: AGPL-3.0-or-later
-->

## License

Copyright © [<YEAR>] [<COPYRIGHT>]

This work is licensed under the following license(s):
[<SCOPE>]

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
